package com.orchtech.edaradotcom.managers.Question;

import com.orchtech.edaradotcom.models.questions.QuestionDataResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/16/2017.
 */

public class QuestionDataManager {


    RetrofitRepository retrofitRepository;

    public QuestionDataManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<QuestionDataResponse> set_question(String SessionID, String Title, String Text, boolean ShowName) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<QuestionDataResponse> call_question = Api_Service.setConsulting(SessionID, Title, Text, ShowName);
        return call_question;
    }
}
