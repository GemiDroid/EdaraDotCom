package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.MySubscriptionResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/11/2017.
 */

public class MySubscriptionManager {

    RetrofitRepository retrofitRepository;

    public MySubscriptionManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<MySubscriptionResponse> get_your_subscription(String Session) { //String session_id
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<MySubscriptionResponse> your_subscription = Api_Service.getMySubscription(Session); //session_id
        return your_subscription;

    }
}
