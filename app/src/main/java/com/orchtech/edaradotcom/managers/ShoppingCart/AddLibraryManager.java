package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class AddLibraryManager {

    RetrofitRepository retrofitRepository;

    public AddLibraryManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> add_library_to_cart(String SessionId, String LibraryId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> add_library = Api_Service.AddLibraryToInvoice(SessionId, LibraryId);
        return add_library;

    }
}
