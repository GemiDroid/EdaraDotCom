package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import java.sql.Time;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class SettingsManager {

    RetrofitRepository retrofitRepository;

    public SettingsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> Settings(Time QuoteTime, boolean isArabic, String Subjects,
                                          boolean QuoteAppearence, boolean isArabicQuotes, int Font
            , String SessionId, boolean isWhatNew, boolean isKhoulsatName) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> getSettings = Api_Service.getSaveSettings(QuoteTime, isArabic, Subjects, QuoteAppearence,
                isArabicQuotes, Font, SessionId, isWhatNew, isKhoulsatName);
        return getSettings;
    }
}
