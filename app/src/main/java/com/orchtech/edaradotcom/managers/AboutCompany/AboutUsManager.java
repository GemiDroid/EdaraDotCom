package com.orchtech.edaradotcom.managers.AboutCompany;

import com.orchtech.edaradotcom.models.AboutUs.AboutUsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */

public class AboutUsManager {
    RetrofitRepository retrofitRepository;

    public AboutUsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<AboutUsResponse> get_about_us() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<AboutUsResponse> get_about = Api_Service.getAboutUs();
        return get_about;

    }
}
