package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class ComplaintsManager {

    RetrofitRepository retrofitRepository;

    public ComplaintsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> get_complaints(String Name, String Email, String Message, boolean isComplain,
                                                boolean isSuggestion, String Phone) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> get_comp = Api_Service.setComplaints(Name, Email, Message, isComplain, isSuggestion
                , Phone);
        return get_comp;

    }
}
