package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.cart.ShoppingCartResponseModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class CartDataManager {

    RetrofitRepository retrofitRepository;

    public CartDataManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ShoppingCartResponseModel> get_cart_data(String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ShoppingCartResponseModel> get_cart = Api_Service.GetShoppingCart(SessionId);
        return get_cart;

    }
}
