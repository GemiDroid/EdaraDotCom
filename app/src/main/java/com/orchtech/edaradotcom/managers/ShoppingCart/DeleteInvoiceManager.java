package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class DeleteInvoiceManager {

    RetrofitRepository retrofitRepository;

    public DeleteInvoiceManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> DeleteInvoice(String SessionId, String InvoiceId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> delete_invoice = Api_Service.DeleteInvoice(SessionId, InvoiceId);
        return delete_invoice;

    }
}
