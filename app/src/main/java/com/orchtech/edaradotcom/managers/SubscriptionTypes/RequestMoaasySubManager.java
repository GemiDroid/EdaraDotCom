package com.orchtech.edaradotcom.managers.SubscriptionTypes;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/15/2017.
 */

public class RequestMoaasySubManager {

    RetrofitRepository retrofitRepository;

    public RequestMoaasySubManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> get_RequestMoassySub(String FullName, String Email, String Company
            , String Country, String Notes, String Phone) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> get_RequestMoassy = Api_Service.getRequestMoassy(FullName, Email, Company,
                Country, Notes, Phone);
        return get_RequestMoassy;

    }
}
