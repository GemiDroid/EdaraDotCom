package com.orchtech.edaradotcom.managers.Payment;

import com.orchtech.edaradotcom.models.Payment.BankTransferModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class BankTransferManger {

    RetrofitRepository retrofitRepository;

    public BankTransferManger() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<BankTransferModel> get_BankTransfer() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<BankTransferModel> get_bank = Api_Service.getBankTransfer();
        return get_bank;

    }


}
