package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.PrivacyModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import java.util.List;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class PrivacyManager {

    RetrofitRepository retrofitRepository;

    public PrivacyManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<List<PrivacyModel>> Privacy(boolean isArabic) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<List<PrivacyModel>> getPrivacy = Api_Service.getPrivacy(isArabic);
        return getPrivacy;
    }
}
