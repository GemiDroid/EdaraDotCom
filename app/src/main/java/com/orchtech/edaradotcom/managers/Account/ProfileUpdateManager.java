package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class ProfileUpdateManager {

    RetrofitRepository retrofitRepository;

    public ProfileUpdateManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> get_update_profile(String SessionId, String FullName,
                                                    String Email, String OldPass, String NewPass) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> get_update_profile = Api_Service.getUpdateProfile(SessionId, FullName, Email, OldPass, NewPass);
        return get_update_profile;

    }
}
