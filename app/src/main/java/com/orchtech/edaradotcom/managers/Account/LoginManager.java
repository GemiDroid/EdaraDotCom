package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 2/8/2017.
 */

public class LoginManager {
    RetrofitRepository retrofitRepository;

    public LoginManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> get_login(String Email, String Password) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> get_login = Api_Service.get_login(Email, Password);
        return get_login;

    }
}
