package com.orchtech.edaradotcom.managers.Khoulasat;

import com.orchtech.edaradotcom.models.KhoulastDialogResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class KholasatInfoManager {

    RetrofitRepository retrofitRepository;

    public KholasatInfoManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<KhoulastDialogResponse> get_kholasat_info(int Id) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<KhoulastDialogResponse> kholasat_info = Api_Service.getInfoDialog(Id);
        return kholasat_info;
    }
}
