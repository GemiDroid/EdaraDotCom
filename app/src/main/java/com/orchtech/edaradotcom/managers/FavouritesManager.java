package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class FavouritesManager {


    RetrofitRepository retrofitRepository;

    public FavouritesManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<SearchResponse> Faourites(String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<SearchResponse> getFavourites = Api_Service.getFavourites(SessionId);
        return getFavourites;
    }
}
