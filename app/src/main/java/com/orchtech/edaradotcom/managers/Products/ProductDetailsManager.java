package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/3/2017.
 */

public class ProductDetailsManager {

    RetrofitRepository retrofitRepository;

    public ProductDetailsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_products_details(String ItemId, String Session) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> call_product_details = Api_Service.get_product_details(ItemId, Session);
        return call_product_details;
    }


}
