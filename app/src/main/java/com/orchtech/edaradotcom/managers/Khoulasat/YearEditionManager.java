package com.orchtech.edaradotcom.managers.Khoulasat;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class YearEditionManager {

    RetrofitRepository retrofitRepository;

    public YearEditionManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_edition_year(int Offset, int ProductTypeId, int Limit, String Session, int issuedYear) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> edition_year = Api_Service.getYearEdition(Offset, ProductTypeId
                , Limit, Session, issuedYear);
        return edition_year;
    }
}
