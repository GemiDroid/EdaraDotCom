package com.orchtech.edaradotcom.managers.Khoulasat;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class RecentBooksManager {

    RetrofitRepository retrofitRepository;

    public RecentBooksManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_recent_books(int Count, int ProductTypeId, String Session) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> recent_books = Api_Service.getRecentBooks(Count, ProductTypeId, Session);
        return recent_books;
    }
}
