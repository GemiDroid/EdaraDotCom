package com.orchtech.edaradotcom.managers.SubscriptionTypes;

import com.orchtech.edaradotcom.models.Subscription.MainSubModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class MainSubManager {


    RetrofitRepository retrofitRepository;

    public MainSubManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<MainSubModel> get_MainSubscription() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<MainSubModel> call_managers = Api_Service.getMainSubscription();
        return call_managers;

    }
}
