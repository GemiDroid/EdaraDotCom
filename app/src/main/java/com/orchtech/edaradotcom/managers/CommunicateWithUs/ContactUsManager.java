package com.orchtech.edaradotcom.managers.CommunicateWithUs;

import com.orchtech.edaradotcom.models.ContactUs.ContactUs;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */

public class ContactUsManager {

    RetrofitRepository retrofitRepository;

    public ContactUsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ContactUs> get_call_us() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ContactUs> get_call = Api_Service.getContactUS();
        return get_call;

    }
}
