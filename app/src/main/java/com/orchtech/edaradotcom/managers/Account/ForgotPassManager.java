package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/17/2017.
 */

public class ForgotPassManager {


    RetrofitRepository retrofitRepository;

    public ForgotPassManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> get_lost_pass(String Email) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> get_password = Api_Service.get_password(Email);
        return get_password;

    }

}
