package com.orchtech.edaradotcom.managers.Account;


import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 2/8/2017.
 */

public class RegisterManager {
    RetrofitRepository retrofitRepository;

    public RegisterManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel> get_register(String Email, String Phone, String UserName, String Country) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel> get_register = Api_Service.get_register(Email, Phone, UserName, Country);
        return get_register;

    }
}
