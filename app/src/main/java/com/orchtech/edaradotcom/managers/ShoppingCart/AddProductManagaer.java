package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class AddProductManagaer {

    RetrofitRepository retrofitRepository;

    public AddProductManagaer() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> add_product_to_cart(String SessionId, String ProductId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> add_product = Api_Service.AddProductToInvoice(SessionId, ProductId);
        return add_product;

    }
}
