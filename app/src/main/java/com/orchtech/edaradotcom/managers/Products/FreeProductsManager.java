package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class FreeProductsManager {


    RetrofitRepository retrofitRepository;

    public FreeProductsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_free_products(String Session, int ProductTypeId) { //String session_id
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> call_free_products = Api_Service.get_free_products(
                Session, ProductTypeId); //session_id
        return call_free_products;

    }
}
