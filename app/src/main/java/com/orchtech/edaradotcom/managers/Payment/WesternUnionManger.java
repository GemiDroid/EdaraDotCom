package com.orchtech.edaradotcom.managers.Payment;

import com.orchtech.edaradotcom.models.Payment.BankTransferModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class WesternUnionManger {

    RetrofitRepository retrofitRepository;

    public WesternUnionManger() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<BankTransferModel> get_WesternUnion() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<BankTransferModel> get_western = Api_Service.getWesternUnion();
        return get_western;

    }
}
