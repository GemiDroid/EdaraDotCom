package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class AuthorsManager {

    RetrofitRepository retrofitRepository;

    public AuthorsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<SearchResponse> get_all_authors() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<SearchResponse> call_authors = Api_Service.getAllAuthors();
        return call_authors;

    }
}
