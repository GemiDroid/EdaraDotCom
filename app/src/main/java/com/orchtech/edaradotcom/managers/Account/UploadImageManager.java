package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 2/8/2017.
 */

public class UploadImageManager {
    RetrofitRepository retrofitRepository;

    public UploadImageManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> UploadImage(String SessionId, String Image, String ImageName) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> upload_image = Api_Service.UploadImage(SessionId, Image, ImageName);
        return upload_image;

    }
}
