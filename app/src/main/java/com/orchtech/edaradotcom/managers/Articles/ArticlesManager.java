package com.orchtech.edaradotcom.managers.Articles;

import com.orchtech.edaradotcom.models.Articles.ArticlesResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class ArticlesManager {

    RetrofitRepository retrofitRepository;

    public ArticlesManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ArticlesResponse> get_articles(String Offset, int Limit, String Year, String Author) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ArticlesResponse> call_articles = Api_Service.getArticlesList(Offset, Limit, Year, Author);
        return call_articles;
    }

}
