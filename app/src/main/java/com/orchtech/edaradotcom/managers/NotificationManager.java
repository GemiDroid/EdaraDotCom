package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.NotificationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import java.util.List;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class NotificationManager {

    RetrofitRepository retrofitRepository;

    public NotificationManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<List<NotificationModel>> Notification() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<List<NotificationModel>> getNotification = Api_Service.getNotifications();
        return getNotification;
    }

}
