package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.CountriesRegisterModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/17/2017.
 */

public class CountriesRegisterManager implements Serializable {

    RetrofitRepository retrofitRepository;

    public CountriesRegisterManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<List<CountriesRegisterModel>> get_all_countries() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<List<CountriesRegisterModel>> get_countries = Api_Service.getAllCountries();
        return get_countries;

    }

}
