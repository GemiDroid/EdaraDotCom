package com.orchtech.edaradotcom.managers.Training;

import com.orchtech.edaradotcom.models.TrainingDetailsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class TrainingDetailsManager {

    RetrofitRepository retrofitRepository;

    public TrainingDetailsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<TrainingDetailsResponse> getTrainingDetails(String trainingId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<TrainingDetailsResponse> gettDetails = Api_Service.getTrainingDetails(trainingId);
        return gettDetails;
    }
}
