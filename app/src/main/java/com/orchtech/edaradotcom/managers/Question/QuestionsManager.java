package com.orchtech.edaradotcom.managers.Question;

import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/13/2017.
 */

public class QuestionsManager {

    RetrofitRepository retrofitRepository;

    public QuestionsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<QuestionsListResponse> get_questions(int Offset, int Limit, boolean MyConsultant, String SearchText, String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<QuestionsListResponse> questions = Api_Service.getConsultant(Offset, Limit, MyConsultant, SearchText, SessionId);
        return questions;
    }
}
