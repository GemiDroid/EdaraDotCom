package com.orchtech.edaradotcom.managers.Account;

import com.orchtech.edaradotcom.models.Profile.ProfileModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class ProfileDataManager {

    RetrofitRepository retrofitRepository;

    public ProfileDataManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProfileModel> get_profile_data(String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProfileModel> get_profile = Api_Service.getProfileData(SessionId);
        return get_profile;

    }
}
