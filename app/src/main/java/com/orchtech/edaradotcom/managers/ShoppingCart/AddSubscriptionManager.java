package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class AddSubscriptionManager {

    RetrofitRepository retrofitRepository;

    public AddSubscriptionManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> add_subscription_to_cart(String SessionId, String SubscriptionType) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> add_subscription = Api_Service.AddSubscriptionToInvoice(SessionId, SubscriptionType);
        return add_subscription;

    }
}
