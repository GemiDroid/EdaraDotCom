package com.orchtech.edaradotcom.managers.Question;

import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/13/2017.
 */

public class QuestionsListManager {

    RetrofitRepository retrofitRepository;

    public QuestionsListManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<QuestionsListResponse> get_question_list(int offset) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<QuestionsListResponse> call_questions_list = Api_Service.getConsultantsIDs(offset);
        return call_questions_list;
    }
}
