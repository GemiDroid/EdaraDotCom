package com.orchtech.edaradotcom.managers.Question;

import com.orchtech.edaradotcom.models.questions.QuestionDetailsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 3/13/2017.
 */

public class QuestionDetailsManager {

    RetrofitRepository retrofitRepository;

    public QuestionDetailsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<QuestionDetailsResponse> get_question(int question_id) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<QuestionDetailsResponse> call_questions = Api_Service.getConsultants(question_id);
        return call_questions;
    }
}
