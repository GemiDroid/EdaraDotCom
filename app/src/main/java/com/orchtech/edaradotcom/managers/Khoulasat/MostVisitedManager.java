package com.orchtech.edaradotcom.managers.Khoulasat;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class MostVisitedManager {

    RetrofitRepository retrofitRepository;

    public MostVisitedManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_most_visited(int ProductTypeId, int Limit, String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> most_visited = Api_Service.getMostVisited(ProductTypeId, Limit, SessionId);
        return most_visited;
    }
}
