package com.orchtech.edaradotcom.managers.ShoppingCart;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class DeleteAllInvoicesManager {

    RetrofitRepository retrofitRepository;

    public DeleteAllInvoicesManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> DeleteAllInvoices(String SessionId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> delete_invoices = Api_Service.DeleteAllInvoices(SessionId);
        return delete_invoices;

    }
}
