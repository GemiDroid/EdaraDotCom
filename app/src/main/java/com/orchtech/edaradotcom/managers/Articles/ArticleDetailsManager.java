package com.orchtech.edaradotcom.managers.Articles;

import com.orchtech.edaradotcom.models.Articles.ArticlesModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class ArticleDetailsManager {

    RetrofitRepository retrofitRepository;

    public ArticleDetailsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ArticlesModel.ArticleDetails> get_article_details(int ID) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ArticlesModel.ArticleDetails> call_article_details = Api_Service.getArticleDetails(ID);
        return call_article_details;
    }
}
