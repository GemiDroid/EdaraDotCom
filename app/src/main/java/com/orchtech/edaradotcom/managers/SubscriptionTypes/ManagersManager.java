package com.orchtech.edaradotcom.managers.SubscriptionTypes;

import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/15/2017.
 */

public class ManagersManager {

    RetrofitRepository retrofitRepository;

    public ManagersManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<SearchResponse> get_ManagersLibraries() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<SearchResponse> call_managers = Api_Service.getManagersLibraries();
        return call_managers;

    }
}
