package com.orchtech.edaradotcom.managers.Training;

import com.orchtech.edaradotcom.models.TrainingListResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class TrainingListManager {

    RetrofitRepository retrofitRepository;

    public TrainingListManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<TrainingListResponse> get_training_list(String Year, String Month, String CityId, String PlanId) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<TrainingListResponse> training_list = Api_Service.getTrainingList(Year, Month, CityId, PlanId);
        return training_list;

    }
}
