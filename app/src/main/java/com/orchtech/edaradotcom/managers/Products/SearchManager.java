package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/12/2017.
 */

public class SearchManager {

    RetrofitRepository retrofitRepository;

    public SearchManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> Search(int Offset, int Limit, String Session,
                                         String SearchText, String SearchTitle,
                                         String YearFrom, String YearTo, String Subject,
                                         String ProductType, String Author) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> call_search = Api_Service.Search(Offset, Limit, Session, SearchText, SearchTitle
                , YearFrom, YearTo, Subject, ProductType, Author);
        return call_search;

    }
}
