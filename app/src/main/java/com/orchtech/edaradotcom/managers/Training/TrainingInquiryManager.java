package com.orchtech.edaradotcom.managers.Training;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class TrainingInquiryManager {

    RetrofitRepository retrofitRepository;

    public TrainingInquiryManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> getTrainingInquiry(String PlanId, String FullName,
                                                            String Email, String Message, String Phone) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> getTInquiry = Api_Service.getTrainingInquiry(PlanId,
                FullName, Email, Message, Phone);
        return getTInquiry;
    }

}
