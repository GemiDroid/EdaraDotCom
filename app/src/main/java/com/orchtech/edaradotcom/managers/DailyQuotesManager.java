package com.orchtech.edaradotcom.managers;

import com.orchtech.edaradotcom.models.DailyQuotesModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class DailyQuotesManager {

    RetrofitRepository retrofitRepository;

    public DailyQuotesManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<DailyQuotesModel> DailyQuotes(boolean isArabic) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<DailyQuotesModel> getDailyQuotes = Api_Service.getDailyQuotes(isArabic);
        return getDailyQuotes;
    }


}
