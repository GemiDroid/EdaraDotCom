package com.orchtech.edaradotcom.managers.Products;

import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class RecentProductsManager {

    RetrofitRepository retrofitRepository;

    public RecentProductsManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ProductsResponse> get_recent_products(int productTyeId, int count, String session_id) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ProductsResponse> call_recent_products = Api_Service.get_newest_products(productTyeId,count,session_id);
        return call_recent_products;
    }
}
