package com.orchtech.edaradotcom.managers.Customer;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class TellAfriendManager {

    RetrofitRepository retrofitRepository;

    public TellAfriendManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> get_tell_friend(String FEmail, String YName, String FName, String FMessage) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> get_about = Api_Service.getTellAfriend(FEmail, YName, FName, FMessage);
        return get_about;

    }
}
