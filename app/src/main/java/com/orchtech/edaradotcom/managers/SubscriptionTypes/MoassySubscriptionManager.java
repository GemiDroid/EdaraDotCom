package com.orchtech.edaradotcom.managers.SubscriptionTypes;

import com.orchtech.edaradotcom.models.Subscription.MoassyModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 4/15/2017.
 */

public class MoassySubscriptionManager {


    RetrofitRepository retrofitRepository;

    public MoassySubscriptionManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<MoassyModel> get_MoassySubscription() {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<MoassyModel> get_Moassy = Api_Service.getMoassySub();
        return get_Moassy;

    }
}
