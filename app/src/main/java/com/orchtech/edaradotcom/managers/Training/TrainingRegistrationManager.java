package com.orchtech.edaradotcom.managers.Training;

import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.repositories.RetrofitRepository;
import com.orchtech.edaradotcom.webservices.APIs;

import retrofit2.Call;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class TrainingRegistrationManager {


    RetrofitRepository retrofitRepository;

    public TrainingRegistrationManager() {
        retrofitRepository = new RetrofitRepository();
    }

    public Call<ValidationModel.Success> getTrainingRegistration(String PlanId, String FullName,
                                                                 String Email, String CountryId, String Phone,
                                                                 String TraineesNo, String MgrName, String MgrPhone) {
        APIs Api_Service = retrofitRepository.getRetrofit().create(APIs.class);
        Call<ValidationModel.Success> getTRegister = Api_Service.getTrainingRegistration(PlanId, FullName,
                Email, CountryId, Phone, TraineesNo, MgrName, MgrPhone);
        return getTRegister;
    }

}
