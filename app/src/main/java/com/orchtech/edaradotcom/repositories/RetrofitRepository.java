package com.orchtech.edaradotcom.repositories;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class RetrofitRepository {

    Retrofit retrofit=null;

    public Retrofit getRetrofit() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();


        if (retrofit == null) {
            // .client(Settings.getCertificate(context))
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://edara.com/restful/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;

    }
}
