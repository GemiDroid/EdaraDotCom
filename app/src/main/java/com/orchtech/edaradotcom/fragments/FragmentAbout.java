package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.AboutUsAdapter;
import com.orchtech.edaradotcom.managers.AboutCompany.AboutUsManager;
import com.orchtech.edaradotcom.models.AboutUs.AboutUsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 1/17/2017.
 */

public class FragmentAbout extends Fragment {


    static String url;
    AboutUsManager aboutUsManager;
    TextView txt_msg, txt_eltzam;
    RecyclerView rec_eltzam;
    AboutUsAdapter aboutUsAdapter;
    ProgressBar loading_bar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_about, container, false);


        // ImageView img_main = (ImageView) getActivity().findViewById(R.id.img_main);

        loading_bar = (ProgressBar) getActivity().findViewById(R.id.home_progress);
        /* Glide.with(getActivity()).load(R.drawable.search_loading).asGif().into(loading_bar);*/

        rec_eltzam = (RecyclerView) v.findViewById(R.id.rec_eltzam);
        rec_eltzam.setLayoutManager(new LinearLayoutManager(getActivity()));

        txt_msg = (TextView) v.findViewById(R.id.txt_msg);
        txt_eltzam = (TextView) v.findViewById(R.id.txt_eltzam);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("من نحن");
        FragmentSideMenu.showDefaultFooter(getActivity());

        if (aboutUsManager == null) {
            aboutUsManager = new AboutUsManager();
        }
        loading_bar.setVisibility(View.VISIBLE);
        aboutUsManager.get_about_us().
                enqueue(new Callback<AboutUsResponse>() {
                            @Override
                            public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {
                                if (response.code() >= 200 && response.code() <= 300) {
                                    if (response.body() == null) {
                                        Snackbar.make(getView(), "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                                    } else {
                                        txt_msg.setText(response.body().getMsgBody());
                                        txt_eltzam.setText(response.body().getCommitmentHearder());
                                        aboutUsAdapter = new AboutUsAdapter(getActivity(), response.body().getGetBulleted());
                                        rec_eltzam.setAdapter(aboutUsAdapter);

                                    }
                                } else {
                                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                    // Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();

                                }

                                loading_bar.setVisibility(GONE);
                            }

                            @Override
                            public void onFailure(Call<AboutUsResponse> call, Throwable t) {
                                Snackbar.make(getView(), "من فضلك تأكد من اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                                loading_bar.setVisibility(GONE);
                            }


                        }

                );

        return v;
    }
}
