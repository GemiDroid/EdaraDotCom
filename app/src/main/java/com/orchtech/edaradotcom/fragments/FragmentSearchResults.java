package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.activities.EndlessRecyclerOnScrollListener;
import com.orchtech.edaradotcom.adapters.SearchResultAdapter;
import com.orchtech.edaradotcom.managers.Products.SearchManager;
import com.orchtech.edaradotcom.models.Products.ProductsModel;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 4/12/2017.
 */

public class FragmentSearchResults extends Fragment {


    static String Session;
    static int page_count = 10;
    String product_type, author, subject, edt_body, edt_subject, year_from, year_to;
    SearchManager searchManager;
    RecyclerView rec_search_result;
    SearchResultAdapter searchResultAdapter;
    TextView txt_from_search;
    List<ProductsModel> SearchResponse;
    ProgressBar prog_search_results;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_search_results, container, false);

        txt_from_search = (TextView) v.findViewById(R.id.txt_from_search);

        prog_search_results = (ProgressBar) v.findViewById(R.id.prog_search_results);

        rec_search_result = (RecyclerView) v.findViewById(R.id.rec_search_results);
        rec_search_result.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        rec_search_result.setLayoutManager(manager);

        visibilties();

        try {
            Bundle bundle = getArguments();
/* bundle.putString("author",selected_author);
                bundle.putString("subject",selected_subject);
                bundle.putString("product_type",selected_product_type);
                bundle.putString("from_year",selected_from_year);
                bundle.putString("to_year",selected_to_year);*/

            /*txt_from_search.setText("Author : " + bundle.getString("author") + "\n" +
                    "Subject : " + bundle.getString("subject") + "\n" +
                    "Product Type : " + bundle.getString("product_type") + "\n" +
                    "Year From : " + bundle.getString("from_year") + "\n" +
                    "Year To : " + bundle.getString("to_year") + "\n" +
                    "Edit Subject : " + bundle.getString("edt_subject") + "\n" +
                    "Edit Body : " + bundle.getString("edt_body"));
*/
            product_type = bundle.getString("product_type");
            author = bundle.getString("author");
            subject = bundle.getString("subject");
            edt_body = bundle.getString("edt_body");
            edt_subject = bundle.getString("edt_subject");
            year_from = bundle.getString("from_year");
            year_to = bundle.getString("to_year");


        } catch (Exception e) {
        }
        SearchResponse = new ArrayList<>();
        searchResultAdapter = new SearchResultAdapter(getActivity(), SearchResponse);
        rec_search_result.setAdapter(searchResultAdapter);

        rec_search_result.addOnScrollListener(new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int current_page) {
                page_count += 9;
                get_search_values(page_count);
            }
        });


        get_search_values(page_count);


        return v;

    }

    private void get_search_values(int page) {
        if (searchManager == null) {
            searchManager = new SearchManager();
        }
        edt_body = edt_body == "" ? null : edt_body;
        edt_subject = edt_subject == "" ? null : edt_subject;
        subject = subject == "" ? null : subject;
        product_type = product_type == "" ? null : product_type;
        author = author == "" ? null : author;

        searchManager.Search(10, page, Session, edt_body, edt_subject, year_from, year_to, subject, product_type, author)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                        prog_search_results.setVisibility(View.VISIBLE);

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body().getItems() == null || response.body() == null) {
                                Toast.makeText(getActivity(), "لا توجد نتائج لبحثك", Toast.LENGTH_SHORT).show();
                                txt_from_search.setText("نتائج البحث " + "(0)");
                                prog_search_results.setVisibility(View.GONE);
                            } else {
                                SearchResponse.addAll(response.body().getItems());
                                searchResultAdapter.notifyItemRangeInserted(searchResultAdapter.getItemCount(), SearchResponse.size() - 1);
                                searchResultAdapter.notifyDataSetChanged();
                                txt_from_search.setText("نتائج البحث " + "(" + SearchResponse.size() + ")");
                                prog_search_results.setVisibility(View.GONE);
                            }
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                            prog_search_results.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {

                        Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                        prog_search_results.setVisibility(View.GONE);
                    }
                });
    }

    private void visibilties() {

        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText("نتيجة البحث");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
      /*  getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);*/
        ActivityHomePage.BuutonsAction(R.drawable.advanced_search, R.drawable.my_library2, R.drawable.home2, R.drawable.my_profile2);
    }
}
