package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Training.TrainingInquiryManager;
import com.orchtech.edaradotcom.models.ValidationModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class FragmentTrainingInquiry extends Fragment {


    static String PlanId;
    EditText edt_inquiry_name, edt_inquiry_email, edt_inquiry_phone, edt_inquiry_message;
    Button btn_inquiry_send;
    TrainingInquiryManager trainingInquiryManager;
    TextView txt_training_title, txt_training_period, txt_training_from_to, txt_training_city, txt_training_language;
    LinearLayout linear_training_contents;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_training_inquiry, container, false);

        edt_inquiry_name = (EditText) v.findViewById(R.id.edt_t_inquiry_name);
        edt_inquiry_email = (EditText) v.findViewById(R.id.edt_t_inquiry_email);
        edt_inquiry_phone = (EditText) v.findViewById(R.id.edt_t_inquiry_phone);
        edt_inquiry_message = (EditText) v.findViewById(R.id.edt_t_inquiry_message);

        btn_inquiry_send = (Button) v.findViewById(R.id.btn_t_inquiry_send);

        txt_training_title = (TextView) v.findViewById(R.id.txt_training_title);
        txt_training_from_to = (TextView) v.findViewById(R.id.txt_training_from_to);
        txt_training_period = (TextView) v.findViewById(R.id.txt_training_period);
        txt_training_city = (TextView) v.findViewById(R.id.txt_training_place);
        txt_training_language = (TextView) v.findViewById(R.id.txt_training_language);

        linear_training_contents = (LinearLayout) v.findViewById(R.id.lin_training_contents);
        linear_training_contents.setVisibility(GONE);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("استعلام");
        FragmentSideMenu.showDefaultFooter(getActivity());


        try {

            Bundle b = getArguments();
            PlanId = b.getString("planId");
            txt_training_title.setText(b.getString("training_title"));
            txt_training_from_to.setText(b.getString("training_from_to"));
            txt_training_period.setText(b.getString("training_period"));
            txt_training_city.setText(b.getString("training_place"));
            txt_training_language.setText("لغة البرنامج العربية");

        } catch (Exception e) {
        }

        btn_inquiry_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InquireTraining();

            }
        });


        return v;
    }

    private void InquireTraining() {

        CheckValidation();

    }

    private void CheckValidation() {

        if (edt_inquiry_name.getText().toString().equals("")) {
            edt_inquiry_name.setError("يجب ادخال الاسم");
        } else if (edt_inquiry_email.getText().toString().equals("")) {
            edt_inquiry_email.setError("يجب ادخال البريد الالكتروني");
        } else if (!FragmentForgotPass.check_email(edt_inquiry_email.getText().toString())) {
            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحه", "الاستعلام", getActivity()).show();
        } else if (edt_inquiry_message.getText().toString().equals("")) {
            edt_inquiry_message.setError(" من فضلك أدخل رسالتك");
        } else {
            StartInquiring();
        }

    }

    private void StartInquiring() {

        if (trainingInquiryManager == null) {
            trainingInquiryManager = new TrainingInquiryManager();
        }


        trainingInquiryManager.getTrainingInquiry(PlanId, edt_inquiry_name.getText().toString(),
                edt_inquiry_email.getText().toString(), edt_inquiry_message.getText().toString(),
                edt_inquiry_phone.getText().toString()).enqueue(new Callback<ValidationModel.Success>() {
            @Override
            public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {
                        if (response.body().isSuccess()) {
                            FragmentForgotPass.showMsg("تم ارسال استعلامك بنجاح", "الاستعلام",
                                    getActivity()).show();
                        } else {
                            FragmentForgotPass.showMsg(response.body().getMessage(), "الاستعلام",
                                    getActivity()).show();
                        }
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure),
                        Snackbar.LENGTH_SHORT).show();
            }
        });

    }
}
