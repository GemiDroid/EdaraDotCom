package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Payment.BankTransferManger;
import com.orchtech.edaradotcom.models.Payment.BankTransferModel;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class FragmentBankTransfer extends Fragment {

    BankTransferManger bankTransferManger;
    ImageView bank_logo1, bank_logo2;
    TextView bank_header, bank_name1, bank_ibn1, bank_name2, bank_ibn2;

    LinearLayout lin_bank_transfer;

    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_bank_transfer, container, false);
        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("تحويل بنكي");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        bank_logo1 = (ImageView) v.findViewById(R.id.img_bank_logo1);
        bank_logo2 = (ImageView) v.findViewById(R.id.img_bank_logo2);

        bank_header = (TextView) v.findViewById(R.id.txt_bank_header);

        bank_name1 = (TextView) v.findViewById(R.id.txt_bank_name1);
        bank_ibn1 = (TextView) v.findViewById(R.id.txt_bank_ibn1);

        bank_name2 = (TextView) v.findViewById(R.id.txt_bank_name2);
        bank_ibn2 = (TextView) v.findViewById(R.id.txt_bank_ibn2);

        lin_bank_transfer = (LinearLayout) v.findViewById(R.id.fragment_bank_transfer);

        if (bankTransferManger == null) {
            bankTransferManger = new BankTransferManger();
        }

        lin_bank_transfer.setVisibility(GONE);

        bankTransferManger.get_BankTransfer().enqueue(new Callback<BankTransferModel>() {
            @Override
            public void onResponse(Call<BankTransferModel> call, Response<BankTransferModel> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                        lin_bank_transfer.setVisibility(GONE);
                    } else {

                        Picasso.with(getActivity()).load(response.body().getFirstBankLogo()).into(bank_logo1);
                        Picasso.with(getActivity()).load(response.body().getSecondBankLogo()).into(bank_logo2);

                        bank_header.setText(response.body().getHeader());

                        bank_name1.setText(response.body().getFirstBankName());
                        bank_ibn1.setText(response.body().getFirstIban());

                        bank_name2.setText(response.body().getSecondBankName());
                        bank_ibn2.setText(response.body().getSecondBankIban());
                        lin_bank_transfer.setVisibility(View.VISIBLE);
                    }
                } else {
                    lin_bank_transfer.setVisibility(GONE);
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<BankTransferModel> call, Throwable t) {
                lin_bank_transfer.setVisibility(GONE);
                Snackbar.make(getView(), getResources().getString(R.string.internet_failure),
                        Snackbar.LENGTH_SHORT).show();
            }
        });


        return v;
    }
}
