package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.managers.SubscriptionTypes.RequestMoaasySubManager;
import com.orchtech.edaradotcom.models.ValidationModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/17/2017.
 */

public class FragmentRequestMoassy extends Fragment {

    EditText edt_req_moassy_name, edt_req_moassy_email, edt_req_moassy_com, edt_req_moassy_phone, edt_req_moassy_country, edt_req_moassy_notes;
    Button btn_req_moassy_order;
    RequestMoaasySubManager requestMoaasySubManager;
    String TAG = FragmentRequestMoassy.class.getSimpleName();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_request_moassy, container, false);

        // Declare Variables here ...
        edt_req_moassy_name = (EditText) v.findViewById(R.id.edt_req_moassy_name);
        edt_req_moassy_email = (EditText) v.findViewById(R.id.edt_req_moassy_email);
        edt_req_moassy_com = (EditText) v.findViewById(R.id.edt_req_moassy_company);
        edt_req_moassy_phone = (EditText) v.findViewById(R.id.edt_req_moassy_phone);
        edt_req_moassy_country = (EditText) v.findViewById(R.id.edt_req_moassy_country);
        edt_req_moassy_notes = (EditText) v.findViewById(R.id.edt_req_moassy_notes);

        btn_req_moassy_order = (Button) v.findViewById(R.id.btn_moassy_order);


        btn_req_moassy_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckFields();
            }
        });

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاشتراك المؤسسي");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        return v;

    }

    private void CheckFields() {
        if (edt_req_moassy_name.getText().toString().equals("")) {

            edt_req_moassy_name.setError("برجاء ادخال الاسم");
        } else if (edt_req_moassy_email.getText().toString().equals("")) {

            edt_req_moassy_email.setError("برجاء ادخال الإيميل");
        } else if (!FragmentForgotPass.check_email(edt_req_moassy_email.getText().toString())) {

            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحه", "الإشتراك المؤسسي", getActivity()).show();
        } else if (edt_req_moassy_com.getText().toString().equals("")) {

            edt_req_moassy_com.setError("برجاء ادخال إسم الشركة");
        } else if (edt_req_moassy_country.getText().toString().equals("")) {

            edt_req_moassy_country.setError("برجاء ادخال إسم الدولة");
        } else {
            ProceedRequest();
        }

    }

    private void ProceedRequest() {

        String notes = edt_req_moassy_notes.getText().toString().equals("") ? "" : edt_req_moassy_notes.getText().toString();
        String phone = edt_req_moassy_phone.getText().toString().equals("") ? "" : edt_req_moassy_phone.getText().toString();


        if (requestMoaasySubManager == null) {
            requestMoaasySubManager = new RequestMoaasySubManager();
        }
        requestMoaasySubManager.get_RequestMoassySub(edt_req_moassy_name.getText().toString(), edt_req_moassy_email.getText().toString(), edt_req_moassy_com.getText().toString(), edt_req_moassy_country.getText().toString()
                , notes, phone).enqueue(new Callback<ValidationModel.Success>() {
            @Override
            public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {
                        if (response.body().isSuccess()) {
                            FragmentForgotPass.showMsg("تم ارسال الطلب", "الإشتراك المؤسسي", getActivity()).show();
                            ClearFields();
                        } else {
                            FragmentForgotPass.showMsg(response.body().getMessage(), "الإشتراك المؤسسي", getActivity()).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ValidationModel.Success> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void ClearFields() {
        edt_req_moassy_name.setText("");
        edt_req_moassy_email.setText("");
        edt_req_moassy_com.setText("");
        edt_req_moassy_country.setText("");
        edt_req_moassy_phone.setText("");
        edt_req_moassy_notes.setText("");
    }

}

