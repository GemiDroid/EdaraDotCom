package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Account.CountriesRegisterManager;
import com.orchtech.edaradotcom.managers.Training.TrainingRegistrationManager;
import com.orchtech.edaradotcom.models.CountriesRegisterModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class FragmentTrainingRegistration extends Fragment {


    static String CountryId, planId, clientEmail;

    TrainingRegistrationManager TRegistrationManager;
    EditText edt_t_register_name, edt_t_register_email, edt_t_register_phone,
            edt_t_register_mgrName, edt_t_register_mgrPhone, edt_t_register_trainee;
    Button btn_t_register_send;
    Spinner spin_t_register_country;

    TextView txt_training_title, txt_training_period, txt_training_from_to, txt_training_city, txt_training_language;

    List<String> CountryNameList;
    CountriesRegisterManager countriesManager;
    List<Integer> CountryIdList;
    LinearLayout linear_training_contents;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        clientEmail = Preferences.getFromPreference(getActivity(), "email");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_training_registration, container, false);


        edt_t_register_name = (EditText) v.findViewById(R.id.edt_t_register_name);
        edt_t_register_email = (EditText) v.findViewById(R.id.edt_t_register_email);
        edt_t_register_phone = (EditText) v.findViewById(R.id.edt_t_register_phone);
        edt_t_register_mgrName = (EditText) v.findViewById(R.id.edt_t_register_mgrName);
        edt_t_register_mgrPhone = (EditText) v.findViewById(R.id.edt_t_register_mgrPhone);
        edt_t_register_trainee = (EditText) v.findViewById(R.id.edt_t_register_trainee);


        txt_training_title = (TextView) v.findViewById(R.id.txt_training_title);
        txt_training_from_to = (TextView) v.findViewById(R.id.txt_training_from_to);
        txt_training_period = (TextView) v.findViewById(R.id.txt_training_period);
        txt_training_city = (TextView) v.findViewById(R.id.txt_training_place);
        txt_training_language = (TextView) v.findViewById(R.id.txt_training_language);


        spin_t_register_country = (Spinner) v.findViewById(R.id.spin_t_register_country);

        btn_t_register_send = (Button) v.findViewById(R.id.btn_t_register_send);

        linear_training_contents = (LinearLayout) v.findViewById(R.id.lin_training_contents);
        linear_training_contents.setVisibility(GONE);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("تسجيل");
        FragmentSideMenu.showDefaultFooter(getActivity());

        edt_t_register_email.setText(clientEmail);

        try {
            Bundle b = getArguments();
            planId = b.getString("planId");

            txt_training_title.setText(b.getString("training_title"));
            txt_training_from_to.setText(b.getString("training_from_to"));
            txt_training_period.setText(b.getString("training_period"));
            txt_training_city.setText(b.getString("training_place"));
            txt_training_language.setText("لغة البرنامج العربية");


        } catch (Exception e) {
        }


        btn_t_register_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckValidation();

            }
        });

        CountryNameList = new ArrayList<>();
        CountryIdList = new ArrayList<>();

        GetAllCountries();

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, CountryNameList);
        countryAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_t_register_country.setAdapter(countryAdapter);

        spin_t_register_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                CountryId = String.valueOf(CountryIdList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return v;

    }

    private void CheckValidation() {

        if (edt_t_register_name.getText().toString().equals("")) {
            edt_t_register_name.setError("يجب ادخال الاسم");
        } else if (edt_t_register_email.getText().toString().equals("")) {
            edt_t_register_email.setError("يجب ادخال البريد الالكتروني");
        } else if (!FragmentForgotPass.check_email(edt_t_register_email.getText().toString())) {
            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحه", "الاستعلام", getActivity()).show();
        } else if (edt_t_register_phone.getText().toString().equals("")) {
            edt_t_register_phone.setError(" من فضلك أدخل رقم هاتفك");
        } else if (edt_t_register_trainee.getText().toString().equals("")) {

            edt_t_register_trainee.setError(" من فضلك أدخل  عدد المشتركين");
        } else {
            RegisterToProgram();
        }

    }

    private void RegisterToProgram() {

        if (TRegistrationManager == null) {
            TRegistrationManager = new TrainingRegistrationManager();
        }

        Log.d("CountryId", "RegisterToProgram:" + CountryId);

        CountryId = CountryId == null ? "0" : CountryId;

        TRegistrationManager.getTrainingRegistration(planId, edt_t_register_name.getText().toString()
                , edt_t_register_email.getText().toString(), CountryId, edt_t_register_phone.getText().toString()
                , edt_t_register_trainee.getText().toString(), edt_t_register_mgrName.getText().toString(), edt_t_register_mgrPhone.getText().toString())
                .enqueue(new Callback<ValidationModel.Success>() {
                    @Override
                    public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body() == null) {
                            } else {
                                if (response.body().isSuccess()) {

                                    FragmentForgotPass.showMsg("تم التسجيل بنجاح", "تسجيل البرنامج", getActivity()).show();
                                } else {
                                    FragmentForgotPass.showMsg("من فضلك أدخل ايميل صحيح ", "تسجيل البرنامج", getActivity()).show();
                                }

                            }
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        }


                    }

                    @Override
                    public void onFailure(Call<ValidationModel.Success> call, Throwable t) {
                        Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });

    }

    private void GetAllCountries() {

        if (countriesManager == null) {
            countriesManager = new CountriesRegisterManager();
        }

        countriesManager.get_all_countries().enqueue(new Callback<List<CountriesRegisterModel>>() {
            @Override
            public void onResponse(Call<List<CountriesRegisterModel>> call, Response<List<CountriesRegisterModel>> response) {

                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body() == null) {
                    } else {

                        for (int i = 0; i < response.body().size(); i++) {
                            CountryNameList.add(response.body().get(i).getName());
                            CountryIdList.add(response.body().get(i).getId());
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<List<CountriesRegisterModel>> call, Throwable t) {

            }
        });
    }
}
