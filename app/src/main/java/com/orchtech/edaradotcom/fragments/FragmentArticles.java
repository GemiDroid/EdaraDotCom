package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.EndlessRecyclerOnScrollListener;
import com.orchtech.edaradotcom.adapters.ArticlesAdapter;
import com.orchtech.edaradotcom.managers.Articles.ArticlesManager;
import com.orchtech.edaradotcom.managers.Products.AuthorsManager;
import com.orchtech.edaradotcom.models.Articles.ArticlesModel;
import com.orchtech.edaradotcom.models.Articles.ArticlesResponse;
import com.orchtech.edaradotcom.models.Products.SearchResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class FragmentArticles extends Fragment {


    static String url;
    RecyclerView rec_articles;
    ArticlesAdapter articlesAdapter;
    ArticlesManager articlesManager;
    int page = 20;
    Spinner spin_article_year, spin_article_author;
    List<String> AuthorsList, YearsList;
    AuthorsManager authorsManager;
    // YearsManager yearsManager;
    List<ArticlesModel> ArticleResponse;
    ProgressBar prog_articles;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //  url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_articles, container, false);

        spin_article_author = (Spinner) view.findViewById(R.id.spin_article_author);
        spin_article_year = (Spinner) view.findViewById(R.id.spin_article_year);
        rec_articles = (RecyclerView) view.findViewById(R.id.rec_articles);
        rec_articles.setLayoutManager(new LinearLayoutManager(getActivity()));
        prog_articles = (ProgressBar) view.findViewById(R.id.prog_articles);

        ArticleResponse = new ArrayList<>();
        articlesAdapter = new ArticlesAdapter(ArticleResponse, getActivity());
        rec_articles.setAdapter(articlesAdapter);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("المقالات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        AuthorsList = new ArrayList<>();
        YearsList = new ArrayList<>();

        getAuthors();
        getYears();

        ArrayAdapter<String> adapter_years = new ArrayAdapter<>
                (getActivity(), android.R.layout.simple_spinner_dropdown_item, YearsList);
        adapter_years.setDropDownViewResource(R.layout.spinner_text);
        spin_article_year.setAdapter(adapter_years);

        ArrayAdapter<String> adapter_author = new ArrayAdapter<>
                (getActivity(), android.R.layout.simple_spinner_dropdown_item, AuthorsList);
        adapter_author.setDropDownViewResource(R.layout.spinner_text);
        spin_article_author.setAdapter(adapter_author);

        rec_articles.addOnScrollListener(new EndlessRecyclerOnScrollListener(new LinearLayoutManager(getActivity())) {
            @Override
            public void onLoadMore(int current_page) {
                page += 10;
                getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
                Log.d(TAG, "onLoadMore: " + page);
                get_articles(page);
            }
        });


        get_articles(page);

        return view;
    }

    private void getYears() {

        int current_year = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < 20; i++) {
            YearsList.add(String.valueOf(current_year - i));
        }
    }

   /*public void DisplayMenuName(String MenuName) {
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText(MenuName);
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
    }*/

    /*private void getYears() {
        if (yearsManager == null) {
            yearsManager = new YearsManager();
        }

        yearsManager.get_all_years().enqueue(new Callback<AuthorResponseModel>() {
            @Override
            public void onResponse(Call<AuthorResponseModel> call, Response<AuthorResponseModel> response) {

                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body().getAuthorList().isEmpty()) {
                        Snackbar.make(getView(),"لا يوجد مؤلفين",Snackbar.LENGTH_SHORT).show();
                    } else {
                        for (int i = 0; i < response.body().getAuthorList().size(); i++) {
                            AuthorsList.add(response.body().getAuthorList().get(i).getName_ar());
                        }
                    }
                }
                else {
                    Snackbar.make(getView(), "مشكله ف السيرفر", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AuthorResponseModel> call, Throwable t) {
                Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });


    }*/

    private void getAuthors() {
        if (authorsManager == null) {
            authorsManager = new AuthorsManager();
        }

        authorsManager.get_all_authors().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body().getAuthorList().isEmpty()) {
                        Snackbar.make(getView(), "لا يوجد مؤلفين", Snackbar.LENGTH_SHORT).show();
                    } else {
                        for (int i = 0; i < response.body().getAuthorList().size(); i++) {
                            AuthorsList.add(response.body().getAuthorList().get(i).getName_ar());
                            Log.d("Author List", ": " + AuthorsList.get(i));
                        }
                    }
                } else {
                    Snackbar.make(getView(), "مشكله ف السيرفر", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    private void get_articles(int page_count) {
        // getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
        if (articlesManager == null) {
            articlesManager = new ArticlesManager();
        }
        prog_articles.setVisibility(View.VISIBLE);
        articlesManager.get_articles("1", page_count, "year", "author")
                .enqueue(new Callback<ArticlesResponse>() {
                    @Override
                    public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
//                        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
                        // ArticleResponse.clear();


                        ArticleResponse.addAll(response.body().getArticles());
                        articlesAdapter.notifyItemRangeInserted(articlesAdapter.getItemCount(), ArticleResponse.size() - 1);
                        articlesAdapter.notifyDataSetChanged();
                        prog_articles.setVisibility(View.GONE);
//                     getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        /*if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                        } else {
                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        }*/
                    }

                    @Override
                    public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                        Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
                        prog_articles.setVisibility(View.GONE);
                    }
                });
    }


}
