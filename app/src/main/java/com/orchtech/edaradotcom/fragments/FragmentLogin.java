package com.orchtech.edaradotcom.fragments;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Account.LoginManager;
import com.orchtech.edaradotcom.models.Fonts;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 12/27/2016.
 */

public class FragmentLogin extends Fragment {

    static String session_id;
    static String url;
    static String TAG = FragmentLogin.class.getSimpleName();
    TextView txt_forget, txt_login;
    Button btn_enter, btn_new, btn_map;
    // private AdView mAdView;
    FragmentManager fragmentManager;
    EditText edt_user, edt_password;

    LoginManager loginManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //   url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();

        loginManager = new LoginManager();

        txt_forget = (TextView) v.findViewById(R.id.txt_forgot);
        txt_login = (TextView) v.findViewById(R.id.txt_login);

        edt_user = (EditText) v.findViewById(R.id.edt_user);
        edt_password = (EditText) v.findViewById(R.id.edt_pass);

        btn_enter = (Button) v.findViewById(R.id.btn_enter);
        btn_new = (Button) v.findViewById(R.id.btn_new_member);
        //   btn_map = (Button) v.findViewById(R.id.btn_map);

    /*    getActivity().findViewById(R.id.img_cart).setTranslationX(-1*4);*/
        //  getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);
        getActivity().findViewById(R.id.img_login_back).setVisibility(View.GONE);
        // btn_enter.setEnabled(true);
        txt_fonts();
/*
        mAdView = (AdView) v.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);*/

        txt_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* startActivity(new Intent(getActivity(), ActivityForgotPassword.class));*/
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentForgotPass()).addToBackStack("forgot").commit();

            }
        });

        btn_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  startActivity(new Intent(getActivity(), ActivityRegister.class));*/
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentRegister()).addToBackStack("register").commit();
            }
        });

       /* btn_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentMapView()).commit();
            }
        });*/

        btn_enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edt_user.getText().toString().equals("")) {
                    edt_user.setHint("من فضلك , ادخل اسمك");
                } else if (edt_password.getText().toString().equals("")) {
                    edt_password.setHint("من فضلك .. ادخل كلمة المرور");
                } else {

                    getActivity().findViewById(R.id.login_progress).setVisibility(View.VISIBLE);
                    loginManager.get_login(edt_user.getText().toString(), edt_password.getText().toString())
                            .enqueue(new Callback<ValidationModel>() {
                                @Override
                                public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {

                                    Log.e(TAG, "onResponse: " + response.code());

                                    if (response.code() >= 200 && response.code() <= 300) {
                                        if (response.body() == null) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                                                Snackbar.make(getView(), "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                                            }
                                            Toast.makeText(getActivity(), "لا توجد بيانات", Toast.LENGTH_SHORT).show();
                                            getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                        } else {
                                            Log.d(TAG, "onResponse: " + response.body().getSuccess().getMessage());
                                            boolean Success = response.body().getSuccess().isSuccess();
                                            String msg = response.body().getSuccess().getMessage();

                                            if (Success) {
                                                session_id = response.body().getAccess().getSession_id();
                                                Preferences.saveInPreference(getActivity(), "session_id", session_id);
                                                Preferences.saveInPreference(getActivity(), "email", edt_user.getText().toString());
                                                Preferences.saveInPreference(getActivity(), "password", edt_password.getText().toString());
                                                Preferences.saveInPreference(getActivity(), "username", response.body().getAccess().getUserName());
                                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                                startActivity(new Intent(getActivity(), ActivityHomePage.class));
                                                // btn_enter.setEnabled(false);

                                            } else {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                                                    Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                                                }
                                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                            }
                                        }
                                    } else {
                                        //   Toast.makeText(getActivity(),"نحن حاليا بصدد تعديلات على هذه الخدمه",Toast.LENGTH_SHORT).show();
                                        //  Snackbar.make(getView(),"نحن حاليا بصدد تعديلات على هذه الخدمه",Snackbar.LENGTH_SHORT).show();
                                        getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                    }
                                }

                                @Override
                                public void onFailure(Call<ValidationModel> call, Throwable t) {
                                    // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                                    Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                                    // } else {
                                    //   Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_failure), Toast.LENGTH_SHORT).show();
                                    // }
                                    getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                    Log.e(TAG, "onFailure: " + t.getMessage());
                                }
                            });


                }


            }
        });

        return v;
    }

    private void txt_fonts() {
        txt_forget.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        txt_login.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_enter.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_new.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
    }


}
