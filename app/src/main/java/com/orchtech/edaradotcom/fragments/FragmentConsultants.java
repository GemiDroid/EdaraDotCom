package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.ConsultantsAdapter;
import com.orchtech.edaradotcom.managers.Question.QuestionsListManager;
import com.orchtech.edaradotcom.managers.Question.QuestionsManager;
import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class FragmentConsultants extends Fragment {


    static String TAG = FragmentConsultants.class.getSimpleName();
    static String Session, txt_field;
    RecyclerView rec_consultants;
    Button btn_ask_consultant;
    FragmentManager fragmentManager;
    QuestionsListManager questionsListManager;
    ConsultantsAdapter consultantsAdapter;
    Spinner spin_all_consultants;
    EditText edt_search_consultant;
    List<String> Consultant_Lists;
    QuestionsManager questionsManager;
    ImageView img_search_consultants;
    ProgressBar prog_consultants;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_consultants, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاستشارات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        prog_consultants = (ProgressBar) v.findViewById(R.id.prog_consultants);

        img_search_consultants = (ImageView) v.findViewById(R.id.img_search_consultants);
        spin_all_consultants = (Spinner) v.findViewById(R.id.spin_all_consultants);
        edt_search_consultant = (EditText) v.findViewById(R.id.edt_search_consultants);

        rec_consultants = (RecyclerView) v.findViewById(R.id.rec_consultants);
        rec_consultants.setLayoutManager(new LinearLayoutManager(getActivity()));


        btn_ask_consultant = (Button) v.findViewById(R.id.btn_ask_consultant);

        Consultant_Lists = new ArrayList<>();

        GetQuestionList();

        ArrayAdapter<String> spin_adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, Consultant_Lists);
        spin_adapter.setDropDownViewResource(R.layout.spinner_text);
        spin_all_consultants.setAdapter(spin_adapter);

        btn_ask_consultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentManager.beginTransaction().
                        replace(R.id.frame_home, new FragmentAskConsultant()).
                        addToBackStack("ask_consultant").commit();

            }
        });

        img_search_consultants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_field = edt_search_consultant.getText().toString().equals("") ? "" : edt_search_consultant.getText().toString();
                if (questionsManager == null) {
                    questionsManager = new QuestionsManager();
                }
                prog_consultants.setVisibility(View.VISIBLE);
                questionsManager.get_questions(1, 50, false, txt_field, Session).enqueue(new Callback<QuestionsListResponse>() {
                    @Override
                    public void onResponse(Call<QuestionsListResponse> call, Response<QuestionsListResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {

                            if (response.body() == null || response.body().getQuestionList() == null || response.body().getQuestionList().isEmpty()) {
                                Snackbar.make(getView(), "لا توجد استشارات", Snackbar.LENGTH_SHORT).show();
                                rec_consultants.setVisibility(GONE);
                                prog_consultants.setVisibility(View.GONE);
                            } else {
                                rec_consultants.setVisibility(View.VISIBLE);
                                consultantsAdapter = new ConsultantsAdapter(getActivity(), response.body().getQuestionList());
                                rec_consultants.setAdapter(consultantsAdapter);
                                prog_consultants.setVisibility(View.GONE);
                            }
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                            prog_consultants.setVisibility(View.GONE);
                        }


                    }

                    @Override
                    public void onFailure(Call<QuestionsListResponse> call, Throwable t) {

                        Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                        prog_consultants.setVisibility(View.GONE);
                    }
                });
            }
        });


        return v;
    }


    private void GetQuestionList() {

        // getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
        if (questionsListManager == null) {
            questionsListManager = new QuestionsListManager();
        }


        questionsListManager.get_question_list(100).enqueue(new Callback<QuestionsListResponse>() {
            @Override
            public void onResponse(Call<QuestionsListResponse> call, Response<QuestionsListResponse> response) {


                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() != null) {
                        Log.d(TAG, "onResponse: " + response.body().getQuestionList().size());
                        Log.d(TAG, "Get ID: " + response.body().getQuestionList().get(0).getQuestion_id());


                        consultantsAdapter = new ConsultantsAdapter(getActivity(), response.body().getQuestionList());
                        rec_consultants.setAdapter(consultantsAdapter);
                        prog_consultants.setVisibility(View.GONE);

                        for (int i = 0; i < response.body().getQuestionList().size(); i++) {
                            Consultant_Lists.add(response.body().getQuestionList().get(i).getTitle());
                        }
//                        getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);

                   /* for (int i = 0; i < response.body().getQuestionList().size(); i++) {
                        Question_Ids.add(response.body().getQuestionList().get(i).getQuestion_id());
                    }*/
                    } else {
                        Snackbar.make(getView(), "لا توجد اسئله", Snackbar.LENGTH_SHORT).show();
                        prog_consultants.setVisibility(View.GONE);
                        //   getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    prog_consultants.setVisibility(View.GONE);
                    //   getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<QuestionsListResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                prog_consultants.setVisibility(View.GONE);
            }
        });

    }


}
