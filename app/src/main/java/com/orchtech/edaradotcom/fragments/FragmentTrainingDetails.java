package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Training.TrainingDetailsManager;
import com.orchtech.edaradotcom.models.TrainingDetailsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class FragmentTrainingDetails extends Fragment {


    static String PlanId;
    static Bundle b;
    TextView txt_training_title, txt_training_period, txt_training_from_to, txt_training_city, txt_training_language;
    LinearLayout linear_training_contents;
    ImageView img_details;
    TextView txt_training_details, txt_training_register, txt_training_inquires, txt_training_registerr;
    FragmentManager fragmentManager;
    TrainingDetailsManager trainingDetailsManager;
    RecyclerView rec_training_content, rec_training_goals;
    TextView txt_training_intro, txt_training_goals, txt_training_contents;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_training_details, container, false);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("تفاصيل البرنامج");
        FragmentSideMenu.showDefaultFooter(getActivity());

        linear_training_contents = (LinearLayout) v.findViewById(R.id.lin_training_contents);
        linear_training_contents.setVisibility(View.VISIBLE);

        txt_training_details = (TextView) v.findViewById(R.id.txt_training_details);
        txt_training_register = (TextView) v.findViewById(R.id.txt_training_register);


        txt_training_inquires = (TextView) v.findViewById(R.id.txt_training_inquires);
        txt_training_registerr = (TextView) v.findViewById(R.id.txt_training_registerr);

        img_details = (ImageView) v.findViewById(R.id.img_details);


        txt_training_details.setText("إستعلام");
        img_details.setImageResource(R.drawable.ic_training_inquiries);


        txt_training_title = (TextView) v.findViewById(R.id.txt_training_title);
        txt_training_from_to = (TextView) v.findViewById(R.id.txt_training_from_to);
        txt_training_period = (TextView) v.findViewById(R.id.txt_training_period);
        txt_training_city = (TextView) v.findViewById(R.id.txt_training_place);
        txt_training_language = (TextView) v.findViewById(R.id.txt_training_language);

        txt_training_intro = (TextView) v.findViewById(R.id.txt_training_intro);
        txt_training_contents = (TextView) v.findViewById(R.id.txt_training_contents);
        txt_training_goals = (TextView) v.findViewById(R.id.txt_training_goals);

        rec_training_goals = (RecyclerView) v.findViewById(R.id.rec_training_goals);
        rec_training_content = (RecyclerView) v.findViewById(R.id.rec_training_contents);

        rec_training_goals.setLayoutManager(new LinearLayoutManager(getActivity()));
        rec_training_content.setLayoutManager(new LinearLayoutManager(getActivity()));


        try {

            Bundle i = getArguments();
            PlanId = i.getString("planId");
            txt_training_title.setText(i.getString("training_title"));
            txt_training_from_to.setText(i.getString("training_from_to"));
            txt_training_period.setText(i.getString("training_period"));
            txt_training_city.setText(i.getString("training_place"));
            txt_training_language.setText("لغة البرنامج العربية");

        } catch (Exception e) {
        } finally {
            b = new Bundle();
            b.putString("planId", PlanId);
            b.putString("training_from_to", txt_training_from_to.getText().toString());
            b.putString("training_period", txt_training_period.getText().toString());
            //  b.putString("training_language", holder.txt_training_language.getText().toString());
            b.putString("training_place", txt_training_city.getText().toString());
            b.putString("training_title", txt_training_title.getText().toString());

        }

        txt_training_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavigateToInquiry();
            }
        });

        txt_training_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavigateToRegisteration();
            }
        });


        txt_training_inquires.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavigateToInquiry();
            }
        });

        txt_training_registerr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NavigateToRegisteration();
            }
        });


        DisplayTrainingDetails();


        return v;
    }

    private void NavigateToInquiry() {
        Fragment fm = new FragmentTrainingInquiry();
        fm.setArguments(b);

        fragmentManager.beginTransaction().replace(R.id.frame_home, fm)
                .addToBackStack("training_inquiry").commit();
    }

    private void NavigateToRegisteration() {
        Fragment fm = new FragmentTrainingRegistration();
        fm.setArguments(b);
        fragmentManager.beginTransaction().replace(R.id.frame_home, fm)
                .addToBackStack("training_register").commit();
    }


    private void DisplayTrainingDetails() {


        if (trainingDetailsManager == null) {
            trainingDetailsManager = new TrainingDetailsManager();
        }

        trainingDetailsManager.getTrainingDetails(PlanId).enqueue(new Callback<TrainingDetailsResponse>() {
            @Override
            public void onResponse(Call<TrainingDetailsResponse> call, Response<TrainingDetailsResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getTrainingDetailsModel() == null || response.body() == null) {
                    } else {

                        txt_training_intro.setText(Html.fromHtml(response.body().getTrainingDetailsModel().getIntro()));

                        txt_training_goals.setText(Html.fromHtml(response.body().getTrainingDetailsModel().getGoals()));
                        txt_training_contents.setText(Html.fromHtml(response.body().getTrainingDetailsModel().getContents()));

                        /*moassyAdapter = new MoassyAdapter(response.body().getTrainingDetailsModel().getContents(), getActivity());
                        rec_training_content.setAdapter(moassyAdapter);
                        moassyAdapter2 = new MoassyAdapter(response.body().getTrainingDetailsModel().getGoals(), getActivity());
                        rec_training_goals.setAdapter(moassyAdapter2);*/
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<TrainingDetailsResponse> call, Throwable t) {

                Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure),
                        Snackbar.LENGTH_SHORT).show();

            }
        });

    }
}
