package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.ProductsAdapter;
import com.orchtech.edaradotcom.managers.Khoulasat.MostVisitedManager;
import com.orchtech.edaradotcom.managers.Khoulasat.RecentBooksManager;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class FragmentKhoulasatArabicBooks extends Fragment {


    static String SessionId;
    final int ProductTypeId = 5;
    MostVisitedManager mostVisitedManager;
    RecentBooksManager recentBooksManager;
    ProductsAdapter productsAdapter;
    RecyclerView rec_edition_recent, rec_most_recent;
    Spinner spin_edition_year;
    TextView txt_recent_books, txt_most_sales, txt_recent_books_colorful, txt_most_sales_colorful;
    List<Integer> IssuedYearList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        SessionId = Preferences.getFromPreference(getActivity(), "session_id");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragmnent_khoulasat_arabic_books, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الكتب العربيه");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.VISIBLE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        spin_edition_year = (Spinner) v.findViewById(R.id.spin_edition_year);

        txt_recent_books = (TextView) v.findViewById(R.id.txt_recent_books);
        txt_most_sales = (TextView) v.findViewById(R.id.txt_more_sales);

        txt_recent_books_colorful = (TextView) v.findViewById(R.id.txt_recent_colorful);
        txt_most_sales_colorful = (TextView) v.findViewById(R.id.txt_more_sales_colorful);

        rec_edition_recent = (RecyclerView) v.findViewById(R.id.rec_edition_recent);
        rec_most_recent = (RecyclerView) v.findViewById(R.id.rec_most_recent);

        GridLayoutManager mLayoutGrid = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);
        GridLayoutManager mLayoutGrid2 = new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false);

        rec_edition_recent.setLayoutManager(mLayoutGrid);
        rec_most_recent.setLayoutManager(mLayoutGrid2);


        txt_most_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                txt_recent_books_colorful.setVisibility(View.GONE);
                txt_most_sales_colorful.setVisibility(View.VISIBLE);

                if (mostVisitedManager == null) {
                    mostVisitedManager = new MostVisitedManager();
                }
                mostVisitedManager.get_most_visited(ProductTypeId, 50, SessionId)
                        .enqueue(new Callback<ProductsResponse>() {
                            @Override
                            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                                if (response.code() >= 200 && response.code() < 300) {

                                    if (response.body().getItems() == null || response.body() == null) {
                                    } else {
                                        productsAdapter = new ProductsAdapter(response.body().getItems(), getActivity());
                                        rec_edition_recent.setAdapter(productsAdapter);
                                    }

                                } else {
                                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                }
                            }

                            @Override
                            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                                Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                            }
                        });

            }
        });

        txt_recent_books.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txt_recent_books_colorful.setVisibility(View.VISIBLE);
                txt_most_sales_colorful.setVisibility(GONE);

                DisplayMostRecentBooks();

            }
        });


        IssuedYearList = new ArrayList<>();
        GetIssuedYears();

        ArrayAdapter<Integer> YearAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, IssuedYearList);
        YearAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_edition_year.setAdapter(YearAdapter);


        getActivity().findViewById(R.id.img_kholasat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                FragmentKhoulasatManager.InfoDialog(ProductTypeId, getActivity(), v);
            }
        });

        DisplayMostRecentBooks();

        return v;

    }

    private void DisplayMostRecentBooks() {

        if (recentBooksManager == null) {
            recentBooksManager = new RecentBooksManager();
        }
        recentBooksManager.get_recent_books(50, ProductTypeId, SessionId)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {

                            if (response.body().getItems() == null || response.body() == null) {
                            } else {
                                productsAdapter = new ProductsAdapter(response.body().getItems(), getActivity());
                                rec_most_recent.setAdapter(productsAdapter);
                            }

                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });
    }


    public void GetIssuedYears() {

        int CurrentYear = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < 30; i++) {
            IssuedYearList.add(CurrentYear - i);
        }
    }


}
