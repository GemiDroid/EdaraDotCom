package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class FragmentPayment extends Fragment {


    ImageView img_bank_transfer, img_western_union;

    FragmentManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        manager = getActivity().getSupportFragmentManager();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_payment, container, false);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الدفع");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());


        img_western_union = (ImageView) v.findViewById(R.id.bank_western);
        img_bank_transfer = (ImageView) v.findViewById(R.id.bank_transfer);


        img_bank_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manager.beginTransaction().replace(R.id.frame_home, new FragmentBankTransfer())
                        .addToBackStack("bank_transfer").commit();

                //
            }
        });

        img_western_union.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manager.beginTransaction().replace(R.id.frame_home, new FragmentWestern())
                        .addToBackStack("western_union").commit();

                //
            }
        });


        return v;
    }
}
