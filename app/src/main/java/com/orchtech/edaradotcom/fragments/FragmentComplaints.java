package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.ComplaintsManager;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class FragmentComplaints extends Fragment {

    static String Name;
    ComplaintsManager complaintsManager;
    EditText edt_complain_name, edt_complain_email, edt_complain_message, edt_complain_phone;
    RadioButton rd_complain, rd_suggestion;
    Button btn_complaint_send;
    boolean status1 = false;
    boolean status2 = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        Name = Preferences.getFromPreference(getActivity(), "username");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_complaints, container, false);

        edt_complain_name = (EditText) v.findViewById(R.id.edt_complaint_name);
        edt_complain_email = (EditText) v.findViewById(R.id.edt_complaint_email);
        edt_complain_phone = (EditText) v.findViewById(R.id.edt_complaint_phone);
        edt_complain_message = (EditText) v.findViewById(R.id.edt_complaint_message);

        rd_complain = (RadioButton) v.findViewById(R.id.rd_complain);
        rd_suggestion = (RadioButton) v.findViewById(R.id.rd_suggestion);

        btn_complaint_send = (Button) v.findViewById(R.id.btn_complain_send);

        edt_complain_name.setText(Name);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الشكاوي و المقترحات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        btn_complaint_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendComplaint();
            }
        });

        return v;

    }

    private void SendComplaint() {

        if (edt_complain_email.getText().toString().equals("")) {
            edt_complain_email.setError("أدخل الايميل");
        } else if (!FragmentForgotPass.check_email(edt_complain_email.getText().toString())) {
            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحة", "الشكاوي و المقترحات", getActivity()).show();
        } else if (edt_complain_phone.getText().toString().equals("")) {
            edt_complain_phone.setError("أدخل رقم الهاتف");
        } else {
            if (complaintsManager == null) {
                complaintsManager = new ComplaintsManager();
            }


            final String Name = edt_complain_name.getText().toString().equals("") ? "" : edt_complain_name.getText().toString().trim();
            final String Email = edt_complain_email.getText().toString().equals("") ? "" : edt_complain_email.getText().toString();
            final String Phone = edt_complain_phone.getText().toString().equals("") ? "" : edt_complain_phone.getText().toString();
            final String Message = edt_complain_message.getText().toString().equals("") ? "null" : edt_complain_message.getText().toString();

            if (rd_complain.isChecked()) {
                status1 = true;
                status2 = false;
            } else {
                status1 = false;
                status2 = true;
            }

            complaintsManager.get_complaints(Name, Email
                    , Message, status1, status2,
                    Phone).enqueue(new Callback<ValidationModel>() {
                @Override
                public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {

                    if (response.code() >= 200 && response.code() < 300) {
                        if (response.body().getSuccess() == null) {
                        } else {
                            if (response.body().getSuccess().isSuccess()) {
                                FragmentForgotPass.showMsg("تم الارسال ", "الشكاوي و المقترحات", getActivity()).show();
                            } else {
                                Log.e("Error", "onResponse: " + status1 + status2 + Name + Email + Message + status1 + status2 + Phone);
                                Log.e("Error", "onResponse: " + response.body().getSuccess().getMessage());
                                FragmentForgotPass.showMsg(response.body().getSuccess().getMessage(), "الشكاوي و المقترحات", getActivity()).show();
                            }
                        }
                    } else {
                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    }
                }

                @Override
                public void onFailure(Call<ValidationModel> call, Throwable t) {

                    Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                }
            });


        }
    }
}
