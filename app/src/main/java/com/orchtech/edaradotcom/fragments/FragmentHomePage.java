package com.orchtech.edaradotcom.fragments;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.activities.EndlessRecyclerOnScrollListener;
import com.orchtech.edaradotcom.adapters.ProductsAdapter;
import com.orchtech.edaradotcom.managers.Products.FreeProductsManager;
import com.orchtech.edaradotcom.managers.Products.RecentProductsManager;
import com.orchtech.edaradotcom.models.Products.ProductsModel;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ahmed.yousef on 1/3/2017.
 */
public class FragmentHomePage extends Fragment {

    static boolean checker_new = false, checker_free = false;
    static boolean flag = false;
    static String Session;
    static int x;
    static int page_count = 9;
    ProductsAdapter productssAdapter;
    RecyclerView rec_versions;
    Configuration configue;
    LinearLayout lin_new, lin_free;
    TextView txt_new_colorful, txt_free_colorful, txt_newest, txt_free;
    // ProgressBar loading_bar;
    ProgressBar loading_bar;
    LinearLayout lin_menu;
    ImageView img_home_menu;
    FrameLayout frameLayout_home_page;
    List<ProductsModel> productsResponse;
    RecentProductsManager recentProductsManager = null;
    FreeProductsManager freeProductsManager = null;
    EditText txt_search;
    FragmentManager fragmentManager;
    ImageView img_home_search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        configue = getResources().getConfiguration();
        fragmentManager = getActivity().getSupportFragmentManager();
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home_page, container, false);
        // BaseURL= Preferences.getUrlHeader();
        /*loading_bar = (ProgressBar) view.findViewById(R.id.loading_bar);
        loading_bar.setBackgroundResource(R.drawable.search_loading);
*/

        txt_search = (EditText) view.findViewById(R.id.txt_search);
        loading_bar = (ProgressBar) getActivity().findViewById(R.id.home_progress);
        // Glide.with(getActivity()).load(R.drawable.search_loading).asGif().into(loading_bar);

        txt_free_colorful = (TextView) view.findViewById(R.id.txt_free_colorful);
        txt_new_colorful = (TextView) view.findViewById(R.id.txt_new_colorful);

        txt_free = (TextView) view.findViewById(R.id.free_versions);
        txt_newest = (TextView) view.findViewById(R.id.newest_versions);

/*img_home_menu = (ImageView) view.findViewById(R.id.img_home_menu);*/

        lin_new = (LinearLayout) view.findViewById(R.id.lin_newest_versions);
        lin_free = (LinearLayout) view.findViewById(R.id.lin_free_versions);

        frameLayout_home_page = (FrameLayout) view.findViewById(R.id.fragment_main);

        //  lin_menu = (LinearLayout) view.findViewById(R.id.lin_layout_menu);
        rec_versions = (RecyclerView) view.findViewById(R.id.rec_versions);


   /*   if (configue.screenWidthDp <= 320) {
            x = 1;
        } else*/
        if (configue.screenWidthDp <= 480) {
            x = 2;
        } else if (configue.screenWidthDp >= 600) {
            x = 4;
        } else {
        }
        LinearLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), x);
        rec_versions.setLayoutManager(gridLayoutManager);


        // Loading Default Versions ...

        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.GONE);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
      /*  getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);*/

        img_home_search = (ImageView) view.findViewById(R.id.img_home_search);

        getActivity().findViewById(R.id.img_cart).setTranslationX(30);

        lin_free.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_free_versions();
            }
        });

        lin_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_newest_values(page_count);
            }
        });
//        img_home_menu.setOnClickListener(this);

        ActivityHomePage.BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library2, R.drawable.home, R.drawable.my_profile2);

        img_home_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("search_key", txt_search.getText().toString());
                Fragment fragment_search = new FragmentSearch();
                fragment_search.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.frame_home, fragment_search).commit();
            }
        });


        rec_versions.setItemAnimator(new DefaultItemAnimator());

        productsResponse = new ArrayList<>();
        productssAdapter = new ProductsAdapter(productsResponse, getActivity());
        rec_versions.setAdapter(productssAdapter);

        rec_versions.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page_count += 9;
                loading_bar.setVisibility(View.VISIBLE);
                if (txt_free_colorful.getVisibility() == View.VISIBLE) {
                } else {
                    get_newest_values(page_count);
                }
            }
        });


        get_newest_values(page_count);


        return view;
    }

    private void get_free_versions() {

        txt_free_colorful.setVisibility(View.VISIBLE);
        txt_new_colorful.setVisibility(View.GONE);


        if (freeProductsManager == null) {
            freeProductsManager = new FreeProductsManager();
        }
        loading_bar.setVisibility(View.VISIBLE);
        try {
            freeProductsManager.get_free_products(Session, 4).enqueue(new Callback<ProductsResponse>() { // "",

                @Override
                public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getItems().isEmpty()) {
                            Toast.makeText(getActivity(), "There aren't any products yet ", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("Success", "onResponse: " + response.body().getItems().size());

                            // productsResponse.clear();
/*                                productsResponse.addAll(response.body().getItems());
                                productssAdapter.notifyItemRangeInserted(productssAdapter.getItemCount(), productsResponse.size() - 1);
*//*
                                */
                            productssAdapter = new ProductsAdapter(response.body().getItems(), getActivity());
                            rec_versions.setAdapter(productssAdapter);
                            productssAdapter.notifyDataSetChanged();


                            rec_versions.setVisibility(View.VISIBLE);
                        }
                    } /*else if (response.code() == 404) {
                        // Server Error
                        Toast.makeText(getActivity(), "The server can not find the requested page.", Toast.LENGTH_SHORT).show();
                        rec_versions.setVisibility(View.GONE);
                    }*/ else {
                        // Toast.makeText(getActivity(), "There is a problem in server", Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(getActivity(), "There is a problem with this page you requested", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        rec_versions.setVisibility(View.GONE);
                    }
                    loading_bar.setVisibility(View.GONE);
                    checker_free = true;
                    checker_new = false;
                }

                @Override
                public void onFailure(Call<ProductsResponse> call, Throwable t) {
                    Log.e("Failure", "onFailure: " + t.getMessage());
                    Toast.makeText(getActivity(), "No Internet Access", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_failure), Toast.LENGTH_SHORT).show();
                    }
                    loading_bar.setVisibility(View.GONE);
                    rec_versions.setVisibility(View.GONE);
                }
            });


        } catch (Exception e) {
            loading_bar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "There is a problem with this page you requested", Toast.LENGTH_SHORT).show();

        }


    }


    private void get_newest_versions() {
        if (checker_new) {
            Toast.makeText(getActivity(), "انت بالفعل في قائمه أحدث الاصدارات ", Toast.LENGTH_SHORT).show();
            loading_bar.setVisibility(View.GONE);
            checker_new = true;
            checker_free = false;

        } else {
            if (txt_new_colorful.getVisibility() == View.GONE) {
                txt_new_colorful.setVisibility(View.VISIBLE);
                txt_free_colorful.setVisibility(View.GONE);
            }
            try {
                get_newest_values(page_count);
               /* productsResponse.addAll(response.body().getItems());
                productssAdapter.notifyItemRangeInserted(productssAdapter.getItemCount(),productsResponse.size()-1);
*/

            } catch (Exception e) {
                Toast.makeText(getActivity(), "There is a problem with this page you requested", Toast.LENGTH_SHORT).show();
                checker_new = true;
                checker_free = false;
                loading_bar.setVisibility(View.GONE);
            }


        }
    }

    private void get_newest_values(int page) {


       /* ImageView img = (ImageView) getActivity().findViewById(R.id.img_main);
        img.setImageResource(R.drawable.home);
*/
        txt_new_colorful.setVisibility(View.VISIBLE);
        txt_free_colorful.setVisibility(View.GONE);


        if (recentProductsManager == null) {
            recentProductsManager = new RecentProductsManager();
        }
        // productsResponse.clear();

        recentProductsManager.get_recent_products(page, 4, Session).enqueue(new Callback<ProductsResponse>() {

            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.code() >= 200 && response.code() <= 300) {
                    if (response.body().getItems().isEmpty() || response.body().equals("")) {
                        Toast.makeText(getActivity(), "There aren't any products yet ", Toast.LENGTH_SHORT).show();
                    } else {
                        Log.d("Success", "onResponse: " + response.body().getItems().size());

                        productsResponse.addAll(response.body().getItems());
                        productssAdapter.notifyItemRangeInserted(productssAdapter.getItemCount(), productsResponse.size() - 1);
                        productssAdapter.notifyDataSetChanged();
                        rec_versions.setVisibility(View.VISIBLE);

//                        Toast.makeText(getActivity(),productssAdapter.getProductId(),Toast.LENGTH_SHORT).show();

                    }
                }/* else if (response.message().contentEquals("404 - File or directory not found")) {
                            // Server Error
                            Toast.makeText(getActivity(), "There is a problem in server", Toast.LENGTH_SHORT).show();
                            rec_versions.setVisibility(View.GONE);
                        }*/ else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    // Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();
                    //    Toast.makeText(getActivity(), "There is a problem with this page you requested", Toast.LENGTH_SHORT).show();
                    rec_versions.setVisibility(View.GONE);
                }
                loading_bar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                Log.e("Failure", "onFailure: " + t.getMessage());
                loading_bar.setVisibility(View.GONE);
                rec_versions.setVisibility(View.GONE);
                // Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_failure), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


}
