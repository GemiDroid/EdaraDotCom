package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.ContactUsAdapter;
import com.orchtech.edaradotcom.managers.CommunicateWithUs.ContactUsManager;
import com.orchtech.edaradotcom.models.ContactUs.ContactUs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */

public class FragmentContactUs extends Fragment {

    static String url;
    ContactUsManager contactUsManager;
    RecyclerView rec_contact_us;
    ContactUsAdapter adapter;
    ProgressBar prog_contact;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_contact_us, container, false);


        rec_contact_us = (RecyclerView) view.findViewById(R.id.rec_contact_us);
        FragmentSideMenu.showDefaultFooter(getActivity());

        /*getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText("اتصل بنا");
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
*/
        rec_contact_us.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);

        if (contactUsManager == null) {
            contactUsManager = new ContactUsManager();
        }
        contactUsManager.get_call_us().enqueue(new Callback<ContactUs>() {
            @Override
            public void onResponse(Call<ContactUs> call, Response<ContactUs> response) {


                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body() == null) {
                        Snackbar.make(view, "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                    } else {

                        adapter = new ContactUsAdapter(response.body().getContacts(), getActivity());
                        rec_contact_us.setAdapter(adapter);
                    }
                } else {
                    // Snackbar.make(view, "مشكله ف السيرفر", Snackbar.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }


                if (getActivity().findViewById(R.id.home_progress).isFocusable()) {
                } else {
                    getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<ContactUs> call, Throwable t) {

                Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("اتصل بنا");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        return view;
    }
}
