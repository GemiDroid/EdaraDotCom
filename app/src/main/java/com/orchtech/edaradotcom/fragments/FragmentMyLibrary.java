package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 1/17/2017.
 */

public class FragmentMyLibrary extends Fragment {


    ImageView img_lib1, img_lib2, img_lib3, img_lib4, img_lib5, img_lib6, img_lib7, img_lib8, img_lib9, img_lib10, img_lib11;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_my_library, container, false);


        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("مكتبتي الخاصة");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);

        InitFilters(v);


      /*  getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);
*/
        ActivityHomePage.BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library, R.drawable.home2, R.drawable.my_profile2);

        return v;
    }

    private void InitFilters(View v) {
        img_lib1 = (ImageView) v.findViewById(R.id.img_filter_1);
        img_lib2 = (ImageView) v.findViewById(R.id.img_filter_2);
        img_lib3 = (ImageView) v.findViewById(R.id.img_filter_3);
        img_lib4 = (ImageView) v.findViewById(R.id.img_filter_4);
        img_lib5 = (ImageView) v.findViewById(R.id.img_filter_5);
        img_lib6 = (ImageView) v.findViewById(R.id.img_filter_6);
        img_lib7 = (ImageView) v.findViewById(R.id.img_filter_7);
        img_lib8 = (ImageView) v.findViewById(R.id.img_filter_8);
        img_lib9 = (ImageView) v.findViewById(R.id.img_filter_9);
        ;

        Glide.with(getActivity()).load(R.drawable.manager_khoulasast).into(img_lib1);
        Glide.with(getActivity()).load(R.drawable.education_khoulasat).into(img_lib2);
        Glide.with(getActivity()).load(R.drawable.manager_mokhtar).into(img_lib3);
        Glide.with(getActivity()).load(R.drawable.relations).into(img_lib4);
        Glide.with(getActivity()).load(R.drawable.arabic_books).into(img_lib5);
        Glide.with(getActivity()).load(R.drawable.management_in_car).into(img_lib6);
        ;
        Glide.with(getActivity()).load(R.drawable.important_secret).into(img_lib7);
        Glide.with(getActivity()).load(R.drawable.your_health).into(img_lib8);
        Glide.with(getActivity()).load(R.drawable.free_library).into(img_lib9);


    }
}
