package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.YourSubscriptionAdapter;
import com.orchtech.edaradotcom.managers.Products.MySubscriptionManager;
import com.orchtech.edaradotcom.models.Products.MySubscriptionResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class FragmentYourSubscription extends Fragment {


    static String Session;
    RecyclerView rec_your_subscription;
    YourSubscriptionAdapter yourSubscriptionAdapter;
    MySubscriptionManager mySubscriptionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_your_subscription, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView tv = (TextView) getActivity().findViewById(R.id.txt_logo);
        tv.setText("إشتراكك");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        ImageView im = (ImageView) getActivity().findViewById(R.id.img_account);
        im.setImageResource(R.drawable.my_profile2);

        rec_your_subscription = (RecyclerView) v.findViewById(R.id.rec_your_subscription);
        rec_your_subscription.setLayoutManager(new LinearLayoutManager(getActivity()));
        rec_your_subscription.setItemAnimator(new DefaultItemAnimator());

        if (mySubscriptionManager == null) {
            mySubscriptionManager = new MySubscriptionManager();
        }

        mySubscriptionManager.get_your_subscription(Session).enqueue(new Callback<MySubscriptionResponse>() {
            @Override
            public void onResponse(Call<MySubscriptionResponse> call, Response<MySubscriptionResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getMySubscriptionModelList() == null || response.body() == null) {
                        Snackbar.make(getView(), "لا توجد لك اشتراكات", Snackbar.LENGTH_SHORT).show();
                    } else {
                        yourSubscriptionAdapter = new YourSubscriptionAdapter(response.body().getMySubscriptionModelList(), getActivity());
                        rec_your_subscription.setAdapter(yourSubscriptionAdapter);
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    //   Snackbar.make(getView(),"نواجه اصلاحات في الموقع",Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MySubscriptionResponse> call, Throwable t) {

                Snackbar.make(getView(), "مشكله في الانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });

        return v;

    }
}
