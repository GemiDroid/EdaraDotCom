package com.orchtech.edaradotcom.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Account.ForgotPassManager;
import com.orchtech.edaradotcom.models.Fonts;
import com.orchtech.edaradotcom.models.ValidationModel;

import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 12/28/2016.
 */
public class FragmentForgotPass extends Fragment {


    static String url;
    static String TAG = FragmentForgotPass.class.getSimpleName();
    TextView txt_forgot_pass;
    Button btn_send, btn_cancel;
    FragmentManager fragmentManager;
    ForgotPassManager forgotPassManager;
    EditText edt_email;

    public static boolean check_email(String Email) {

        // Pattern.matches(Email, Patterns.EMAIL_ADDRESS.pattern());
        if (Pattern.matches(Patterns.EMAIL_ADDRESS.pattern(), Email)) {
            return true;
        } else {
            return false;
        }

    }

    public static AlertDialog showMsg(String Message, String Title, Context context) {
        AlertDialog d = new AlertDialog.Builder(context).create();
        TextView tv = new TextView(context);
        tv.setTextSize(25);
        tv.setTextColor(context.getResources().getColor(R.color.black_color));
        tv.setGravity(Gravity.CENTER);
        tv.setText(Title);
        d.setCustomTitle(tv);
        d.setMessage(Message);
        d.setButton(AlertDialog.BUTTON_POSITIVE, "حسنا", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return d;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_forgot_pass, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();

        txt_forgot_pass = (TextView) v.findViewById(R.id.txt_forgot_pass);
        btn_send = (Button) v.findViewById(R.id.btn_send);
        btn_cancel = (Button) v.findViewById(R.id.btn_cancel);
        edt_email = (EditText) v.findViewById(R.id.edt_email);

        txt_forgot_pass.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Bold"));
        btn_send.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_cancel.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));

      /*  getActivity().findViewById(R.id.img_login_back).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_login_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentLogin()).commit();
            }
        });*/


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentLogin()).commit();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edt_email.getText().toString().equals("")) {
                    edt_email.setError("من فضلك أدخل ايميلك");
                } else if (!check_email(edt_email.getText().toString())) {
                    showMsg("صيغة الايميل غير صحيحه", "نسيت كلمة السر", getActivity()).show();
                    Snackbar.make(getView(), "صيغة الايميل غير صحيحه", Snackbar.LENGTH_LONG).show();
                } else {
                    if (forgotPassManager == null) {
                        forgotPassManager = new ForgotPassManager();

                        getActivity().findViewById(R.id.login_progress).setVisibility(View.VISIBLE);
                        forgotPassManager.get_lost_pass(edt_email.getText().toString())
                                .enqueue(new Callback<ValidationModel>() {
                                    @Override
                                    public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {

                                        if (response.code() >= 200 && response.code() <= 300) {

                                            if (response.body() == null) {
                                                Snackbar.make(getView(), "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                                            }

                                            Log.d(TAG, "onResponse: " + response.body().getSuccess().getMessage());
                                            boolean Success = response.body().getSuccess().isSuccess();
                                            String msg = response.body().getSuccess().getMessage();
                                            if (Success) {
                                                showMsg(msg + "\n\t من فضلك تفحص ايميلك", "نسيت كلمة السر", getActivity()).show();
                                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                                edt_email.getText().clear();
                                            } else {
                                                showMsg(msg, "نسيت كلمة السر", getActivity()).show();
                                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                            }
                                        } else {
                                            // Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();
                                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                            getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ValidationModel> call, Throwable t) {
                                        Snackbar.make(getView(), "من فضلك تأكد من اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                                        getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                                    }
                                });
                    }
                }
            }
        });
        return v;
    }
}
