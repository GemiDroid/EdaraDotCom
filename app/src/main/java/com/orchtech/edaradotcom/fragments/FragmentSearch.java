package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Products.AuthorsManager;
import com.orchtech.edaradotcom.managers.Products.ProductTypesManager;
import com.orchtech.edaradotcom.managers.Products.SubjectsManager;
import com.orchtech.edaradotcom.models.Fonts;
import com.orchtech.edaradotcom.models.Products.SearchResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 1/2/2017.
 */
public class FragmentSearch extends android.support.v4.app.Fragment {


    static String selected_author;
    static String selected_product_type;
    static String selected_subject;
    static String selected_from_year;
    static String selected_to_year;


    static String edt_subject_data, edt_body_data;
    String TAG = FragmentSearch.class.getSimpleName();
    Button btn_search, btn_clear_all;
    Spinner spin_all_authors, spin_all_products, spin_all_subjectTypes, spin_fromYear, spin_toYear;
    EditText edt_body, edt_subject;
    AuthorsManager authorsManager;
    SubjectsManager subjectsManager;
    ProductTypesManager productTypesManager;
    List<String> AuthorList;
    List<String> ProductTypesList;
    List<String> SubjectsList;
    List<Integer> DateFromList;
    List<Integer> DateToList;
    FragmentManager fragmentManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search, container, false);


        edt_body = (EditText) view.findViewById(R.id.edt_body);
        edt_subject = (EditText) view.findViewById(R.id.edt_subject);

        //    txt_search = (TextView) view.findViewById(R.id.txt_search);
        btn_clear_all = (Button) view.findViewById(R.id.btn_clear_all);

        btn_search = (Button) view.findViewById(R.id.btn_search);


        visibilties();

        try {
            Bundle bundle = getArguments();
            if (bundle.getString("search_key") == null) {
            } else {
                edt_body.setText(bundle.getString("search_key"));
            }
        } catch (Exception e) {
        }

        SpinnersAndListsDeclaration(view);

        InitializeSpinners();

        SpinnersAdapters();


        SpinnersEvents();

        draw_fonts();


        edt_subject_data = edt_subject.getText().toString().equals("") ?
                "" : edt_subject.getText().toString();
        edt_body_data = edt_body.getText().toString().equals("") ?
                "" : edt_body.getText().toString();

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("author", selected_author == null ? "" : selected_author);
                bundle.putString("subject", selected_subject == null ? "" : selected_subject);
                bundle.putString("product_type", selected_product_type == null ? "" : selected_product_type);
                bundle.putString("from_year", selected_from_year);
                bundle.putString("to_year", selected_to_year);
                bundle.putString("edt_subject", edt_subject_data);
                bundle.putString("edt_body", edt_body_data);
                Fragment fragment = new FragmentSearchResults();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.frame_home, fragment).addToBackStack("search_results").commit();

            }
        });


        btn_clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_body.setText("");
                edt_subject.setText("");
            }
        });





        return view;
    }

    private void SpinnersEvents() {


        spin_all_authors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_author = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

      /*  author_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(),parent.getSelectedItem().toString(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });*/





        spin_all_subjectTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_subject = parent.getSelectedItem().toString();
                //  Toast.makeText(getActivity(), selected_subject, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_all_products.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_product_type = parent.getSelectedItem().toString();
                // Toast.makeText(getActivity(), selected_product_type, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_fromYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_from_year = parent.getSelectedItem().toString();
                // Toast.makeText(getActivity(), selected_from_year, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_toYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_to_year = parent.getSelectedItem().toString();
                //Toast.makeText(getActivity(), selected_to_year, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void SpinnersAdapters() {

        ArrayAdapter<String> adapter_authors = new ArrayAdapter<>
                (getActivity(), R.layout.spinner_text, AuthorList);
        adapter_authors.setDropDownViewResource(R.layout.spinner_text);
        adapter_authors.setDropDownViewResource(R.layout.spinner_text);

       /* SpinnerAdapter adapter_authors=new SpinnerAdapter(AuthorList,getActivity());

        spin_all_authors.setAdapter(adapter_authors);
*/

       /* CustomSpinner adapter_authors=(CustomSpinner)

        spin_all_authors.setAdapter(adapter_authors);*/

     /*  String[]AuthorArrays=new String[AuthorList.size()];

        author_spinner.initializeStringValues(AuthorList.toArray(AuthorArrays),"");*/



        ArrayAdapter<String> adapter_products = new ArrayAdapter<>
                (getActivity(), R.layout.spinner_text, ProductTypesList);
        adapter_products.setDropDownViewResource(R.layout.spinner_text);
        spin_all_products.setAdapter(adapter_products);


        ArrayAdapter<String> adapter_subjects = new ArrayAdapter<>
                (getActivity(), R.layout.spinner_text, SubjectsList);
        adapter_subjects.setDropDownViewResource(R.layout.spinner_text);
        spin_all_subjectTypes.setAdapter(adapter_subjects);


        ArrayAdapter<Integer> adapter_fromYear = new ArrayAdapter<>
                (getActivity(), R.layout.spinner_text, DateFromList);
        adapter_fromYear.setDropDownViewResource(R.layout.spinner_text);
        spin_fromYear.setAdapter(adapter_fromYear);

        ArrayAdapter<Integer> adapter_toYear = new ArrayAdapter<>
                (getActivity(), R.layout.spinner_text, DateToList);
        adapter_toYear.setDropDownViewResource(R.layout.spinner_text);
        spin_toYear.setAdapter(adapter_toYear);


    }

    private void SpinnersAndListsDeclaration(View view) {

        spin_all_authors = (Spinner) view.findViewById(R.id.spin_all_authors);
        spin_all_products = (Spinner) view.findViewById(R.id.spin_all_products);
        spin_all_subjectTypes = (Spinner) view.findViewById(R.id.spin_all_subjects);
        spin_fromYear = (Spinner) view.findViewById(R.id.spin_fromYear);
        spin_toYear = (Spinner) view.findViewById(R.id.spin_toYear);

        AuthorList = new ArrayList<>();
        SubjectsList = new ArrayList<>();
        ProductTypesList = new ArrayList<>();
        DateToList = new ArrayList<>();
        DateFromList = new ArrayList<>();
    }

    private void InitializeSpinners() {

        if (authorsManager == null) {
            authorsManager = new AuthorsManager();
        }
        authorsManager.get_all_authors().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getAuthorList() == null) {
                    } else {
                        for (int i = 0; i < response.body().getAuthorList().size(); i++) {
                            AuthorList.add(response.body().getAuthorList().get(i).getName_ar().toString());
                        }


                        Log.d(TAG, "onResponse: " + AuthorList.get(0));
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), "مشكلة في اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });

        if (productTypesManager == null) {
            productTypesManager = new ProductTypesManager();
        }
        productTypesManager.get_all_ProductsTypes().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getProductTypessList() == null) {
                    } else {
                        for (int i = 0; i < response.body().getProductTypessList().size(); i++) {
                            ProductTypesList.add(response.body().getProductTypessList().get(i).getName().toString());
                        }
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), "مشكلة في اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });

        if (subjectsManager == null) {
            subjectsManager = new SubjectsManager();
        }
        subjectsManager.get_all_subjects().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getSubjectsList() == null) {
                    } else {
                        for (int i = 0; i < response.body().getSubjectsList().size(); i++) {
                            SubjectsList.add(response.body().getSubjectsList().get(i).getText_Ar());
                        }
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), "مشكلة في اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });


        int Year = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < 30; i++) {
            DateFromList.add(Year - i);
        }
        for (int i = 0; i < 30; i++) {
            DateToList.add(Year - i);
        }

        Log.e(TAG, "IntializeSpinners: " + Year);

    }

    private void visibilties() {

        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText("بحث تفصيلي");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
//        getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);
        ActivityHomePage.BuutonsAction(R.drawable.advanced_search, R.drawable.my_library2, R.drawable.home2, R.drawable.my_profile2);
    }

    private void draw_fonts() {
//        txt_search.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Bold"));
        btn_search.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_clear_all.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
    }


}
