package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.settings.Preferences;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class FragmentSettings extends Fragment {


    static boolean isDaily, isWhats, isKhoulasat;
    static String lang_name;
    String[] lang = {"العربية", "الانجليزية"};
    Switch isDailyQoutes, isWhatNew, isKhoulasatNew;
    Spinner spin_language;
    ImageView img_favourites;
    FragmentManager fragmentManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);


        isDailyQoutes = (Switch) v.findViewById(R.id.switch_daily_quotes);
        isWhatNew = (Switch) v.findViewById(R.id.switch_what_new);
        isKhoulasatNew = (Switch) v.findViewById(R.id.switch_khoulasat_names);

        spin_language = (Spinner) v.findViewById(R.id.spin_quotes_language);
        img_favourites = (ImageView) v.findViewById(R.id.img_favourites);


        ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, lang);
        adapter.setDropDownViewResource(R.layout.spinner_text);
        spin_language.setAdapter(adapter);


        img_favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PassDataToFavourites();
            }
        });


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاعدادات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        try {
            isDaily = Boolean.valueOf(Preferences.getFromPreference(getActivity(), "isQuotes"));
            isWhats = Boolean.valueOf(Preferences.getFromPreference(getActivity(), "isNew"));
            isKhoulasat = Boolean.valueOf(Preferences.getFromPreference(getActivity(), "isKhoulasat"));
            lang_name = Preferences.getFromPreference(getActivity(), "QuoteLang");

            isDailyQoutes.setChecked(isDaily);
            isKhoulasatNew.setChecked(isKhoulasat);
            isWhatNew.setChecked(isWhats);


        } catch (Exception e) {

        }
        return v;

    }

    private void PassDataToFavourites() {

        Fragment fragment_favourites = new FragmentFavourites();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IsQuotes", isDailyQoutes.isChecked());
        bundle.putBoolean("IsWhatNew", isWhatNew.isChecked());
        bundle.putBoolean("IsKhoulasat", isKhoulasatNew.isChecked());
        bundle.putString("QuoteLang", spin_language.getSelectedItem().toString());
        fragment_favourites.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame_home, fragment_favourites).
                addToBackStack("favourites").commit();


    }
}
