package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.NotificationAdapter;
import com.orchtech.edaradotcom.managers.NotificationManager;
import com.orchtech.edaradotcom.models.NotificationModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class FragmentNotification extends Fragment {

    RecyclerView rec_notification;
    NotificationManager notificationManager;
    NotificationAdapter notificationAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReenterTransition(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText("إشعارات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);

        rec_notification = (RecyclerView) view.findViewById(R.id.rec_notifications);

        rec_notification.setLayoutManager(new LinearLayoutManager(getActivity()));

        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
        if (notificationManager == null) {
            notificationManager = new NotificationManager();
        }

        notificationManager.Notification().enqueue(new Callback<List<NotificationModel>>() {
            @Override
            public void onResponse(Call<List<NotificationModel>> call, Response<List<NotificationModel>> response) {

                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body().isEmpty()) {
                        Snackbar.make(getView(), "لا توجد اشعارات", Snackbar.LENGTH_SHORT).show();
                    } else {
                        notificationAdapter = new NotificationAdapter(response.body(), getActivity());
                        rec_notification.setAdapter(notificationAdapter);

                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    //   Snackbar.make(getView(),"مشكله ف الموقع",Snackbar.LENGTH_SHORT).show();
                }


                getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<NotificationModel>> call, Throwable t) {

                Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
                getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
            }
        });


        return view;


    }
}
