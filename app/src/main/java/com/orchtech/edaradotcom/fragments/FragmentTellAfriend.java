package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Customer.TellAfriendManager;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class FragmentTellAfriend extends DialogFragment {


    static String MyName;
    TellAfriendManager tellAfriendManager;
    EditText edt_FName, edt_FEmail, edt_YName, edt_FMessage;
    Button btn_send_to_friend;

   /* @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Simple Dialog");
        builder.setMessage("Some message here");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        return builder.create();
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        MyName = Preferences.getFromPreference(getActivity(), "username");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tell_friend, container, false);

        edt_FName = (EditText) view.findViewById(R.id.edt_tell_friend_name);
        edt_FEmail = (EditText) view.findViewById(R.id.edt_tell_friend_email);
        edt_YName = (EditText) view.findViewById(R.id.edt_tell_friend_your_name);
        edt_FMessage = (EditText) view.findViewById(R.id.edt_tell_friend_message);

        btn_send_to_friend = (Button) view.findViewById(R.id.btn_tell_friend_send);

        edt_YName.setText(MyName);

//        getDialog().setTitle("kkkkk");

        btn_send_to_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendToFriend();
            }
        });

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("أخبر صديق");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        return view;
    }

    private void SendToFriend() {

        if (edt_FEmail.getText().toString().equals("")) {
            edt_FEmail.setError("يجب ادخال ايميل الصديق");
        } else if (!FragmentForgotPass.check_email(edt_FEmail.getText().toString())) {
            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحه", "رسالة لصديق", getActivity()).show();
        } else {


            if (tellAfriendManager == null) {
                tellAfriendManager = new TellAfriendManager();
            }

            String FName = edt_FName.getText().toString().equals("") ? "" : edt_FName.getText().toString();
            String FEmail = edt_FEmail.getText().toString().equals("") ? "" : edt_FEmail.getText().toString();
            String YName = edt_YName.getText().toString().equals("") ? "" : edt_YName.getText().toString();
            String FMessage = edt_FMessage.getText().toString().equals("") ? "" : edt_FMessage.getText().toString();

            tellAfriendManager.get_tell_friend(FEmail
                    , YName, FName, FMessage).enqueue(new Callback<ValidationModel.Success>() {
                @Override
                public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                    if (response.code() >= 200 && response.code() < 300) {
                        if (response.body() == null) {

                        } else {
                            if (response.body().isSuccess()) {
                                FragmentForgotPass.showMsg("تم الارسال", "رسالة لصديق", getActivity()).show();
                            } else {
                                Log.e("Error", "onResponse: " + response.body().getMessage());
                                FragmentForgotPass.showMsg("توجد مشكلة", "رسالة لصديق", getActivity()).show();
                            }
                        }
                    } else {
                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    }
                }

                @Override
                public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                    Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                }
            });


        }
    }
}
