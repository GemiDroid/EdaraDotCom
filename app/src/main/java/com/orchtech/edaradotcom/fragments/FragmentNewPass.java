package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.Fonts;

/**
 * Created by ahmed.yousef on 1/1/2017.
 */

public class FragmentNewPass extends Fragment {


    TextView txt_new_pass;
    Button btn_save,btn_back;
    FragmentManager fragmentManager ;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_new_pass, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();

        txt_new_pass=(TextView)view.findViewById(R.id.txt_new_pass);
        btn_back=(Button)view.findViewById(R.id.btn_back);
        btn_save=(Button)view.findViewById(R.id.btn_save);

        txt_new_pass.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Bold"));
        btn_save.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_back.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentForgotPass()).commit();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Saved correctly",Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }



}


