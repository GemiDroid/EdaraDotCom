package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.TrainingListAdapter;
import com.orchtech.edaradotcom.managers.Account.CountriesRegisterManager;
import com.orchtech.edaradotcom.managers.Training.TrainingListManager;
import com.orchtech.edaradotcom.models.CountriesRegisterModel;
import com.orchtech.edaradotcom.models.TrainingListResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class FragmentTraining extends Fragment {

    static List<String> ProgramList, YearList, MonthList, CityList;
    RecyclerView rec_training;
    TrainingListAdapter trainingListAdapter;
    TrainingListManager trainingListManager;
    Spinner spin_training_program, spin_training_year, spin_training_month, spin_training_city;
    CountriesRegisterManager countriesRegisterManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_training, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("التدريب");
        FragmentSideMenu.showDefaultFooter(getActivity());

        rec_training = (RecyclerView) v.findViewById(R.id.rec_training);
        rec_training.setLayoutManager(new LinearLayoutManager(getActivity()));


        spin_training_program = (Spinner) v.findViewById(R.id.spin_training_program);
        spin_training_year = (Spinner) v.findViewById(R.id.spin_training_year);
        spin_training_month = (Spinner) v.findViewById(R.id.spin_training_month);
        spin_training_city = (Spinner) v.findViewById(R.id.spin_training_city);

        getActivity().findViewById(R.id.img_kholasat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                FragmentKhoulasatManager.InfoDialog(29, getActivity(), v);
            }
        });


        ProgramList = new ArrayList<>();
        GetProgramData(v);
        YearList = new ArrayList<>();
        GetYearsData();
        MonthList = new ArrayList<>();
        GetMonthsData();
        CityList = new ArrayList<>();
        GetCityData();

        ArrayAdapter<String> ProgramAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, ProgramList);
        ProgramAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_training_program.setAdapter(ProgramAdapter);

        ArrayAdapter<String> YearAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, YearList);
        YearAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_training_year.setAdapter(YearAdapter);

        ArrayAdapter<String> MonthAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, MonthList);
        MonthAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_training_month.setAdapter(MonthAdapter);

        ArrayAdapter<String> CityAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, CityList);
        CityAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_training_city.setAdapter(CityAdapter);


        return v;
    }

    private void GetProgramData(final View v) {
        if (trainingListManager == null) {
            trainingListManager = new TrainingListManager();
        }
        trainingListManager.get_training_list("year", "month", "cityId", "planId")
                .enqueue(new Callback<TrainingListResponse>() {
                    @Override
                    public void onResponse(Call<TrainingListResponse> call, Response<TrainingListResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body().getTrainingListManager() == null || response.body() == null) {
                                Snackbar.make(getView(), "لا يوجد تدريب في هذه الفترة", Snackbar.LENGTH_SHORT).show();
                            } else {
                                trainingListAdapter = new TrainingListAdapter(response.body().getTrainingListManager(), getActivity());
                                rec_training.setAdapter(trainingListAdapter);

                                for (int i = 0; i < response.body().getTrainingListManager().size(); i++) {
                                    ProgramList.add(response.body().getTrainingListManager().get(i).getName());

                                }

                            }
                        } else {

                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        }

                    }

                    @Override
                    public void onFailure(Call<TrainingListResponse> call, Throwable t) {

                        Snackbar.make(v.getRootView(), getActivity().getResources().getString(R.string.internet_failure),
                                Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    private void GetCityData() {
        if (countriesRegisterManager == null) {
            countriesRegisterManager = new CountriesRegisterManager();
        }


        countriesRegisterManager.get_all_countries().enqueue(new Callback<List<CountriesRegisterModel>>() {
            @Override
            public void onResponse(Call<List<CountriesRegisterModel>> call, Response<List<CountriesRegisterModel>> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {
                        for (int i = 0; i < response.body().size(); i++) {
                            CityList.add(String.valueOf(response.body().get(i).getName()));
                        }
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<List<CountriesRegisterModel>> call, Throwable t) {

                Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    private void GetMonthsData() {

        for (int i = 0; i < 12; i++) {

            MonthList.add(String.valueOf(i + 1));
        }

    }

    private void GetYearsData() {

        int CurrentYear = Calendar.getInstance().get(Calendar.YEAR);

        for (int i = 0; i < 30; i++) {

            YearList.add(String.valueOf(CurrentYear - i));

        }

    }


}
