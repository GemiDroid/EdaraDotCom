package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class FragmentHelpSupport extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReenterTransition(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_support, container, false);


        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText("المساعدة و الدعم");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);


        return view;

    }
}
