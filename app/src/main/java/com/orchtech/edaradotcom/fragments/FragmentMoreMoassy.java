package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class FragmentMoreMoassy extends Fragment {


    WebView web;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_more_moassy, container, false);

        web = (WebView) v.findViewById(R.id.web_more);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاشتراك الرئيسي");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.getSettings().setUseWideViewPort(true);
        web.loadUrl("https://edara.com/marketing/CorpProfile_Ar/");

        return v;

    }
}
