package com.orchtech.edaradotcom.fragments;

import android.support.v4.app.Fragment;

/*import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;*/

/**
 * Created by ahmed.yousef on 4/15/2017.
 */

@Deprecated
public class FragmentMapView extends Fragment {

   /* private MapView mapView = null;*//*

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) v.findViewById(R.id.mapboxMapView);
        mapView.setAccessToken(getString(R.string.accessToken));
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.setStyle(Style.MAPBOX_STREETS);
                mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(45.520486, -122.673541), 20));

                ArrayList<LatLng> polygon = new ArrayList<>();
                polygon.add(new LatLng(45.522585, -122.685699));
                polygon.add(new LatLng(45.534611, -122.708873));
                polygon.add(new LatLng(45.530883, -122.678833));
                polygon.add(new LatLng(45.547115, -122.667503));
                polygon.add(new LatLng(45.530643, -122.660121));
                polygon.add(new LatLng(45.533529, -122.636260));
                polygon.add(new LatLng(45.521743, -122.659091));
                polygon.add(new LatLng(45.510677, -122.648792));
                polygon.add(new LatLng(45.515008, -122.664070));
                polygon.add(new LatLng(45.502496, -122.669048));
                polygon.add(new LatLng(45.515369, -122.678489));
                polygon.add(new LatLng(45.506346, -122.702007));
                polygon.add(new LatLng(45.522585, -122.685699));

                mapboxMap.addPolygon(new PolygonOptions()
                        .addAll(polygon)
                        .fillColor(Color.parseColor("#3bb2d0")));
            }
        });


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }*/
}
