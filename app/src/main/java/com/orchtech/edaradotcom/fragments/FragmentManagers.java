package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.SubManagersAdapter;
import com.orchtech.edaradotcom.managers.SubscriptionTypes.ManagersManager;
import com.orchtech.edaradotcom.models.Products.SearchResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/16/2017.
 */

public class FragmentManagers extends Fragment implements SubManagersAdapter.GetId {

    static String library_id;
    RecyclerView rec_sub_managers;
    ManagersManager managersManager;
    SubManagersAdapter subManagersAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_sub_manager, container, false);


        rec_sub_managers = (RecyclerView) v.findViewById(R.id.rec_sub_managers);
        rec_sub_managers.setItemAnimator(new DefaultItemAnimator());
        rec_sub_managers.setLayoutManager(new LinearLayoutManager(getActivity()));
        FragmentSideMenu.showDefaultFooter(getActivity());

        if (managersManager == null) {
            managersManager = new ManagersManager();
        }

        managersManager.get_ManagersLibraries().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getSubjectsList() == null) {
                        Toast.makeText(getActivity(), "لا توجد مكتبات في هذا الاشتراك", Toast.LENGTH_SHORT).show();
                    } else {

                        subManagersAdapter = new SubManagersAdapter(response.body().getSubjectsList(), getActivity());
                        rec_sub_managers.setAdapter(subManagersAdapter);
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_failure), Toast.LENGTH_SHORT).show();
                }

            }
        });

        try {
            Bundle bundle = getArguments();
            if (bundle.getString("library_id") == null) {
            } else {
                library_id = bundle.getString("library_id");

                Toast.makeText(getActivity(), library_id + "kkk", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("مكتبات المديرين");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        return v;

    }

    @Override
    public void getID() {

    }
}
