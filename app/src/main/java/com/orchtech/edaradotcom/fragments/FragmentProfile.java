package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityHomePage;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Account.ProfileDataManager;
import com.orchtech.edaradotcom.managers.Account.ProfileUpdateManager;
import com.orchtech.edaradotcom.managers.Account.UploadImageManager;
import com.orchtech.edaradotcom.models.Profile.ProfileModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 1/3/2017.
 */
public class FragmentProfile extends android.support.v4.app.Fragment {

    static String Session;
    static String Email, Password, UserName;
    static String FN, MN, LN;
    static Bitmap bitmap;
    FragmentManager fragmentManager;
    ProfileDataManager profileDataManager = null;
    ProfileUpdateManager profileUpdateManager;
    EditText edt_profile_name, edt_profile_email, edt_profile_password;
    Button btn_your_subscription, btn_save, btn_cancel;
    ImageView img_edit_profile, img_profile;
    RelativeLayout rlt_profile_data;
    LinearLayout lin_profile_edit;
    EditText email_edit, pass_edit, pass_new_edit, pass_confirm_edit;
    ProgressBar loading_bar;

    UploadImageManager uploadImageManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
        Session = Preferences.getFromPreference(getActivity(), "session_id");
        Email = Preferences.getFromPreference(getActivity(), "email");
        Password = Preferences.getFromPreference(getActivity(), "password");
        UserName = Preferences.getFromPreference(getActivity(), "username");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("حسابي");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        /*getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);*/


        loading_bar = (ProgressBar) getActivity().findViewById(R.id.home_progress);
        //  Glide.with(getActivity()).load(R.drawable.search_loading).asGif().into(loading_bar);


        rlt_profile_data = (RelativeLayout) v.findViewById(R.id.layout_profile_data);
        lin_profile_edit = (LinearLayout) v.findViewById(R.id.layout_profile_edit);

        btn_save = (Button) v.findViewById(R.id.profile_save);
        btn_your_subscription = (Button) v.findViewById(R.id.btn_profile_participate);
        btn_cancel = (Button) v.findViewById(R.id.profile_cancel);

        // Fields for Profile Data ..
        edt_profile_email = (EditText) v.findViewById(R.id.email_profile);
        edt_profile_password = (EditText) v.findViewById(R.id.password_profile);
        edt_profile_name = (EditText) v.findViewById(R.id.name_profile);
        // Fields for Profile Edits ..
        email_edit = (EditText) v.findViewById(R.id.email_edit);
        pass_edit = (EditText) v.findViewById(R.id.password_edit);
        pass_new_edit = (EditText) v.findViewById(R.id.pass_new_edit);
        pass_confirm_edit = (EditText) v.findViewById(R.id.pass_confirm_edit);
        img_edit_profile = (ImageView) v.findViewById(R.id.img_upload);
        img_profile = (ImageView) v.findViewById(R.id.img_profile);

        rlt_profile_data.setVisibility(View.VISIBLE);
        lin_profile_edit.setVisibility(View.GONE);
        edt_profile_name.setEnabled(false);
        img_edit_profile.setVisibility(GONE);

        edt_profile_email.setText(Email);
        email_edit.setText(Email);



        /*getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);*/

        // Fill Profile Data with cashed data in Shared Preferences..

        ActivityHomePage.BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library2, R.drawable.home2, R.drawable.my_profile);


        v.findViewById(R.id.img_edit_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayEditProfile();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateMyProfile();
            }
        });

        btn_your_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySubscription();
            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlt_profile_data.setVisibility(View.VISIBLE);
                lin_profile_edit.setVisibility(View.GONE);
                edt_profile_name.setEnabled(false);
                img_edit_profile.setVisibility(GONE);
                getProfileData();
            }
        });


        getProfileData();

        img_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageLocally();
            }
        });

        return v;
    }

    private void getProfileData() {

        if (profileDataManager == null) {
            profileDataManager = new ProfileDataManager();
        }

        profileDataManager.get_profile_data(Session).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {

                        edt_profile_password.setText(response.body().getPassword());
                        edt_profile_name.setText(response.body().getFirstName());
                        // Fill Profile Edits with cashed data in Shared Preferences..
                        try {
                            Glide.with(getActivity()).load(response.body().getImage()).into(img_profile);
                        } catch (Exception e) {
                        }
                        pass_edit.setText(response.body().getPassword());


                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {

                Snackbar.make(getView(), getActivity().getResources().
                        getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

            }
        });
    }


    private void DisplaySubscription() {
        if (getActivity().findViewById(R.id.fragment_your_subscription) == null) {
            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentYourSubscription()).addToBackStack("your_subscription").commit();
        }

    }

    private void UpdateMyProfile() {

        String UserName = edt_profile_name.getText().toString();
        String Email = email_edit.getText().toString();
        String Password = pass_edit.getText().toString();
        String NewPassword = pass_new_edit.getText().toString();
        String ConfirmPassword = pass_confirm_edit.getText().toString();

        if (!NewPassword.equals(ConfirmPassword)) {
            Snackbar.make(getView(), "الباسوردين غير متطابقين", Snackbar.LENGTH_SHORT).show();
        } else {
            if (profileUpdateManager == null) {
                profileUpdateManager = new ProfileUpdateManager();
            }
            loading_bar.setVisibility(View.VISIBLE);
            profileUpdateManager.get_update_profile(Session, UserName, Email, Password, NewPassword)
                    .enqueue(new Callback<ValidationModel>() {
                        @Override
                        public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {

                            if (response.code() >= 200 && response.code() < 300) {
                                if (response.body() == null) {

                                } else {
                                    if (response.body().getSuccess().isSuccess()) {
                                        Snackbar.make(getView(), response.body().
                                                getSuccess().getMessage(), Snackbar.LENGTH_SHORT).show();
//                                        UpdateMyPhoto();
                                    } else {
                                        Snackbar.make(getView(), response.body().
                                                getSuccess().getMessage(), Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                                loading_bar.setVisibility(View.GONE);
                            } else {
                                startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                loading_bar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<ValidationModel> call, Throwable t) {

                            Snackbar.make(getView(), "مشكلة في اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                            loading_bar.setVisibility(View.GONE);
                        }
                    });

        }


    }

    private void DisplayEditProfile() {

        rlt_profile_data.setVisibility(View.GONE);
        lin_profile_edit.setVisibility(View.VISIBLE);
        edt_profile_name.setEnabled(true);
        img_edit_profile.setVisibility(View.VISIBLE);
    }

    private void getImageLocally() {

        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        i.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        startActivityForResult(i, 1);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == 1 && resultCode == RESULT_OK) {

            Uri uri = data.getData();
            try {

                bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(uri), null, null);
                img_profile.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    private String GetImageUpload() {

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);
        byte[] ba = bao.toByteArray();
        Log.e("Image Bytes", "GetImageUpload: " + Base64.encodeToString(ba, Base64.DEFAULT));

        return Base64.encodeToString(ba, Base64.DEFAULT);
    }


    private void UpdateMyPhoto() {

        if (uploadImageManager == null) {
            uploadImageManager = new UploadImageManager();
        }
        uploadImageManager.UploadImage(Session, GetImageUpload(), "")
                .enqueue(new Callback<ValidationModel.Success>() {
                    @Override
                    public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body() == null) {
                            } else {
                                if (response.body().isSuccess()) {

                                } else {
                                    FragmentForgotPass.showMsg(response.body().getMessage(), "حسابي", getActivity()).show();
                                }
                            }
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                        Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure),
                                Snackbar.LENGTH_SHORT).show();
                    }
                });


    }


}
