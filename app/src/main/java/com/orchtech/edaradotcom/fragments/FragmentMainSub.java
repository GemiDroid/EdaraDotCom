package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.MainSubAdapter;
import com.orchtech.edaradotcom.managers.SubscriptionTypes.MainSubManager;
import com.orchtech.edaradotcom.models.Subscription.MainSubModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/17/2017.
 */

public class FragmentMainSub extends Fragment {


    RecyclerView rec_main_sub;
    MainSubAdapter mainSubAdapter;
    MainSubManager mainSubManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main_sub, container, false);

        rec_main_sub = (RecyclerView) v.findViewById(R.id.rec_main_sub);
        rec_main_sub.setLayoutManager(new LinearLayoutManager(getActivity()));
        rec_main_sub.setItemAnimator(new DefaultItemAnimator());

        if (mainSubManager == null) {
            mainSubManager = new MainSubManager();
        }

        mainSubManager.get_MainSubscription().enqueue(new Callback<MainSubModel>() {
            @Override
            public void onResponse(Call<MainSubModel> call, Response<MainSubModel> response) {

                if (response.code() >= 200 && response.code() < 300) {

                    if (response.body().getSubTypesListList() == null) {
                    } else {
                        mainSubAdapter = new MainSubAdapter(response.body().getSubTypesListList(), getActivity());
                        rec_main_sub.setAdapter(mainSubAdapter);
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<MainSubModel> call, Throwable t) {

                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاشتراكات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        return v;

    }
}
