package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Question.QuestionDetailsManager;
import com.orchtech.edaradotcom.managers.Question.QuestionsListManager;
import com.orchtech.edaradotcom.models.questions.QuestionDetailsResponse;
import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 3/12/2017.
 */
public class FragmentConsultantDetails extends Fragment implements View.OnClickListener {

    static int Index = 0;
    static ArrayList<Integer> Question_Ids;
    int question;
    QuestionDetailsManager questionsManager = null;
    QuestionsListManager questionsListManager = null;
    TextView question_id, question_title, question_body, question_answer, question_answered_by, question_date, question_answer_txt;
    String TAG = FragmentConsultantDetails.class.getSimpleName();
    ImageView img_next, img_prev;
    ImageView floatingActionButton;
    FragmentManager fragmentManager;

    int consultant_id;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_consultant_details, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();

        Question_Ids = new ArrayList<>();

        img_next = (ImageView) view.findViewById(R.id.img_question_next);
        img_prev = (ImageView) view.findViewById(R.id.img_question_prev);

        floatingActionButton = (ImageView) view.findViewById(R.id.img_askConsulting);

        question_id = (TextView) view.findViewById(R.id.question_number);
        question_title = (TextView) view.findViewById(R.id.question_title);
        question_body = (TextView) view.findViewById(R.id.question_body);
        question_answer = (TextView) view.findViewById(R.id.question_answer);
        question_answered_by = (TextView) view.findViewById(R.id.question_answered_by);
        question_date = (TextView) view.findViewById(R.id.question_date);
        question_answer_txt = (TextView) view.findViewById(R.id.question_answer_text);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاستشارات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());


        try {
            Bundle i = getArguments();
            question = i.getInt("consultant_id");
        } catch (Exception e) {
        }

        //  url = Preferences.getUrlHeader();

       /* getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);

        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        v.setText("الاستشارات");
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        getActivity().findViewById(R.id.img_back).setVisibility(View.GONE);*/

        /*ImageView img_main = (ImageView) getActivity().findViewById(R.id.img_main);
        img_main.setImageResource(R.drawable.home2);*/


        // Getting Question List ..

        // Getting First Question in List .. Then after (Next/Prev.), it will change..
//        QuestionDetails(Question_Ids.get(0));
       /* Snackbar.make(getView(), "انت تشاهد أول سؤال من اسئله الساده الزوار", Snackbar.LENGTH_SHORT).show();
        Snackbar.make(getView(), "يمكنك استخدام الازار للتنقل بين كل سؤال و الاخر", Snackbar.LENGTH_LONG).show();
*/

        GetQuestionList();

        img_next.setOnClickListener(FragmentConsultantDetails.this);
        img_prev.setOnClickListener(FragmentConsultantDetails.this);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentAskConsultant()).commit();
            }
        });

        QuestionDetails(question);

        return view;
    }

    private void GetQuestionList() {

        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
        if (questionsListManager == null) {
            questionsListManager = new QuestionsListManager();
        }
        questionsListManager.get_question_list(100).enqueue(new Callback<QuestionsListResponse>() {
            @Override
            public void onResponse(Call<QuestionsListResponse> call, Response<QuestionsListResponse> response) {

                if (response.body() != null) {
                    Log.d(TAG, "onResponse: " + response.body().getQuestionList().size());
                    Log.d(TAG, "Get ID: " + response.body().getQuestionList().get(0).getQuestion_id());

                    for (int i = 0; i < response.body().getQuestionList().size(); i++) {
                        Question_Ids.add(response.body().getQuestionList().get(i).getQuestion_id());
                    }
                } else {
                    Snackbar.make(getView(), "لا توجد اسئله", Snackbar.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<QuestionsListResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    private void QuestionDetails(int index) {
        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
        if (questionsManager == null) {
            questionsManager = new QuestionDetailsManager();
        }
        questionsManager.get_question(index).enqueue(new Callback<QuestionDetailsResponse>() {
            @Override
            public void onResponse(Call<QuestionDetailsResponse> call, Response<QuestionDetailsResponse> response) {

               /* Log.d(TAG, "onResponse: "+ response.body().getQuestion().getAnswer());*/

                if (response.code() >= 200 && response.code() <= 300) {
                    if (response.body() == null || response.body().getQuestionDetails() == null) {
                        getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        Snackbar.make(getView(), "لا توجد اسئله", Snackbar.LENGTH_SHORT).show();
                    } else {

                        question_id.setText(response.body().getQuestionDetails().getQuestion_id() + "#");
                        question_title.setText(response.body().getQuestionDetails().getTitle());
                        question_body.setText(response.body().getQuestionDetails().getBody());
                        question_answer.setText(response.body().getQuestionDetails().getAnswer());
                        question_answered_by.setText("بواسطه : " + response.body().getQuestionDetails().getAnswered_by());
                        question_date.setText("بتاريخ : " + response.body().getQuestionDetails().getDate());
                        question_answer_txt.setText("الاجابه");
                        floatingActionButton.setVisibility(View.VISIBLE);
                        getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);

                    }
                } else {
                    // Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                }
                /*if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                } else {
                    getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                }*/
            }

            @Override
            public void onFailure(Call<QuestionDetailsResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                Snackbar.make(getView(), "من فضلك تأكد من اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                } else {
                    getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_question_next:
                // Next Question Details
                if (Index < Question_Ids.size()) {
                    Index++;
                    QuestionDetails(Question_Ids.get(Index));
                    //Toast.makeText(getActivity(), "ID"+Index, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "No Next", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.img_question_prev:
                // Prev. Question Details
                if (Index > 0) {
                    Index--;
                    QuestionDetails(Question_Ids.get(Index));
                    // Toast.makeText(getActivity(), "ID"+Index, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "No Prev.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

}
