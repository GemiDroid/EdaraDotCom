package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.FavoriteListener;
import com.orchtech.edaradotcom.adapters.FavouritesAdapter;
import com.orchtech.edaradotcom.managers.FavouritesManager;
import com.orchtech.edaradotcom.managers.SettingsManager;
import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.models.Products.SubjectsModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

public class FragmentFavourites extends Fragment {


    static boolean isDailyQuotes, isNew, isKhoulasat;
    static String quotes_language, Session;
    static String builder;
    static String builderId;
    ArrayList<SubjectsModel> items;
    RecyclerView rec_favourites;
    Button btn_sava_fav;
    FavouritesManager favouritesManager;
    SettingsManager settingsManager;
    FavouritesAdapter favouritesAdapter;
    FavoriteListener Listener;
    List<String> GetIDs;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_favourites, container, false);

        rec_favourites = (RecyclerView) v.findViewById(R.id.rec_favourites);
        rec_favourites.setItemAnimator(new DefaultItemAnimator());
        rec_favourites.setLayoutManager(new LinearLayoutManager(getActivity()));
        FragmentSideMenu.showDefaultFooter(getActivity());

        btn_sava_fav = (Button) v.findViewById(R.id.btn_save_fav);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاعدادات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        GetFvourites();
        GetIDs = new ArrayList<>();


        Listener = new FavoriteListener() {
            @Override
            public void onFavoriteChecked(String model, boolean isChecked) {
                if (isChecked) {
                    isInterested(model, true);
                } else {
                    isInterested(model, false);
                }
            }
        };

        btn_sava_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Arguments();
                builder = String.valueOf(new StringBuilder().append(TextUtils.join(",", GetIDs)));
                // Toast.makeText(getActivity(), builder, Toast.LENGTH_SHORT).show();
                SaveFavourites();
            }
        });


        return v;

    }

    private void SaveFavourites() {


        if (settingsManager == null) {
            settingsManager = new SettingsManager();
        }
        builderId = builder == "" ? "0" : builder;
        settingsManager.Settings(new Time(12, 00, 00), true, builderId, isDailyQuotes, true, 12, Session, isNew, isKhoulasat)
                .enqueue(new Callback<ValidationModel>() {
                    @Override
                    public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            /*if (response.body().getSuccess()== null) {
                            } else {*/
                            if (response.body().getSuccess().isSuccess()) {
                                Log.d("Interested Params", "onResponse: " + builderId);
                                FragmentForgotPass.showMsg("تم حفظ اعداداتك بنجاح", "الاعدادات", getActivity()).show();
                                Preferences.saveInPreference(getActivity(), "isQuotes", "" + isDailyQuotes);
                                Preferences.saveInPreference(getActivity(), "isKhoulasat", "" + isKhoulasat);
                                Preferences.saveInPreference(getActivity(), "isNew", "" + isNew);
                                Preferences.saveInPreference(getActivity(), "QuoteLang", "" + quotes_language);
                            } else {
                                Log.d("Interested Params", "onResponse: " + builderId);
                                FragmentForgotPass.showMsg("مشكلة في الاعدادات", "الاعدادات", getActivity()).show();
                            }
                            //}
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidationModel> call, Throwable t) {

                        Snackbar.make(getView().getRootView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

                    }
                });


    }

    private void Arguments() {
        try {
            Bundle bundle = getArguments();
            isDailyQuotes = bundle.getBoolean("IsQuotes");
            isKhoulasat = bundle.getBoolean("IsKhoulasat");
            isNew = bundle.getBoolean("IsWhatNew");
            quotes_language = bundle.getString("QuoteLang");

            //   Toast.makeText(getActivity(), "Is Quotes : " + isDailyQuotes, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
        }
    }

    private void GetFvourites() {

        if (favouritesManager == null) {
            favouritesManager = new FavouritesManager();
        }

        favouritesManager.Faourites(Session).enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getSubjectsList() == null) {
                    } else {
                        items = response.body().getSubjectsList();
                        favouritesAdapter = new FavouritesAdapter(items, getActivity(), Listener);
                        rec_favourites.setAdapter(favouritesAdapter);
                        favouritesAdapter.notifyDataSetChanged();
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    public void isInterested(String id, boolean isInterest) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getID().equals(id)) {
                if (isInterest) {
                    items.get(i).setIs_Interest(true);
                    GetIDs.add(items.get(i).getID());
                } else {
                    items.get(i).setIs_Interest(false);
                    GetIDs.remove(items.get(i).getID());

                }
            }
        }
    }


}
