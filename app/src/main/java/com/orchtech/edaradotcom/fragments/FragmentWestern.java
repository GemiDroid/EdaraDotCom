package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.WesternAdapter;
import com.orchtech.edaradotcom.managers.Payment.WesternUnionManger;
import com.orchtech.edaradotcom.models.Payment.BankTransferModel;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class FragmentWestern extends Fragment {


    LinearLayout lin_western;
    WesternUnionManger westernUnionManger;
    TextView western_header, western_point1, western_point2, western_point3, western_point4, western_footer;
    ImageView western_logo;
    RecyclerView rec_western_bullets;
    WesternAdapter westernAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_western, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("ويسترن يونيون");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        lin_western = (LinearLayout) v.findViewById(R.id.fragment_western);
        lin_western.setVisibility(GONE);


        western_header = (TextView) v.findViewById(R.id.txt_western_header);
        western_footer = (TextView) v.findViewById(R.id.txt_western_footer);
        western_point1 = (TextView) v.findViewById(R.id.txt_western_point1);
        western_point2 = (TextView) v.findViewById(R.id.txt_western_point2);
        western_point3 = (TextView) v.findViewById(R.id.txt_western_point3);
        western_point4 = (TextView) v.findViewById(R.id.txt_western_point4);

        western_logo = (ImageView) v.findViewById(R.id.img_western_logo);

        rec_western_bullets = (RecyclerView) v.findViewById(R.id.rec_western_bullets);

        rec_western_bullets.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (westernUnionManger == null) {
            westernUnionManger = new WesternUnionManger();
        }

        westernUnionManger.get_WesternUnion().enqueue(new Callback<BankTransferModel>() {
            @Override
            public void onResponse(Call<BankTransferModel> call, Response<BankTransferModel> response) {


                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                        lin_western.setVisibility(GONE);
                    } else {
                        western_header.setText(response.body().getHeader());
                        western_footer.setText(response.body().getFooter());
                        western_point1.setText(response.body().getFirstPoint());
                        western_point2.setText(response.body().getSecondPoint());
                        western_point3.setText(response.body().getThirdPoint());
                        western_point4.setText(response.body().getFourthPoint());
                        Picasso.with(getActivity()).load(response.body().getLogo()).into(western_logo);


                        westernAdapter = new WesternAdapter(response.body().getPointBullets(), getActivity());
                        rec_western_bullets.setAdapter(westernAdapter);

                        lin_western.setVisibility(View.VISIBLE);
                    }
                } else {
                    lin_western.setVisibility(GONE);
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<BankTransferModel> call, Throwable t) {
                lin_western.setVisibility(GONE);
                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });


        return v;
    }
}
