package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Articles.ArticleDetailsManager;
import com.orchtech.edaradotcom.managers.Articles.ArticlesManager;
import com.orchtech.edaradotcom.models.Articles.ArticlesModel;
import com.orchtech.edaradotcom.models.Articles.ArticlesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class FragmentArticleDetails extends Fragment {

    static int Index = 0;
    int article_id;
    ImageView img_article_next, img_article_prev;
    TextView article_title, article_written_by, article_body;
    ArticlesManager articlesManager;
    List<ArticlesModel> ArticleResponse;
    List<Integer> Articles_Ids;

    ArticleDetailsManager articleDetailsManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_article_details, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("المقالات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        article_body = (TextView) view.findViewById(R.id.article_body);
        article_title = (TextView) view.findViewById(R.id.article_title);
        article_written_by = (TextView) view.findViewById(R.id.article_written_by);

        img_article_next = (ImageView) view.findViewById(R.id.img_article_next);
        img_article_prev = (ImageView) view.findViewById(R.id.img_article_prev);

        Articles_Ids = new ArrayList<>();
        ArticleResponse = new ArrayList<>();
        get_articles(50);

        img_article_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Index < Articles_Ids.size()) {
                    Index++;
                    GetArticleDetails(Articles_Ids.get(Index));
                } else {

                }
            }
        });

        img_article_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Index > 0) {
                    Index--;
                    GetArticleDetails(Articles_Ids.get(Index));
                } else {

                }
            }
        });


        try {
            Bundle i = getArguments();
            article_id = i.getInt("article_id");
        } catch (Exception e) {
        }


        GetArticleDetails(article_id);


        return view;
    }

    private void GetArticleDetails(int Id) {
        if (articleDetailsManager == null) {
            articleDetailsManager = new ArticleDetailsManager();
        }

        articleDetailsManager.get_article_details(Id).enqueue(new Callback<ArticlesModel.ArticleDetails>() {
            @Override
            public void onResponse(Call<ArticlesModel.ArticleDetails> call, Response<ArticlesModel.ArticleDetails> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {

                        article_title.setText(response.body().getDetailsArticle().getTitle());
                        article_written_by.setText(response.body().getDetailsArticle().getAuthor());
                        article_body.setText(response.body().getDetailsArticle().getBody());

                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<ArticlesModel.ArticleDetails> call, Throwable t) {

                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

            }
        });
    }

    private void get_articles(int page_count) {

        if (articlesManager == null) {
            articlesManager = new ArticlesManager();
        }

        articlesManager.get_articles("1", page_count, "year", "author")
                .enqueue(new Callback<ArticlesResponse>() {
                    @Override
                    public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
//                        getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
                        // ArticleResponse.clear();


                        ArticleResponse.addAll(response.body().getArticles());


                        for (int i = 0; i < ArticleResponse.size(); i++) {

                            Articles_Ids.add(ArticleResponse.get(i).getArticle_id());

                        }


                        /*if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                        } else {
                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        }*/
                    }

                    @Override
                    public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                        Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
                        if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                        } else {
                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        }
                    }
                });
    }
}
