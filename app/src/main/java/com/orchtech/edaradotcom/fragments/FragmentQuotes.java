package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.DailyQuotesManager;
import com.orchtech.edaradotcom.models.DailyQuotesModel;
import com.orchtech.edaradotcom.settings.Preferences;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 3/17/2017.
 */
public class FragmentQuotes extends Fragment {

    static String QuoteLang;
    static boolean isArabic;
    DailyQuotesManager dailyQuotesManager;
    TextView txt_title, txt_author;
    ImageView img_author;
    ProgressBar prog_quotes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_quotes, container, false);


        txt_title = (TextView) v.findViewById(R.id.txt_quote_body);
        txt_author = (TextView) v.findViewById(R.id.txt_quote_writer);
        img_author = (ImageView) v.findViewById(R.id.img_quote_writer);

        prog_quotes = (ProgressBar) v.findViewById(R.id.prog_quotes);

        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView view = (TextView) getActivity().findViewById(R.id.txt_logo);
        view.setText("أقوال يومية");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        /*
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);

        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);

        getActivity().findViewById(R.id.img_back).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);*/

        //getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);

        if (Preferences.getFromPreference(getActivity(), "isQuotes").equals("false")) {
            FragmentForgotPass.showMsg("من فضلك قم بتمكين الاقوال من قائمة الاعدادات", "أقوال مأثورة", getActivity()).show();
            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
        } else {

            if (dailyQuotesManager == null) {
                dailyQuotesManager = new DailyQuotesManager();
            }

            QuoteLang = Preferences.getFromPreference(getActivity(), "QuoteLang");
            if (QuoteLang.equals("العربية")) {
                isArabic = true;
            } else {
                isArabic = false;
            }

            //  getActivity().findViewById(R.id.home_progress).setVisibility(View.VISIBLE);
            dailyQuotesManager.DailyQuotes(isArabic).
                    enqueue(new Callback<DailyQuotesModel>() {
                        @Override
                        public void onResponse(Call<DailyQuotesModel> call, Response<DailyQuotesModel> response) {

                            if (response.code() >= 200 && response.code() < 300) {
                                if (response.body() == null) {
                                    Snackbar.make(getView(), "لا توجد مقولات", Snackbar.LENGTH_SHORT).show();
                                    prog_quotes.setVisibility(View.GONE);
                                } else {
                                    Picasso.with(getActivity()).load(response.body().getAuthorImage()).
                                            into(img_author);
                                    txt_title.setText(response.body().getTitle());
                                    txt_author.setText(response.body().getAuthor());
                                    prog_quotes.setVisibility(View.GONE);
                                }
                            } else {
                                // Snackbar.make(getView(),"مشكله ف السيرفر",Snackbar.LENGTH_SHORT).show();
                                startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                prog_quotes.setVisibility(View.GONE);
                            }
//                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<DailyQuotesModel> call, Throwable t) {
                            Snackbar.make(getView(), "مشكله ف الانترنت", Snackbar.LENGTH_SHORT).show();
                            prog_quotes.setVisibility(View.GONE);
                        }
                    });
        }


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("أقوال مأثورة");

        return v;

    }
}
