package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.activities.EndlessRecyclerOnScrollListener;
import com.orchtech.edaradotcom.adapters.ProductsAdapter;
import com.orchtech.edaradotcom.managers.Products.SearchManager;
import com.orchtech.edaradotcom.managers.Products.SubjectsManager;
import com.orchtech.edaradotcom.models.Products.ProductsModel;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/3/2017.
 */

public class FragmentSoundLibrary extends Fragment {


    static String Session, subject;
    static int page_count = 10;
    List<ProductsModel> SearchResponse;
    List<String> SubjectsList;
    FragmentManager fragmentManager;
    RecyclerView rec_sound_books;
    Spinner spin_sound_subjects;
    EditText edt_sound_search;
    Button btn_sound_search;
    SubjectsManager subjectsManager;
    SearchManager searchManager;
    ProductsAdapter productsAdapter;
    ProgressBar prog_sound_books;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_sound_library, container, false);

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الكتب المسموعة");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.VISIBLE);
        FragmentSideMenu.showDefaultFooter(getActivity());


        rec_sound_books = (RecyclerView) v.findViewById(R.id.rec_sound_books);
        spin_sound_subjects = (Spinner) v.findViewById(R.id.spin_subjects_sound);
        edt_sound_search = (EditText) v.findViewById(R.id.edt_search_sound);
        btn_sound_search = (Button) v.findViewById(R.id.btn_search_sound);

        prog_sound_books = (ProgressBar) v.findViewById(R.id.prog_sound_books);

        LinearLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        rec_sound_books.setLayoutManager(manager);

        FillSubjectSpinner();

        DefineSpinner();

        SearchResponse = new ArrayList<>();
        productsAdapter = new ProductsAdapter(SearchResponse, getActivity());
        rec_sound_books.setAdapter(productsAdapter);

        ItemSelection();

        rec_sound_books.addOnScrollListener(new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int current_page) {
                page_count += 9;
                get_sound_books(page_count);
            }
        });

        getActivity().findViewById(R.id.img_kholasat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                FragmentKhoulasatManager.InfoDialog(28
                        , getActivity(), v);
            }
        });


        btn_sound_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_sound_books(page_count);
            }
        });
        get_sound_books(page_count);


        return v;

    }

    private void ItemSelection() {
        spin_sound_subjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                subject = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void get_sound_books(int page) {

        if (searchManager == null) {
            searchManager = new SearchManager();
        }
        String subjects = subject == "" ? null : subject;
        String search_title = edt_sound_search.getText().toString().equals("") ? null : edt_sound_search.getText().toString();


        searchManager.Search(20, page, Session, null, search_title, null, null, subjects, "28", null)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body().getItems() == null || response.body() == null) {
                                Toast.makeText(getActivity(), "لا توجد كتب صوتية", Toast.LENGTH_SHORT).show();
                                prog_sound_books.setVisibility(GONE);
                            } else {

                                SearchResponse.addAll(response.body().getItems());
                                productsAdapter.notifyItemRangeInserted(productsAdapter.getItemCount(), SearchResponse.size() - 1);
                                productsAdapter.notifyDataSetChanged();
                                prog_sound_books.setVisibility(GONE);
                            }
                        } else {
                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                            prog_sound_books.setVisibility(GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        prog_sound_books.setVisibility(GONE);
                        Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });

    }

    private void DefineSpinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, SubjectsList);
        adapter.setDropDownViewResource(R.layout.spinner_text);
        spin_sound_subjects.setAdapter(adapter);
    }

    private void FillSubjectSpinner() {

        if (subjectsManager == null) {
            subjectsManager = new SubjectsManager();
        }
        SubjectsList = new ArrayList<>();

        subjectsManager.get_all_subjects().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getSubjectsList() == null) {
                    } else {

                        for (int i = 0; i < response.body().getSubjectsList().size(); i++) {
                            SubjectsList.add(response.body().getSubjectsList().get(i).getText_Ar());
                        }
                    }

                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Snackbar.make(getView(), "مشكلة في اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
            }
        });

    }
}
