package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Question.QuestionDataManager;
import com.orchtech.edaradotcom.models.questions.QuestionDataResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ahmed.yousef on 3/16/2017.
 */

public class FragmentAskConsultant extends Fragment {

    static String SessionID;
    static String url;
    static String TAG = FragmentAskConsultant.class.getSimpleName();
    EditText edt_title, edt_body;
    Switch switch_showName;
    QuestionDataManager questionDataManager;
    Button btn_send_consultant;
    FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // url = Preferences.getUrlHeader();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ask_consultant, container, false);


        edt_title = (EditText) view.findViewById(R.id.edt_title);
        edt_body = (EditText) view.findViewById(R.id.edt_body);
        switch_showName = (Switch) view.findViewById(R.id.swich_showName);

        fragmentManager = getActivity().getSupportFragmentManager();

        btn_send_consultant = (Button) view.findViewById(R.id.btn_send_consultant);
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        /*getActivity().findViewById(R.id.img_back).setVisibility(View.VISIBLE);

        getActivity().findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentConsultantDetails()).commit();
            }
        });*/


        btn_send_consultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SessionID = Preferences.getFromPreference(getActivity(), "session_id");
                // SessionID="6ef07843-f60f-423a-98d9-da1e2f25826f";
                if (SessionID.equals("")) {
                    Snackbar.make(getView(), "انت غير مسجل , يجب التسجيل اولا", Snackbar.LENGTH_LONG).setActionTextColor(getResources().getColor(R.color.colorOrange)).setAction("تسجيل", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentLogin()).commit();
                        }
                    }).show();


                } else {

                    if (edt_title.getText().toString().equals("") | edt_body.getText().toString().equals("")) {
                        Snackbar.make(getView(), "يجب ادخال قيم في خانات العنوان أو السؤال المطروح", Snackbar.LENGTH_LONG).show();
                    } else {

                        if (questionDataManager == null) {
                            questionDataManager = new QuestionDataManager();
                        }
                        questionDataManager.set_question(SessionID, edt_title.getText().toString(),
                                edt_body.getText().toString(), switch_showName.isChecked())
                                .enqueue(new Callback<QuestionDataResponse>() {
                                    @Override
                                    public void onResponse(Call<QuestionDataResponse> call, Response<QuestionDataResponse> response) {

                                        if (response.code() >= 200 && response.code() <= 300) {
                                            if (response.body() == null) {
                                                Snackbar.make(getView(), "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                                            } else {
                                                boolean Success = response.body().getResponse().isSucess();
                                                if (Success) {
                                                    Snackbar.make(getView(), response.body().getResponse().getMessage(), Snackbar.LENGTH_LONG).show();
                                                } else {
                                                    Snackbar.make(getView(), response.body().getResponse().getMessage(), Snackbar.LENGTH_LONG).show();
                                                }
                                            }
                                        } else {
                                            //  Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();
                                            startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                        }
                                        if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                                        } else {
                                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<QuestionDataResponse> call, Throwable t) {

                                        Log.e(TAG, "onFailure: " + t.getMessage());
                                        if (getActivity().findViewById(R.id.home_progress).getVisibility() == View.GONE) {

                                        } else {
                                            getActivity().findViewById(R.id.home_progress).setVisibility(View.GONE);
                                        }

                                    }
                                });
                    }
                }
            }
        });


        return view;
    }
}
