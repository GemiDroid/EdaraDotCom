package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.MoassyAdapter;
import com.orchtech.edaradotcom.managers.Products.ProductDetailsManager;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/3/2017.
 */

public class FragmentProductDetails extends Fragment {


    static String Session, Item_id;
    boolean is_audio, is_paid;

    MoassyAdapter moassyAdapter;

    ProductDetailsManager productDetailsManager;

    RecyclerView rec_product_kholasat;

    ImageView img_product_headphone, img_product_cart;

    TextView txt_product_number, txt_product_author, txt_product_main,
            txt_product_khoulsat, txt_product_isbn,
            txt_product_pages, txt_product_date, txt_product_price, txt_product_listen1, txt_product_listen2;

    List<String> inner_products_items;
    ProgressBar prog_product_details;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_product_details, container, false);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الكتب المسموعة");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());
        prog_product_details = (ProgressBar) v.findViewById(R.id.prog_product_details);

        img_product_headphone = (ImageView) v.findViewById(R.id.img_headphone);
        img_product_cart = (ImageView) v.findViewById(R.id.img_cart);

        txt_product_number = (TextView) v.findViewById(R.id.txt_pro_no);
        txt_product_author = (TextView) v.findViewById(R.id.txt_pro_author);
        txt_product_main = (TextView) v.findViewById(R.id.txt_pro_main_topic);
        txt_product_khoulsat = (TextView) v.findViewById(R.id.txt_pro_khoulasa);
        txt_product_isbn = (TextView) v.findViewById(R.id.txt_pro_isbn);
        txt_product_pages = (TextView) v.findViewById(R.id.txt_pro_pages);
        txt_product_date = (TextView) v.findViewById(R.id.txt_pro_date);
        txt_product_price = (TextView) v.findViewById(R.id.txt_pro_price);
        txt_product_listen1 = (TextView) v.findViewById(R.id.txt_pro_listen1);
        txt_product_listen2 = (TextView) v.findViewById(R.id.txt_pro_listen2);

        rec_product_kholasat = (RecyclerView) v.findViewById(R.id.rec_pro_outlines);
        rec_product_kholasat.setLayoutManager(new LinearLayoutManager(getActivity()));


        try {
            Bundle bundle = getArguments();
            Item_id = bundle.getString("product_id");
            is_audio = bundle.getBoolean("is_audio");
            is_paid = bundle.getBoolean("is_paid");
        } catch (Exception e) {
        }


        if (is_paid) {
            txt_product_price.setVisibility(View.GONE);
            img_product_cart.setVisibility(View.GONE);
        }
        if (!is_audio) {
            txt_product_listen1.setVisibility(GONE);
            txt_product_listen2.setVisibility(GONE);
            // img_product_headphone.setVisibility(View.GONE);
            img_product_headphone.setImageResource(R.drawable.ic_book_download);
            txt_logo.setText("الكتب المقروءة");
        }

        if (productDetailsManager == null) {
            productDetailsManager = new ProductDetailsManager();
        }

        productDetailsManager.get_products_details(Item_id, Session).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {


                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                        prog_product_details.setVisibility(View.GONE);
                        // Toast.makeText(getActivity(),"hhh",Toast.LENGTH_SHORT).show();
                    } else {

                        txt_product_price.setText(response.body().getItemsObject().getPrice() + " $");
                        txt_product_number.setText(response.body().getItemsObject().getNumber());
                        txt_product_author.setText(response.body().getItemsObject().getAuthor());
                        txt_product_main.setText(response.body().getItemsObject().getTitle_ar());
                        txt_product_khoulsat.setText(response.body().getItemsObject().getDescription());
                        txt_product_isbn.setText(response.body().getItemsObject().getIsbn());
                        txt_product_pages.setText(response.body().getItemsObject().getBookPageCount());
                        txt_product_date.setText(response.body().getItemsObject().getPublication_date());

                        inner_products_items = new ArrayList<>();

                        for (int i = 0; i < response.body().getItemsObject().getInnerProductModels().size(); i++) {
                            inner_products_items.add(response.body().getItemsObject().getInnerProductModels().get(i).getText());
                        }
                        moassyAdapter = new MoassyAdapter(inner_products_items, getActivity(), true);
                        rec_product_kholasat.setAdapter(moassyAdapter);

                        prog_product_details.setVisibility(View.GONE);
                    }
                } else {

                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    prog_product_details.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {

                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

                prog_product_details.setVisibility(View.GONE);
            }
        });


        return v;


    }
}
