package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.ActivityLogin;
import com.orchtech.edaradotcom.adapters.MenuAdapter;
import com.orchtech.edaradotcom.models.ModelMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ahmed.yousef on 1/10/2017.
 */

public class FragmentSideMenu extends Fragment {

    static String[] txt_menu_child1, txt_menu_child2, txt_menu_child3, txt_menu_child4, txt_menu_parent;
    static int[] img_menu_child1, img_menu_child2, img_menu_parent;
    private static ImageView img_main, img_library, img_profile, img_search;
    ExpandableListView list_menu_drawer;
    MenuAdapter adapter;
    List<ModelMenu> list_parent, list_child, list_child2, list_child3, list_child4;
    HashMap<ModelMenu, List<ModelMenu>> list_hash_child;
    FragmentManager fragmentManager;

    public static void showDefaultFooter(FragmentActivity context) {

        img_main = (ImageView) context.findViewById(R.id.img_main);
        img_library = (ImageView) context.findViewById(R.id.img_library);
        img_search = (ImageView) context.findViewById(R.id.img_search);
        img_profile = (ImageView) context.findViewById(R.id.img_account);

        img_main.setImageResource(R.drawable.home2);
        img_library.setImageResource(R.drawable.my_library2);
        img_search.setImageResource(R.drawable.advanced_search2);
        img_profile.setImageResource(R.drawable.my_profile2);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_side_menu, container, false);


        txt_menu_parent = getActivity().getResources().getStringArray(R.array.menu_parent_items);

        txt_menu_child1 = getActivity().getResources().getStringArray(R.array.menu_child1_items);
        txt_menu_child2 = getActivity().getResources().getStringArray(R.array.menu_child2_items);
        txt_menu_child3 = getActivity().getResources().getStringArray(R.array.menu_child3_items);
        txt_menu_child4 = getActivity().getResources().getStringArray(R.array.menu_child4_items);

       /* img_menu_parent = getActivity().getResources().getIntArray(R.array.menu_parent_images);*/
        img_menu_parent = new int[]{R.drawable.releases, R.drawable.audio_books, R.drawable.subscription,
                R.drawable.articles, R.drawable.consulting, R.drawable.training, R.drawable.daily_quotes,
                R.drawable.payment, R.drawable.contact_us, R.drawable.ic_about_company, R.drawable.notifications, R.drawable.settings, R.drawable.ic_sign_out};
        /*img_menu_child = getActivity().getResources().getIntArray(R.array.menu_child_images)*/
        ;
        img_menu_child1 = new int[]{R.drawable.call_us, R.drawable.complaints_suggestions, R.drawable.cart};
        img_menu_child2 = new int[]{R.drawable.cart, R.drawable.cart, R.drawable.cart, R.drawable.cart};

        Log.d("ImagesArray", "onCreateView: \n" + img_menu_parent[0] + "\t" + img_menu_child1[1]);

        list_parent = new ArrayList<>();
        list_child = new ArrayList<>();
        list_child2 = new ArrayList<>();
        list_child3 = new ArrayList<>();
        list_child4 = new ArrayList<>();
        list_hash_child = new HashMap<>();

        for (int i = 0; i < txt_menu_child1.length; i++) {
            ModelMenu model_child = new ModelMenu(txt_menu_child1[i]);
            list_child.add(model_child);
        }

        for (int i = 0; i < txt_menu_child2.length; i++) {
            ModelMenu model_child = new ModelMenu(txt_menu_child2[i]);
            list_child2.add(model_child);
        }
        for (int i = 0; i < txt_menu_child3.length; i++) {
            ModelMenu model_child = new ModelMenu(txt_menu_child3[i]);
            list_child3.add(model_child);
        }
        for (int i = 0; i < txt_menu_child4.length; i++) {
            ModelMenu model_child = new ModelMenu(txt_menu_child4[i]);
            list_child4.add(model_child);
        }

        for (int i = 0; i < txt_menu_parent.length; i++) {
            ModelMenu model_parent = new ModelMenu(txt_menu_parent[i], img_menu_parent[i]);
            list_parent.add(model_parent);
        }


        list_hash_child.put(list_parent.get(8), list_child);
        list_hash_child.put(list_parent.get(9), list_child2);
        list_hash_child.put(list_parent.get(0), list_child3);
        list_hash_child.put(list_parent.get(1), list_child4);



        /*if (list_hash_child.get(list_parent.get(9)).size() == 2) {

        }*/

/*

        rx.Observable<String> Samah = rx.Observable.just("Hi", "Hello");
        Observer<String> Sultan = new Observer<String>() {
            @Override
            public void onCompleted() {
                Log.e("Samah", "called shawky: ");
            }

            @Override
            public void onError(Throwable e) {
                Log.e("onError: ", "Samah didn't apppear today");
            }

            @Override
            public void onNext(String s) {
                Log.e("Samah moves and say: ", s);
            }
        };

        Subscription subscriptions = Samah.subscribe(Sultan);
*/

        list_menu_drawer = (ExpandableListView) view.findViewById(R.id.list_side_menu);


        adapter = new MenuAdapter(getActivity(), list_parent, list_hash_child);

        list_menu_drawer.setAdapter(adapter);

        list_menu_drawer.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {

                // String[] menu_parent_text = getActivity().getResources().getStringArray(R.array.menu_parent_items);


                switch (i) {
                    /*case 0:
*//*                        showDefaultFooter();
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //   Toast.makeText(getActivity(), menu_parent_text[0], Toast.LENGTH_SHORT).show();
                       *//**//* startActivity(new Intent(getActivity(), ActivityLogin.class));*//**//*

                        getActivity().findViewById(R.id.home_footer).setVisibility(View.GONE);
                        if (getActivity().findViewById(R.id.fragment_login) == null) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentLogin()).commit();
                        } else {
                            Toast.makeText(getActivity(), "You are in Login", Toast.LENGTH_SHORT).show();
                        }*//*

                        break;*/
                    /*case 1:
                        Toast.makeText(getActivity(), menu_parent_text[1], Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(getActivity(), menu_parent_text[2], Toast.LENGTH_SHORT).show();
                        break;*/
                    case 2:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);

                        if (getActivity().findViewById(R.id.fragment_sound_books) == null) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentSoundLibrary()).addToBackStack("sound_books").commit();
                            DisplayMenuName("الكتب الصوتية");
                        } else {
                            getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "You are in Sound Books", Toast.LENGTH_SHORT).show();
                        }
                        //  Toast.makeText(getActivity(), menu_parent_text[3], Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //   Toast.makeText(getActivity(), menu_parent_text[4], Toast.LENGTH_SHORT).show();

                        if (getActivity().findViewById(R.id.fragment_articles) == null) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentArticles()).addToBackStack("articles").commit();
                            DisplayMenuName("المقالات");
                        } else {
                            getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "You are in Articles", Toast.LENGTH_SHORT).show();
                        }


                        break;
                    case 4:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //Toast.makeText(getActivity(), menu_parent_text[5], Toast.LENGTH_SHORT).show();
                        //  getActivity().findViewById(R.id.home_footer).setVisibility(View.GONE);
                        if (getActivity().findViewById(R.id.fragment_consultant) == null) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentConsultants()).addToBackStack("consultants").commit();
                            DisplayMenuName("الاستشارات");
                        } else {
                            getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "You are in Consultant", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case 5:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);

                        if (getActivity().findViewById(R.id.fragment_training) == null) {
                            fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentTraining()).
                                    addToBackStack("training").commit();
                        } else {

                        }
                        // Toast.makeText(getActivity(), menu_parent_text[6], Toast.LENGTH_SHORT).show();
                        break;
                    case 6:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //  Toast.makeText(getActivity(), menu_parent_text[7], Toast.LENGTH_SHORT).show();
                        // if(getActivity().findViewById(R.id.fragment_quotes)==null){
                        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentQuotes()).addToBackStack("quotes").commit();
                        DisplayMenuName("أقوال يومية");
                        /*}else{

                        }
                */
                        break;
                    case 7:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentPayment()).addToBackStack("payment").commit();
                        DisplayMenuName("الدفع");

                        //  Toast.makeText(getActivity(), menu_parent_text[8], Toast.LENGTH_SHORT).show();
                        break;
                   /* case 10:
                        Toast.makeText(getActivity(), menu_parent_text[10], Toast.LENGTH_SHORT).show();
                        break;*/
                    case 10:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentNotification()).addToBackStack("notifications").commit();
                        DisplayMenuName("الاشعارات");
                        //  Toast.makeText(getActivity(), menu_parent_text[11], Toast.LENGTH_SHORT).show();
                        break;
                    case 11:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentSettings()).addToBackStack("Pathes/settings").commit();
                        DisplayMenuName("الإعدادات");
                        //  Toast.makeText(getActivity(), menu_parent_text[12], Toast.LENGTH_SHORT).show();
                        break;
                    case 12:
                        /*showDefaultFooter();
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);*/
                        //  Toast.makeText(getActivity(), menu_parent_text[12], Toast.LENGTH_SHORT).show();

                        getActivity().startActivity(new Intent(getActivity(), ActivityLogin.class));
                        break;


                }

                return false;
            }
        });


        // @param id The row id of the child that was clicked
        list_menu_drawer.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {

                switch (groupPosition) {
                    case 0:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        // Toast.makeText(getActivity(), "" + list_child3.get(childPosition).getMenu_txt(), Toast.LENGTH_SHORT).show();
                        switch (childPosition) {
                            case 0:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatManager()).addToBackStack("khoulasat_manager").commit();
                                break;
                            case 1:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatTarbiahWaTa2lim()).addToBackStack("khoulasat_tarbiah").commit();
                                break;
                            case 2:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatMokhtarEdary()).addToBackStack("khoulasat_mokhtar").commit();
                                break;
                            case 3:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatRelations()).addToBackStack("khoulasat_relations").commit();
                                break;
                            case 4:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatSehatikTharwtik()).addToBackStack("khoulasat_sehatk").commit();
                                break;
                            case 5:
                                fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentKhoulasatArabicBooks()).addToBackStack("khoulasat_books").commit();
                                break;

                        }
                        break;
                    case 1:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //   Toast.makeText(getActivity(), "" + list_child4.get(childPosition).getMenu_txt(), Toast.LENGTH_SHORT).show();
                        switch (childPosition) {
                            case 0:
                                if (getActivity().findViewById(R.id.fragment_main_sub) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentMainSub()).addToBackStack("main_sub").commit();
                                    DisplayMenuName("الاشتراكات");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                                }

                                break;
                            case 1:
                                if (getActivity().findViewById(R.id.fragment_sub_managers) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentManagers()).addToBackStack("Pathes/managers").commit();
                                    DisplayMenuName("مكتبات المديرين المتخصصة");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 2:
                                if (getActivity().findViewById(R.id.fragment_moassy) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentMoassy()).addToBackStack("moassy").commit();
                                    DisplayMenuName("الإشتراك المؤسسي");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }

                        break;
                    case 8:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //  Toast.makeText(getActivity(), "" + list_child.get(childPosition).getMenu_txt(), Toast.LENGTH_SHORT).show();
                        switch (childPosition) {
                            case 0:
                                if (getActivity().findViewById(R.id.fragment_contact_us) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentContactUs()).addToBackStack("call_us").commit();
                                    DisplayMenuName("اتصل بنا");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in Contact Us", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                if (getActivity().findViewById(R.id.fragment_compalints) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentComplaints()).addToBackStack("complaints").commit();
                                    DisplayMenuName("الشكاوي و المقترحات");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in Complaints", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 2:
                                if (getActivity().findViewById(R.id.fragment_support) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentHelpSupport()).addToBackStack("help").commit();
                                    DisplayMenuName("المساعدة و الدعم");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in Help", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }

                        break;

                    case 9:
                        showDefaultFooter(getActivity());
                        getActivity().findViewById(R.id.lin_menu).setTranslationX(0);
                        getActivity().findViewById(R.id.lin_home_content).setTranslationX(0);
                        //  Toast.makeText(getActivity(), "" + list_child2.get(childPosition).getMenu_txt(), Toast.LENGTH_SHORT).show();
                        switch (childPosition) {
                            case 0:
                                if (getActivity().findViewById(R.id.fragment_about) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentAbout()).addToBackStack("about_us").commit();
                                    DisplayMenuName("من نحن");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in About Us", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 1:
                                //fragment_privacy

                                if (getActivity().findViewById(R.id.fragment_privacy) == null) {
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentPrivacy()).addToBackStack("privacy").commit();
                                    DisplayMenuName("سياسة الخصوصية");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in Privacy", Toast.LENGTH_SHORT).show();
                                }
                                break;
                            case 2:
                                if (getActivity().findViewById(R.id.fragment_tell_friend) == null) {
                                    FragmentTellAfriend friend = new FragmentTellAfriend();
                                    fragmentManager.beginTransaction().replace(R.id.frame_home, friend).addToBackStack("tell_friend").commit();
                                    //  friend.show(fragmentManager,"Tell A friend ");
                                    // DisplayMenuName("سياسة الخصوصية");
                                } else {
                                    getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "You are in Tell a friend", Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                        break;
                }
                return false;
            }
        });
        return view;
    }

    public void DisplayMenuName(String MenuName) {
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView v = (TextView) getActivity().findViewById(R.id.txt_logo);
        getActivity().findViewById(R.id.img_logo).setVisibility(View.GONE);
        v.setText(MenuName);
        getActivity().findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
    }

}
