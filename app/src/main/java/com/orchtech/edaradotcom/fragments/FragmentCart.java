package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.CardDataAdapter;
import com.orchtech.edaradotcom.managers.ShoppingCart.CartDataManager;
import com.orchtech.edaradotcom.managers.ShoppingCart.DeleteAllInvoicesManager;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.models.cart.ShoppingCartResponseModel;
import com.orchtech.edaradotcom.settings.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class FragmentCart extends Fragment {

    static String Session;
    static double TotalPrice = 0.0;
    static CartDataManager cartDataManager;
    static RecyclerView rec_cart;
    static CardDataAdapter cardDataAdapter;
    static TextView txt_total_amount, txt_clear_all_cart;
    static DeleteAllInvoicesManager deleteAllInvoicesManager;
    FragmentManager manager;

    public static void getShoppingCart(final View view, final FragmentActivity con) {

        if (cartDataManager == null) {
            cartDataManager = new CartDataManager();
        }

        cartDataManager.get_cart_data(Session).enqueue(new Callback<ShoppingCartResponseModel>() {
            @Override
            public void onResponse(Call<ShoppingCartResponseModel> call, Response<ShoppingCartResponseModel> response) {
                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getShoppingCartModelList() == null || response.body() == null) {
                        Snackbar.make(view, "لا توجد لديك اي مشتريات", Snackbar.LENGTH_SHORT).show();
                        txt_total_amount.setText("0 $");
                    } else {
                        cardDataAdapter = new CardDataAdapter(response.body().getShoppingCartModelList(), con);
                        rec_cart.setAdapter(cardDataAdapter);
                        cardDataAdapter.notifyDataSetChanged();

                        for (int i = 0; i < response.body().getShoppingCartModelList().size(); i++) {
                            TotalPrice += Double.parseDouble(response.body().getShoppingCartModelList().get(i).getFinalPrice());
                        }
                        txt_total_amount.setText(TotalPrice + " $");
                    }
                } else {
                    con.startActivity(new Intent(con, BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<ShoppingCartResponseModel> call, Throwable t) {
                Snackbar.make(view, con.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });
        TotalPrice = 0.0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Session = Preferences.getFromPreference(getActivity(), "session_id");
        manager = getActivity().getSupportFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_cart, container, false);


        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("سلة المشتريات");
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());


        txt_total_amount = (TextView) v.findViewById(R.id.txt_cart_total);
        txt_clear_all_cart = (TextView) v.findViewById(R.id.txt_clear_all_cart);

        rec_cart = (RecyclerView) v.findViewById(R.id.rec_cart_data);
        rec_cart.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (getActivity().findViewById(R.id.frame_payment) == null) {
            manager.beginTransaction().add(R.id.frame_payment, new FragmentPayment()).commit();
        }
        txt_clear_all_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (TotalPrice == 0.0) {
                    FragmentForgotPass.showMsg("السلة بالفعل فارغة", "سلة المشتريات", getActivity()).show();
                } else {

                    if (deleteAllInvoicesManager == null) {
                        deleteAllInvoicesManager = new DeleteAllInvoicesManager();
                    }
                    deleteAllInvoicesManager.DeleteAllInvoices(Session).
                            enqueue(new Callback<ValidationModel.Success>() {
                                @Override
                                public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {
                                    if (response.code() >= 200 && response.code() < 300) {
                                        if (response.body() == null) {
                                        } else {
                                            if (response.body().isSuccess()) {

                                                FragmentForgotPass.showMsg("تم محو سلة مشترياتك بنجاح", "سلة المشتريات", getActivity()).show();
                                                getShoppingCart(v, getActivity());
                                            } else {
                                                FragmentForgotPass.showMsg(response.body().getMessage(), "سلة المشتريات", getActivity()).show();
                                            }
                                        }
                                    } else {
                                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                                    }
                                }

                                @Override
                                public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                                    Snackbar.make(getView(), getActivity().getResources().getString(R.string.internet_failure),
                                            Snackbar.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });


        getShoppingCart(v, getActivity());


        return v;


    }
}
