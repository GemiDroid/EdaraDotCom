package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class FragmentKhoulasatMokhtarEdary extends Fragment {


    TextView txt_most_read, txt_month_edition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragmnent_khoulasat_manager, container, false);

        FragmentKhoulasatManager.getKhoulasatEdition(v, "المختار الاداري", getActivity(), 8);

        getActivity().findViewById(R.id.img_kholasat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                FragmentKhoulasatManager.InfoDialog(8, getActivity(), v);
            }
        });

        txt_most_read = (TextView) v.findViewById(R.id.txt_more_read);


        txt_most_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentKhoulasatManager.GetMostRead(8, getActivity(), v);

            }
        });

        txt_month_edition = (TextView) v.findViewById(R.id.txt_month_edition);
        txt_month_edition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentKhoulasatManager.GetMonthEdition(8, v, getActivity());
            }
        });


        return v;


    }


}
