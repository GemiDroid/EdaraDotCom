package com.orchtech.edaradotcom.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.orchtech.edaradotcom.R;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */


@Deprecated
public class FragmentSplashScreen extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_splash_screen, container, false);

/*
        new Runnable() {

            @Override
            public void run() {



            }
        };*/

        return v;
    }
}
