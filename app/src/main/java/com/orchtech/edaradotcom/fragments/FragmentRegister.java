package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.managers.Account.CountriesRegisterManager;
import com.orchtech.edaradotcom.managers.Account.RegisterManager;
import com.orchtech.edaradotcom.models.CountriesRegisterModel;
import com.orchtech.edaradotcom.models.Fonts;
import com.orchtech.edaradotcom.models.ValidationModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 12/28/2016.
 */

public class FragmentRegister extends Fragment {

    static String url;
    static String TAG = FragmentRegister.class.getSimpleName();
    static String country, phone;
    static List<String> CountriesList;
    TextView txt_register;
    Button btn_participate;
    FragmentManager fragmentManager;
    EditText edt_user, edt_email, edt_phone;
    AutoCompleteTextView edt_countries;
    CountriesRegisterManager countriesRegisterManager;
    RegisterManager registerManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        // url = Preferences.getUrlHeader();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);

        fragmentManager = getActivity().getSupportFragmentManager();

        btn_participate = (Button) view.findViewById(R.id.btn_participate);
        edt_user = (EditText) view.findViewById(R.id.edt_user);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_phone = (EditText) view.findViewById(R.id.edt_mobile);
        edt_countries = (AutoCompleteTextView) view.findViewById(R.id.edt_country);
        txt_register = (TextView) view.findViewById(R.id.txt_register);


        /*getActivity().findViewById(R.id.img_login_back).setVisibility(View.VISIBLE);

        getActivity().findViewById(R.id.img_login_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager.beginTransaction().replace(R.id.frame_login, new FragmentLogin()).commit();
            }
        });*/


        get_fonts();

        GetAllCountries();

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, CountriesList);
        edt_countries.setThreshold(1);
        edt_countries.setAdapter(spinnerAdapter);

        btn_participate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompleteRegistration();
            }
        });

        return view;
    }

    private void CompleteRegistration() {
        if (edt_user.getText().toString().equals("")) {
            edt_user.setHint("من فضلك أدخل اسمك");
        } else if (edt_email.getText().toString().equals("")) {
            edt_email.setHint("من فضلك أدخل ايميلك");
        } else if (!FragmentForgotPass.check_email(edt_email.getText().toString())) {
            FragmentForgotPass.showMsg("صيغة الايميل غير صحيحه", "التسجيل", getActivity()).show();
        } /*else if (edt_phone.getText().toString().equals("")) {
            edt_phone.setHint("من فضلك ادخل رقم هاتفهك");
        }*/ else {
            if (registerManager == null) {
                registerManager = new RegisterManager();
            }

            country = edt_countries.getText().toString().equals("") ? "null" : edt_countries.getText().toString();
            phone = edt_phone.getText().toString().equals("") ? "" : edt_phone.getText().toString();
            registerManager.get_register(edt_email.getText().toString(), phone, edt_user.getText()
                    .toString(), country).enqueue(new Callback<ValidationModel>() {
                @Override
                public void onResponse(Call<ValidationModel> call, Response<ValidationModel> response) {
                    getActivity().findViewById(R.id.login_progress).setVisibility(View.VISIBLE);
                    if (response.code() >= 200 & response.code() <= 300) {

                        if (response.body() == null) {
                            Snackbar.make(getView(), "لا توجد بيانات", Snackbar.LENGTH_SHORT).show();
                        } else {


                            Log.d(TAG, "onResponse: " + response.body().getSuccess().getMessage());
                            boolean Success = response.body().getSuccess().isSuccess();
                            String msg = response.body().getSuccess().getMessage();

                            if (Success) {
                                FragmentForgotPass.showMsg(msg, "التسجيل", getActivity()).show();
                                Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                            } else {
                                FragmentForgotPass.showMsg(msg, "التسجيل", getActivity()).show();
                                Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG).show();
                                getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                            }
                        }
                    } else {
                        startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                        // Snackbar.make(getView(), "نحن حاليا بصدد تعديلات على هذه الخدمه", Snackbar.LENGTH_SHORT).show();
                        getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ValidationModel> call, Throwable t) {
                    Snackbar.make(getView(), "من فضلك تأكد من اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                    getActivity().findViewById(R.id.login_progress).setVisibility(View.GONE);
                }
            });

        }
    }

    private void GetAllCountries() {

        if (countriesRegisterManager == null) {
            countriesRegisterManager = new CountriesRegisterManager();
        }
        CountriesList = new ArrayList<>();
        countriesRegisterManager.get_all_countries().
                enqueue(new Callback<List<CountriesRegisterModel>>() {
                    @Override
                    public void onResponse(Call<List<CountriesRegisterModel>> call, Response<List<CountriesRegisterModel>> response) {

                        if (response.code() >= 200 && response.code() < 300) {

                            if (response.body() == null) {
                            } else {

                                for (int i = 0; i < response.body().size(); i++) {
                                    CountriesList.add(response.body().get(i).getName());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<CountriesRegisterModel>> call, Throwable t) {
                        Snackbar.make(getView(), "من فضلك تأكد من اتصالك بالانترنت", Snackbar.LENGTH_SHORT).show();
                    }
                });


    }

    public void get_fonts() {
        txt_register.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
        btn_participate.setTypeface(Fonts.get_fonts(getActivity(), "DroidKufi-Regular"));
    }


}
