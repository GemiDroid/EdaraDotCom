package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.MoassyAdapter;
import com.orchtech.edaradotcom.managers.PrivacyManager;
import com.orchtech.edaradotcom.models.PrivacyModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class FragmentPrivacy extends Fragment {


    PrivacyManager privacyManager;
    MoassyAdapter moassyAdapter;
    RecyclerView rec_privacy_body;
    TextView txt_header, txt_instructions, txt_privacy_back;
    Button btn_privacy_language;
    LinearLayout lin_privacy;
    ProgressBar prog_privacy;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_privacy, container, false);

        rec_privacy_body = (RecyclerView) view.findViewById(R.id.rec_privacy_body);
        rec_privacy_body.setItemAnimator(new DefaultItemAnimator());
        rec_privacy_body.setLayoutManager(new LinearLayoutManager(getActivity()));

        txt_header = (TextView) view.findViewById(R.id.txt_privacy_header);
        txt_instructions = (TextView) view.findViewById(R.id.txt_privacy_instruction);
        txt_privacy_back = (TextView) view.findViewById(R.id.txt_privacy_back);
        prog_privacy = (ProgressBar) view.findViewById(R.id.prog_privacy);
        btn_privacy_language = (Button) view.findViewById(R.id.btn_privacy_language);
        lin_privacy = (LinearLayout) view.findViewById(R.id.fragment_privacy);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());


        lin_privacy.setVisibility(GONE);
        //Align View to Right
        txt_header.setGravity(Gravity.RIGHT);
        rec_privacy_body.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        txt_instructions.setGravity(Gravity.RIGHT);
        txt_privacy_back.setGravity(Gravity.RIGHT);
        ReteivePrivacy(true);
        lin_privacy.setVisibility(View.VISIBLE);

        btn_privacy_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btn_privacy_language.getText().equals("Arabic")) {


                    //Align View to Left
                    txt_header.setGravity(Gravity.RIGHT);
                    rec_privacy_body.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    txt_instructions.setGravity(Gravity.RIGHT);
                    txt_privacy_back.setGravity(Gravity.RIGHT);

                    ReteivePrivacy(true);
                    txt_privacy_back.setText("سياسة الاستخدام");
                    btn_privacy_language.setText("اللغة الانجليزية");
                } else {
                    //Align View to Left
                    txt_header.setGravity(Gravity.LEFT);
                    rec_privacy_body.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

                    txt_instructions.setGravity(Gravity.LEFT);
                    txt_privacy_back.setGravity(Gravity.LEFT);

                    ReteivePrivacy(false);
                    btn_privacy_language.setText("Arabic");
                    txt_privacy_back.setText("Privacy Policy");
                }
            }
        });

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الخصوصية");


        return view;
    }

    private void ReteivePrivacy(final Boolean isArabic) {

        prog_privacy.setVisibility(View.VISIBLE);
        if (privacyManager == null) {
            privacyManager = new PrivacyManager();
        }


        privacyManager.Privacy(isArabic).enqueue(new Callback<List<PrivacyModel>>() {
            @Override
            public void onResponse(Call<List<PrivacyModel>> call, Response<List<PrivacyModel>> response) {

                if (response.code() <= 200 && response.code() < 300) {

                    if (response.body() == null) {
                        prog_privacy.setVisibility(View.GONE);
                    } else {
                        txt_header.setText(response.body().get(0).getHeader());
                        txt_instructions.setText(response.body().get(0).getInstruction());
                        moassyAdapter = new MoassyAdapter(response.body().get(0).getBody(), getActivity(), isArabic);
                        rec_privacy_body.setAdapter(moassyAdapter);
                        prog_privacy.setVisibility(View.GONE);
                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                    prog_privacy.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<PrivacyModel>> call, Throwable t) {

                Log.e("Error", "onFailure:" + t.getMessage());
                Snackbar.make(getView(), getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                prog_privacy.setVisibility(View.GONE);
            }
        });
    }
}
