package com.orchtech.edaradotcom.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.ProductsAdapter;
import com.orchtech.edaradotcom.managers.Khoulasat.KholasatInfoManager;
import com.orchtech.edaradotcom.managers.Khoulasat.MostVisitedManager;
import com.orchtech.edaradotcom.managers.Khoulasat.RecentBooksManager;
import com.orchtech.edaradotcom.managers.Khoulasat.YearEditionManager;
import com.orchtech.edaradotcom.managers.Products.SubjectsManager;
import com.orchtech.edaradotcom.models.KhoulastDialogResponse;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class FragmentKhoulasatManager extends Fragment {


    static int NoOfCicks = 0;
    static List<Integer> IssuedYearList;
    static List<String> IssuedSubjectList;
    static int CurrentYear;
    static Spinner spin_edition_year, spin_edition_subject;
    static SubjectsManager subjectsManager;
    static MostVisitedManager mostVisitedManager;
    static YearEditionManager yearEditionManager;
    static RecyclerView rec_edition_month, rec_edition_year;
    static ProductsAdapter productsAdapter;
    static TextView txt_month_edition, txt_colorful_month;
    static TextView txt_most_read, txt_colorful_read, txt_current_year;
    static String SessionId;
    static RecentBooksManager recentBooksManager;
    static KholasatInfoManager kholasatInfoManager;


    public static void getKhoulasatEdition(View v, String Title, FragmentActivity con, int ProductID) {

        SessionId = Preferences.getFromPreference(con, "session_id");

        con.findViewById(R.id.img_logo).setVisibility(GONE);
        con.findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        con.findViewById(R.id.img_kholasat).setVisibility(View.VISIBLE);

        TextView txt_logo = (TextView) con.findViewById(R.id.txt_logo);
        txt_logo.setText(Title);


        FragmentSideMenu.showDefaultFooter(con);


        spin_edition_subject = (Spinner) v.findViewById(R.id.spin_edition_subject);
        spin_edition_year = (Spinner) v.findViewById(R.id.spin_edition_year);

        txt_month_edition = (TextView) v.findViewById(R.id.txt_month_edition);
        txt_colorful_month = (TextView) v.findViewById(R.id.txt_month_colorful);

        txt_current_year = (TextView) v.findViewById(R.id.txt_current_year);

        txt_most_read = (TextView) v.findViewById(R.id.txt_more_read);
        txt_colorful_read = (TextView) v.findViewById(R.id.txt_more_read_colorful);

        GridLayoutManager mLayoutYear = new GridLayoutManager(con, 1,
                GridLayoutManager.HORIZONTAL, false);
        GridLayoutManager mLayoutMonth = new GridLayoutManager(con, 1,
                GridLayoutManager.HORIZONTAL, false);

        rec_edition_month = (RecyclerView) v.findViewById(R.id.rec_edition_month);
        //rec_edition_month.setHasFixedSize(true);

        rec_edition_year = (RecyclerView) v.findViewById(R.id.rec_edition_year);
        //rec_edition_year.setHasFixedSize(true);

        rec_edition_month.setLayoutManager(mLayoutMonth);
        rec_edition_year.setLayoutManager(mLayoutYear);


        CurrentYear = Calendar.getInstance().get(Calendar.YEAR);


        IssuedSubjectList = new ArrayList<>();
        IssuedYearList = new ArrayList<>();
        GetIssuedYears();

        ArrayAdapter<Integer> YearAdapter = new ArrayAdapter<>(con, R.layout.spinner_text, IssuedYearList);
        YearAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_edition_year.setAdapter(YearAdapter);

        GetIssuedSubjects(con, v);


        ArrayAdapter<String> SubjectAdapter = new ArrayAdapter<>(con, R.layout.spinner_text, IssuedSubjectList);
        SubjectAdapter.setDropDownViewResource(R.layout.spinner_text);
        spin_edition_subject.setAdapter(SubjectAdapter);

        DisplayYearEdition(CurrentYear, con, v, ProductID);

        txt_current_year.setText(CurrentYear + "");
    }

    public static void DisplayYearEdition(int Year, final FragmentActivity act, final View v, int ProductTypeId) {

        if (yearEditionManager == null) {
            yearEditionManager = new YearEditionManager();
        }
        yearEditionManager.get_edition_year(1, ProductTypeId, 50, SessionId, Year)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body().getItems() == null || response.body() == null) {
                            } else {
                                productsAdapter = new ProductsAdapter(response.body().getItems(), act);
                                rec_edition_year.setAdapter(productsAdapter);
                                productsAdapter.notifyDataSetChanged();

                            }
                        } else {
                            act.startActivity(new Intent(act, BackEndFailureDialog.class));
                        }

                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        Snackbar.make(v.getRootView(), act.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });


    }


    public static void GetIssuedYears() {
        for (int i = 0; i < 30; i++) {
            IssuedYearList.add(CurrentYear - i);
        }
    }

    public static void InfoDialog(final int ProductTypeId, final FragmentActivity con, final View v) {


        if (kholasatInfoManager == null) {
            kholasatInfoManager = new KholasatInfoManager();
        }

        kholasatInfoManager.get_kholasat_info(ProductTypeId).enqueue(new Callback<KhoulastDialogResponse>() {
            @Override
            public void onResponse(Call<KhoulastDialogResponse> call, Response<KhoulastDialogResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null || response.body().getKhoulasatManager() == null) {
                    } else {


                        ShowKhoulasatDialog(con, response.body().getKhoulasatManager().getAbout()
                                , response.body().getKhoulasatManager().getName()).show();


                    }
                } else {
                    con.startActivity(new Intent(con, BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<KhoulastDialogResponse> call, Throwable t) {

                Snackbar.make(v.getRootView(), con.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
            }
        });


    }

    private static AlertDialog ShowKhoulasatDialog(FragmentActivity con, String about, String name) {

        final AlertDialog alertDialog = new AlertDialog.Builder(con).create();


        TextView tv = new TextView(con);
        tv.setGravity(Gravity.CENTER);
        tv.setText(name);
        tv.setTextColor(con.getResources().getColor(R.color.colorOrange));
        tv.setTextSize(20);
        alertDialog.setCustomTitle(tv);
        alertDialog.setMessage(about);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "حسناً", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setCancelable(false);
        return alertDialog;


    }

    public static void GetIssuedSubjects(final FragmentActivity act, final View v) {

        if (subjectsManager == null) {
            subjectsManager = new SubjectsManager();
        }

        subjectsManager.get_all_subjects().enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body().getSubjectsList() == null || response.body() == null) {
                    } else {

                        for (int i = 0; i < response.body().getSubjectsList().size(); i++) {

                            IssuedSubjectList.add(response.body().getSubjectsList().get(i).getText_Ar());
                        }
                    }
                } else {
                    act.startActivity(new Intent(act, BackEndFailureDialog.class));
                }

            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {

                Snackbar.make(v.getRootView(), act.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

            }
        });
    }

    public static void GetMostRead(int ProductTypeId, final FragmentActivity con, final View v) {

        txt_colorful_read.setVisibility(View.VISIBLE);
        txt_colorful_month.setVisibility(GONE);

        if (mostVisitedManager == null) {
            mostVisitedManager = new MostVisitedManager();
        }

        mostVisitedManager.get_most_visited(ProductTypeId, 50, SessionId)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body() == null || response.body().getItems() == null) {
                            } else {
                                productsAdapter = new ProductsAdapter(response.body().getItems(), con);
                                rec_edition_month.setAdapter(productsAdapter);
                            }
                        } else {
                            con.startActivity(new Intent(con, BackEndFailureDialog.class));
                        }

                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        Snackbar.make(v.getRootView(), con.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

                    }
                });
    }

    public static void GetMonthEdition(int ProductTypeId, final View v, final FragmentActivity con) {

        txt_colorful_read.setVisibility(View.GONE);
        txt_colorful_month.setVisibility(View.VISIBLE);

        if (recentBooksManager == null) {
            recentBooksManager = new RecentBooksManager();
        }
        recentBooksManager.get_recent_books(50, ProductTypeId, SessionId)
                .enqueue(new Callback<ProductsResponse>() {
                    @Override
                    public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {

                        if (response.code() >= 200 && response.code() < 300) {

                            if (response.body().getItems() == null || response.body() == null) {
                            } else {
                                productsAdapter = new ProductsAdapter(response.body().getItems(), con);
                                rec_edition_month.setAdapter(productsAdapter);
                            }

                        } else {
                            con.startActivity(new Intent(con, BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsResponse> call, Throwable t) {
                        Snackbar.make(v.getRootView(), con.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragmnent_khoulasat_manager, container, false);


        getKhoulasatEdition(v, "خلاصات المدير و رجال الاعمال", getActivity(), 4);

        getActivity().findViewById(R.id.img_kholasat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                InfoDialog(4, getActivity(), v);


            }
        });

        txt_most_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetMostRead(4, getActivity(), v);

            }
        });

        txt_month_edition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetMonthEdition(4, v, getActivity());
            }
        });


        return v;


    }


}
