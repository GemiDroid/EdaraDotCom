package com.orchtech.edaradotcom.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.adapters.MoassyAdapter;
import com.orchtech.edaradotcom.managers.SubscriptionTypes.MoassySubscriptionManager;
import com.orchtech.edaradotcom.models.Subscription.MoassyModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 4/17/2017.
 */

public class FragmentMoassy extends Fragment {


    static String TAG = FragmentMoassy.class.getSimpleName();
    MoassyAdapter moassyAdapter;
    RecyclerView rec_moassy;
    MoassySubscriptionManager moassySubscriptionManager;
    TextView txt_moassy_title, txt_moassy_details;
    android.support.v4.app.FragmentManager fragmentManager;

    Button btn_moassy_request, btn_moassy_more;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        fragmentManager = getActivity().getSupportFragmentManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_moassy, container, false);

        txt_moassy_title = (TextView) v.findViewById(R.id.txt_moassy_title);
        txt_moassy_details = (TextView) v.findViewById(R.id.txt_moassy_details);

        rec_moassy = (RecyclerView) v.findViewById(R.id.rec_moassy_details);
        rec_moassy.setLayoutManager(new LinearLayoutManager(getActivity()));
        rec_moassy.setItemAnimator(new DefaultItemAnimator());

        btn_moassy_more = (Button) v.findViewById(R.id.btn_moassy_more);
        btn_moassy_request = (Button) v.findViewById(R.id.btn_moassy_request);
        getActivity().findViewById(R.id.img_kholasat).setVisibility(View.GONE);
        FragmentSideMenu.showDefaultFooter(getActivity());

        if (moassySubscriptionManager == null) {
            moassySubscriptionManager = new MoassySubscriptionManager();
        }
        moassySubscriptionManager.get_MoassySubscription().enqueue(new Callback<MoassyModel>() {
            @Override
            public void onResponse(Call<MoassyModel> call, Response<MoassyModel> response) {

                if (response.code() >= 200 && response.code() < 300) {
                    if (response.body() == null) {
                    } else {

                        txt_moassy_details.setText(response.body().getTitle());
                        txt_moassy_title.setText(response.body().getIntro());
                        moassyAdapter = new MoassyAdapter(response.body().getBulletsList(), getActivity(), true);
                        rec_moassy.setAdapter(moassyAdapter);

                        Log.d(TAG, "Intro.: " + response.body().getIntro());

                    }
                } else {
                    startActivity(new Intent(getActivity(), BackEndFailureDialog.class));
                }
            }

            @Override
            public void onFailure(Call<MoassyModel> call, Throwable t) {

            }
        });

        btn_moassy_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestOrder();
            }
        });

        btn_moassy_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReadMore();
            }
        });

        getActivity().findViewById(R.id.img_logo).setVisibility(GONE);
        getActivity().findViewById(R.id.txt_logo).setVisibility(View.VISIBLE);
        TextView txt_logo = (TextView) getActivity().findViewById(R.id.txt_logo);
        txt_logo.setText("الاشتراك المؤسسي");

        return v;

    }

    private void ReadMore() {
        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentMoreMoassy())
                .addToBackStack("read_more").commit();
    }
    private void RequestOrder() {
        fragmentManager.beginTransaction().replace(R.id.frame_home, new FragmentRequestMoassy())
                .addToBackStack("request_more").commit();
    }
}
