package com.orchtech.edaradotcom.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;


/**
 * Created by ahmed.yousef on 4/12/2017.
 */

public class AllDialog {


    public static AlertDialog display_dialog(final Activity act) {

        AlertDialog dialog_exit = new AlertDialog.Builder(act).create();
        TextView tv = new TextView(act);
        tv.setTextColor(act.getResources().getColor(R.color.black_color));
        tv.setText(R.string.app_name);
        tv.setTextSize(25);
        tv.setGravity(Gravity.CENTER);
        dialog_exit.setCustomTitle(tv);
        dialog_exit.setMessage("هل تريد الخروج ؟");
        dialog_exit.setButton(DialogInterface.BUTTON_POSITIVE, "موافق", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                act.startActivity(startMain);
                act.finish();

            }
        });
        dialog_exit.setButton(DialogInterface.BUTTON_NEGATIVE, "الغاء", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return dialog_exit;

    }


    public static AlertDialog display_dialog(Context context, String Message) {

        AlertDialog dialog_buy = new AlertDialog.Builder(context).create();
        dialog_buy.setTitle("إدارة دوت كوم");
        dialog_buy.setMessage(Message);
        dialog_buy.setButton(DialogInterface.BUTTON_POSITIVE, "موافق", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return dialog_buy;

    }

}
