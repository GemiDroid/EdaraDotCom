/*
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import edaradotcom.R;


class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "";
    private final static String DATABASE_NAME = "pdf_database";
    private final static int DATABASE_VERSION = 1;
    private final static String NIVEAU = "niveau";
    private static final String COL_NIVEAU_ID = "_id";
    private static final String NIVEAU_NAME = "name";
    private final static String CLASSE = "classe";
    private static final String COL_CLASSE_ID = "id";
    private static final String KEY_ID_Niveau = "id_niveau";
    private static final String CLASSE_NAME = "name";
    private final static String CAT = "categorie";
    private static final String COL_CAT_ID = "id";
    private static final String KEY_ID_Classe = "id_classe";
    private static final String CAT_NAME = "name";
    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
            + NIVEAU + "("
            + COL_NIVEAU_ID + " integer PRIMARY KEY autoincrement,"
            + NIVEAU_NAME + " TEXT NULL " + ")";
    private static final String CREATE_TABLE_C = "CREATE TABLE "
            + CLASSE + "("
            + COL_CLASSE_ID + " INTEGER PRIMARY KEY  null,"
            + CLASSE_NAME + " TEXT  null,"

            + KEY_ID_Niveau + " INTEGER null,"
            + " FOREIGN KEY (" + KEY_ID_Niveau + ") REFERENCES " + NIVEAU + "(" + COL_NIVEAU_ID + "));"
            + ")";
    private static final String CREATE_TABLE_CAT = "CREATE TABLE "
            + CAT + "("
            + COL_CAT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT null,"
            + CAT_NAME + " TEXT  null,"

            + KEY_ID_Classe + " INTEGER null,"
            + " FOREIGN KEY (" + KEY_ID_Classe + ") REFERENCES " + CLASSE + "(" + COL_CLASSE_ID + "));"
            + ")";
    String value;
    String valueclasse;

    private Context context;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

        this.context = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);
        db.execSQL(CREATE_TABLE_C);
        db.execSQL(CREATE_TABLE_CAT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NIVEAU);
        db.execSQL("DROP TABLE IF EXISTS " + CLASSE);
        db.execSQL("DROP TABLE IF EXISTS " + CAT);

        onCreate(db);

    }

    public void addNiveau(Niveau Data) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NIVEAU_NAME, Data.getName());

        db.insert(NIVEAU, null, values);


        db.close();

    }

    public List<Niveau> getAllNiveau() {
        List<Niveau> NiveauList = new ArrayList<Niveau>();

        String selectQuery = "SELECT  * FROM " + NIVEAU;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Niveau data = new Niveau();
                data.setId(Integer.parseInt(cursor.getString(0)));
                data.setName(cursor.getString(1));

                // Adding contact to list
                NiveauList.add(data);
            } while (cursor.moveToNext());
        }

        return NiveauList;

    }

    public void addClasse(Classe Data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CLASSE_NAME, Data.getName());

        values.put(KEY_ID_Niveau, Data.getIdN());
        db.insert(CLASSE, null, values);
        db.close();
    }

    public List<Classe> getAllClasse() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString("key", null);
        List<Classe> ClasseList = new ArrayList<Classe>();

        String selectQuery = "SELECT * FROM " + CLASSE + " WHERE " + KEY_ID_Niveau + " = " + value;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                Classe data = new Classe();
                data.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_CLASSE_ID))));
                data.setIdN(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_Niveau))));
                data.setName(cursor.getString(cursor.getColumnIndex(CLASSE_NAME)));


                // Adding contact to list
                ClasseList.add(data);

            } while (cursor.moveToNext());

        }

        return ClasseList;
    }

    public void addCat(Categorie Data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CAT_NAME, Data.getName());
        values.put(KEY_ID_Classe, Data.getIdS());

        db.insert(CAT, null, values);
        db.close();
    }


    public List<Categorie> getAllCat() {
        SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(context);
        valueclasse = prefs1.getString("k", null);
        List<Categorie> CategorieList = new ArrayList<Categorie>();

        String selectQuery = "SELECT  * FROM " + CAT + " WHERE " + KEY_ID_Classe + " = " + valueclasse;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Categorie data = new Categorie();
                data.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COL_CAT_ID))));
                data.setIdS(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_ID_Classe))));
                data.setName(cursor.getString(cursor.getColumnIndex(CAT_NAME)));

                // Adding contact to list
                CategorieList.add(data);
            } while (cursor.moveToNext());
        }

        return CategorieList;

    }





*/
