package com.orchtech.edaradotcom.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.Digits;
import com.digits.sdk.android.DigitsAuthButton;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.orchtech.edaradotcom.R;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;

import io.fabric.sdk.android.Fabric;

/*import com.mapbox.mapboxsdk.maps.MapView;*/

/**
 * Created by ahmed.yousef on 1/19/2017.
 */

public class ActivityMapBox extends AppCompatActivity {

    private static final String TWITTER_KEY = "VLZBR584YyRXD40Wtl4vSmqn0";

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_SECRET = "9AkUYsSkw7xf0Fh5DwqHZUkt8ElkCpdTv9cyS4DjPjMm2DXaUz";
    /*private MapView mapView = null;*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new TwitterCore(authConfig), new Digits.Builder().build());

       /* mapView = (MapView) findViewById(R.id.mapboxMapView);
       mapView.setAccessToken(getString(R.string.accessToken));
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
             mapboxMap.setStyle(Style.SATELLITE_STREETS);
            }
        });*/

        DigitsAuthButton digitsButton = (DigitsAuthButton) findViewById(R.id.auth_button);
        digitsButton.setCallback(new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                // TODO: associate the session userID with your user model
                Toast.makeText(getApplicationContext(), "Authentication successful for "
                        + phoneNumber, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(DigitsException exception) {
                Log.d("Digits", "Sign in with Digits failure", exception);
            }
        });


        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    @Override
    public void onPause() {
        super.onPause();
        // mapView.onPause();


    }

    @Override
    public void onResume() {
        super.onResume();
        //  mapView.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //   mapView.onDestroy();

    }

    /***
     * Save all appropriate fragment state.*     **     * @param outState Bundle in which to place your saved state.*
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //  mapView.onSaveInstanceState(outState);
    }
}


