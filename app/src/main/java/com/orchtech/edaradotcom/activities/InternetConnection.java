package com.orchtech.edaradotcom.activities;

import android.app.Application;

import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.ConnectionBuddyConfiguration;

/**
 * Created by ahmed.yousef on 3/4/2017.
 */

public class InternetConnection extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ConnectionBuddyConfiguration connectionBuddyActivity = new ConnectionBuddyConfiguration.Builder(this).build();
        ConnectionBuddy.getInstance().init(connectionBuddyActivity);


    }
}
