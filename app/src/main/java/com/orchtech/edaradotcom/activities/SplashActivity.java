package com.orchtech.edaradotcom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.orchtech.edaradotcom.R;

public class SplashActivity extends AppCompatActivity {


    ImageView img_splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        img_splash = (ImageView) findViewById(R.id.img_splash);
        Glide.with(this).load(R.drawable.splash).into(img_splash);

/*

        new Timer("").schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, ActivityLogin.class));
            }
        }, 5000);
*/


        Thread th = new Thread() {
            @Override
            public void run() {

                try {
                    sleep(5000);

                } catch (InterruptedException e) {

                } finally {
                    startActivity(new Intent(SplashActivity.this, ActivityLogin.class));
                }

            }
        };

        th.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
      /*  finish();*/
    }
}



