package com.orchtech.edaradotcom.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentLogin;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;

public class ActivityLogin extends AppCompatActivity implements ConnectivityChangeListener {

    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.frame_login, new FragmentLogin()).addToBackStack("login").commit();


    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {

      /*  int connection_type = event.getType().ordinal();

        if (connection_type == CONNECTED) {
            // device has active internet connection
            Toast.makeText(this, "You are now connected", Toast.LENGTH_SHORT).show();
        } else {
            // there is no active internet connection on this device
            InternetIsuueDialogClass.display_dialog(this).show();
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
        }
*/
    }

    @Override
    public void onBackPressed() {

        try {

            int stack_size = getSupportFragmentManager().getBackStackEntryCount();

            if (stack_size > 1) {

                getSupportFragmentManager().popBackStackImmediate();

            }
        } catch (Exception e) {
        }
    }
}