package com.orchtech.edaradotcom.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

/**
 * Created by ahmed.yousef on 3/4/2017.
 */

public class InternetIsuueDialogClass {

//"Your Data is Off","Turn on data or Wi-Fi in Settings"

    public static AlertDialog display_dialog(final Context con) {

        AlertDialog myAlertDialog = new AlertDialog.Builder(con).create();
        myAlertDialog.setTitle("Your Data is Off");
        myAlertDialog.setMessage("Turn on data or Wi-Fi in Settings");
        myAlertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                con.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        myAlertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        myAlertDialog.setCancelable(false);

        return myAlertDialog;
    }
}
