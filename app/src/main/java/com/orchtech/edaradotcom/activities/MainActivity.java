/*
package activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import edaradotcom.R;

*/
/**
 * Created by ahmed.yousef on 1/23/2017.
 *//*


public class MainActivity extends AppCompatActivity {
    ListView listView;
    private List<Niveau> listniveau;
    private ListNiveauAdapter adapter;
    private DBHelper mDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = (ListView) findViewById(R.id.listview_niveau);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                prefs.edit().putString("key", String.valueOf(listniveau.get(position).getId())).commit();

                Intent intent = new Intent(MainActivity.this, ClasseActivity.class);
                //based on item add info to intent


                startActivity(intent);
            }
        });
        listniveau = new ArrayList<>();


        // insert in DB
        mDBHelper = new DBHelper(this);
        mDBHelper.addNiveau(new Niveau("إبتدائي"));
        mDBHelper.addNiveau(new Niveau("إعدادي"));


        listniveau = mDBHelper.getAllNiveau();

        adapter = new ListNiveauAdapter(this, listniveau);
        //Set adapter for listview
        listView.setAdapter(adapter);


    }
}*/
