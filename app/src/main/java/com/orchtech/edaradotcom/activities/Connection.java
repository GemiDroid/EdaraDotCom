package com.orchtech.edaradotcom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;

/**
 * Created by ahmed.yousef on 1/15/2017.
 */


/**
 * Broadcast receiver that listens to network connectivity changes.
 */
@Deprecated
class Connection extends BroadcastReceiver {

    private Object object;

    private ConnectivityChangeListener mCallback;

    public Connection(Object object, ConnectivityChangeListener mCallback) {
        this.object = object;
        this.mCallback = mCallback;
    }

    /**
     * Receive network connectivity change event.
     */
    @Override
    public void onReceive(Context context, Intent intent) {
       /* boolean hasConnectivity = ConnectionBuddy.getInstance().hasNetworkConnection();
        ConnectionBuddyCache cache = ConnectionBuddy.getInstance().getConfiguration().getMinimumSignalStrength();

        if (hasConnectivity && cache.getLastNetworkState(object) != hasConnectivity) {
            cache.setLastNetworkState(object, hasConnectivity);
            ConnectionBuddy.getInstance().notifyConnectionChange(hasConnectivity, mCallback);
        } else if (!hasConnectivity && cache.getLastNetworkState(object) != hasConnectivity) {
            cache.setLastNetworkState(object, hasConnectivity);
            ConnectionBuddy.getInstance().notifyConnectionChange(hasConnectivity, mCallback);
        }*/
    }
}
