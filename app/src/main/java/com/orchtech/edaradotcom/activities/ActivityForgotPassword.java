package com.orchtech.edaradotcom.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentForgotPass;

/**
 * Created by ahmed.yousef on 12/28/2016.
 */

public class ActivityForgotPassword extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

            fragmentManager.beginTransaction().add(R.id.container_forgot_password,
                    new FragmentForgotPass()).commit();

    }


}
