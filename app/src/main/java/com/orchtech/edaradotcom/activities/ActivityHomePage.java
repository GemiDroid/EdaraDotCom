package com.orchtech.edaradotcom.activities;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentCart;
import com.orchtech.edaradotcom.fragments.FragmentHomePage;
import com.orchtech.edaradotcom.fragments.FragmentMyLibrary;
import com.orchtech.edaradotcom.fragments.FragmentProfile;
import com.orchtech.edaradotcom.fragments.FragmentSearch;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;


/**
 * Created by ahmed.yousef on 1/3/2017.
 */

public class ActivityHomePage extends AppCompatActivity implements View.OnClickListener, ConnectivityChangeListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "Ks6xWKW6qt74UmpXkJiifjQHB";
    private static final String TWITTER_SECRET = "nN6UbEbXDcvCPC9Rpqw6g0RaWYZS8Nh4iHJlbGTP8dlqkUwdPh";


    static boolean flag;
    static ImageView img_search, img_library, img_account, img_home;
    FragmentManager fm = getSupportFragmentManager();
    ImageView img_home_menu, img_cart;
    ImageView img_logo;
    LinearLayout lin_home, lin_search, lin_account, lin_library, lin_home_content, lin_home_menu, lin_home_footer;
    ExpandableListView exp_list_view;
    TextView txt_logo, txt_menu_item;

    public static void BuutonsAction(int advanced_search, int my_library2, int home2, int my_profile2) {
        img_search.setImageResource(advanced_search);
        img_library.setImageResource(my_library2);
        img_home.setImageResource(home2);
        img_account.setImageResource(my_profile2);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        // Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_home_page);

       /*img_logo = (ImageView) findViewById(R.id.img_logo);
        txt_logo = (TextView) findViewById(R.id.txt_logo);
        txt_menu_item = (TextView) findViewById(R.id.txt_menu_item);
        lin_home_footer = (LinearLayout) findViewById(R.id.home_footer);*/

        img_account = (ImageView) findViewById(R.id.img_account);
        img_search = (ImageView) findViewById(R.id.img_search);
        img_library = (ImageView) findViewById(R.id.img_library);
        img_home = (ImageView) findViewById(R.id.img_main);
        img_home_menu = (ImageView) findViewById(R.id.img_menu);
        lin_account = (LinearLayout) findViewById(R.id.lin_account);
        lin_home = (LinearLayout) findViewById(R.id.lin_main);
        lin_search = (LinearLayout) findViewById(R.id.lin_search);
        lin_library = (LinearLayout) findViewById(R.id.lin_library);
        lin_home_content = (LinearLayout) findViewById(R.id.lin_home_content);
        lin_home_menu = (LinearLayout) findViewById(R.id.lin_home_menu);
        img_logo = (ImageView) findViewById(R.id.img_logo);
        img_cart = (ImageView) findViewById(R.id.img_cart);

        exp_list_view = (ExpandableListView) findViewById(R.id.list_side_menu);


        fm.beginTransaction().add(R.id.frame_home, new FragmentHomePage()).addToBackStack("main_home").commit();


        lin_account.setOnClickListener(this);
        lin_home.setOnClickListener(this);
        lin_library.setOnClickListener(this);
        lin_search.setOnClickListener(this);
        img_home_menu.setOnClickListener(this);
        img_logo.setOnClickListener(this);
        img_cart.setOnClickListener(this);

    }


    /*public static void showDefaultFooter(Context context){

        img_account.setImageResource(R.drawable.my_profile2);
        img_search .setImageResource(R.drawable.advanced_search2);
        img_library.setImageResource(R.drawable.my_library3);
        home2.setImageResource(R.drawable.home2);

    }*/

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void get_animation(View v) {
        int cx = v.getWidth() / 2;
        int cy = v.getHeight() / 2;
        float finalRaduis = (float) Math.hypot(cx, cy);
        Animator anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0, finalRaduis);
        anim.setDuration(2000);
        anim.start();
    }

    private void display_menu() {

        try {
            if (flag) {
                this.findViewById(R.id.lin_menu).setTranslationX(0);
                this.findViewById(R.id.lin_home_content).setTranslationX(0);
                flag = false;
            } else {
                this.findViewById(R.id.lin_menu).setTranslationX(-1 * (getResources().getDimension(R.dimen.consultant_text)));
                this.findViewById(R.id.lin_home_content).setTranslationX(-1 * (getResources().getDimension(R.dimen.consultant_text)));
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                get_animation(findViewById(R.id.lin_menu));
            }*/
                flag = true;
            }

            // getActivity().findViewById(R.id.lin_layout_menu).setTranslationX(0);
       /* } else {
            lin_menu.setVisibility(View.VISIBLE);
            *//*frameLayout_home_page.setTranslationX(300 * (-1));*//*
            getActivity().findViewById(R.id.container_home_page).setTranslationX(350 * (-1));
          //  getActivity().findViewById(R.id.lin_layout_menu).setTranslationX(350);

        }*/
        } catch (Exception e) {

        }
    }

    /*@Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {

 /*       int connection_type = event.getState().getValue();

        if (connection_type == ConnectivityState.CONNECTED) {
            // device has active internet connection
            Toast.makeText(this, "You are now connected", Toast.LENGTH_SHORT).show();
        } else {
            // there is no active internet connection on this device
            InternetIsuueDialogClass.display_dialog(this).show();
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
        }*/

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_cart:
                if (findViewById(R.id.fragment_cart) == null) {
                    fm.beginTransaction().replace(R.id.frame_home, new FragmentCart())
                            .addToBackStack("cart").commit();
                } else {

                }
                break;


            case R.id.img_logo:
                if (findViewById(R.id.fragment_main) == null) {
                    fm.beginTransaction().replace(R.id.frame_home, new FragmentHomePage()).commit();
                    img_search.setImageResource(R.drawable.advanced_search2);
                    img_library.setImageResource(R.drawable.my_library2);
                    img_account.setImageResource(R.drawable.my_profile2);
                } else {
                    // Toast.makeText(this, "hh logo", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.lin_account:
                if (findViewById(R.id.fragment_profile) == null) {
                    fm.beginTransaction().replace(R.id.frame_home, new FragmentProfile()).addToBackStack("account_home").commit();
                    BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library2, R.drawable.home2, R.drawable.my_profile);
                } else {
                    //  Toast.makeText(this, "hh profile", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.lin_main:

                if (findViewById(R.id.fragment_main) == null) {

                    fm.beginTransaction().replace(R.id.frame_home, new FragmentHomePage()).addToBackStack("main_home").commit();
                    BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library2, R.drawable.home, R.drawable.my_profile2);
                } else {
                    //  Toast.makeText(this, "hh main", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.lin_search:
                if (findViewById(R.id.fragment_search) == null) {
                    fm.beginTransaction().replace(R.id.frame_home, new FragmentSearch()).addToBackStack("search_home").commit();
                    BuutonsAction(R.drawable.advanced_search, R.drawable.my_library2, R.drawable.home2, R.drawable.my_profile2);
                } else {
                    //   Toast.makeText(this, "hh search", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.lin_library:
                if (findViewById(R.id.fragment_library) == null) {
                    fm.beginTransaction().replace(R.id.frame_home, new FragmentMyLibrary()).addToBackStack("library_home").commit();
                    BuutonsAction(R.drawable.advanced_search2, R.drawable.my_library, R.drawable.home2, R.drawable.my_profile2);
                } else {
                    // Toast.makeText(this, "hh library", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.img_menu:
                display_menu();
                break;

        }

    }

   /* @Override
    public void onBackPressed() {
        fm.beginTransaction().replace(R.id.frame_home,new FragmentHomePage()).commit();
        findViewById(R.id.home_footer).setVisibility(View.VISIBLE);
    }*/

    @Override
    public void onBackPressed() {

        //  Log.d("BackStack", "onBackPressed: " + getSupportFragmentManager().
        // getBackStackEntryCount());


        try {

            int stack_size = getSupportFragmentManager().getBackStackEntryCount();

            if (stack_size > 1) {

                //  stack_size = getSupportFragmentManager().getBackStackEntryCount();
                //  Log.d("BackStack", "onBackPressed: " + getSupportFragmentManager()
                // .getFragments().get(stack_size - 1));
                //  if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                //       AllDialog.display_dialog(this).show();
                // } else {
                //  FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();

                getSupportFragmentManager().popBackStack();

                //transaction.commit();
                //  }
            } else {
                AllDialog.display_dialog(this).show();
            }
        } catch (Exception e) {
            AllDialog.display_dialog(this).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
