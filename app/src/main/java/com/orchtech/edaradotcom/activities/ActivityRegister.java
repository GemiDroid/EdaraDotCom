package com.orchtech.edaradotcom.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.orchtech.edaradotcom.R;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;

/**
 * Created by ahmed.yousef on 12/28/2016.
 */

public class ActivityRegister extends AppCompatActivity implements ConnectivityChangeListener {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {
/*

        int connection_type = event.getState().getValue();

        if (connection_type == ConnectivityState.CONNECTED) {
            // device has active internet connection
            Toast.makeText(this, "You are now connected", Toast.LENGTH_SHORT).show();
        } else {
            // there is no active internet connection on this device
            InternetIsuueDialogClass.display_dialog(this).show();
            Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
        }
*/

    }

}
