package com.orchtech.edaradotcom.webservices;

import com.orchtech.edaradotcom.models.AboutUs.AboutUsResponse;
import com.orchtech.edaradotcom.models.Articles.ArticlesModel;
import com.orchtech.edaradotcom.models.Articles.ArticlesResponse;
import com.orchtech.edaradotcom.models.ContactUs.ContactUs;
import com.orchtech.edaradotcom.models.CountriesRegisterModel;
import com.orchtech.edaradotcom.models.DailyQuotesModel;
import com.orchtech.edaradotcom.models.KhoulastDialogResponse;
import com.orchtech.edaradotcom.models.NotificationModel;
import com.orchtech.edaradotcom.models.Payment.BankTransferModel;
import com.orchtech.edaradotcom.models.PrivacyModel;
import com.orchtech.edaradotcom.models.Products.MySubscriptionResponse;
import com.orchtech.edaradotcom.models.Products.ProductsResponse;
import com.orchtech.edaradotcom.models.Products.SearchResponse;
import com.orchtech.edaradotcom.models.Profile.ProfileModel;
import com.orchtech.edaradotcom.models.Subscription.MainSubModel;
import com.orchtech.edaradotcom.models.Subscription.MoassyModel;
import com.orchtech.edaradotcom.models.TrainingDetailsResponse;
import com.orchtech.edaradotcom.models.TrainingListResponse;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.models.cart.ShoppingCartResponseModel;
import com.orchtech.edaradotcom.models.questions.QuestionDataResponse;
import com.orchtech.edaradotcom.models.questions.QuestionDetailsResponse;
import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;

import java.sql.Time;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by ahmed.yousef on 1/4/2017.
 */

public interface APIs {

    /*                              # API for fetching Newest Products                            */
    @GET("product/GetRecentProducts")
    Call<ProductsResponse> get_newest_products(
            @Query("count") int count,
            @Query("productTyeId") int productTyeId,
            @Query("sessionId") String sessionId);

    /*                              # API for fetching Free Products                              */
    @GET("product/GetFreeProducts")
    Call<ProductsResponse> get_free_products(
            @Query("sessionId") String sessionId,
            @Query("productTyeId") int productTyeId);

    /*                               # API Product Details                                         */
    @GET("product/GetProductDetails")
    Call<ProductsResponse> get_product_details(
            @Query("itemId") String ItemId,
            @Query("sessionId") String Session);


    /*                               # API Khoulasat( Most Visited Edition )                      */
    @GET("product/GetMostVisitedProducts")
    Call<ProductsResponse> getMostVisited(
            @Query("productTyeId") int ProductTypeId,
            @Query("limit") int Limit,
            @Query("sessionId") String SessionId);

    /*                               # API Khoulasat Info Dialog                                  */
    @GET("product/GetProductTypeById")
    Call<KhoulastDialogResponse> getInfoDialog(
            @Query("Id") int Id);


    /*                               # API Khoulasat ( Recent Books )                             */
    @GET("product/GetRecentProducts")
    Call<ProductsResponse> getRecentBooks
    (@Query("count") int Count,
     @Query("productTyeId") int ProductTypeId,
     @Query("sessionId") String SessionId);

    /*                                # API Khoulasat ( Most Sales )                              */
    @GET("product/GetTopsalesProducts")
    Call<ProductsResponse> getMostSales(
            @Query("productTyeId") int ProductTypeId,
            @Query("count") int Count,
            @Query("sessionId") String SessionId);

    /*                                # API Khoulasat ( Year Edition )                            */
    @GET("product/GetProductsList")
    Call<ProductsResponse> getYearEdition(
            @Query("offset") int Offset,
            @Query("productTypeId") int ProductTypeId,
            @Query("limit") int Limit,
            @Query("sessionId") String SessionId,
            @Query("issuedYear") int issuedYear);

     /*                               # Training APIs                                             */

    /*                                1-  API Training List                                         */
    @GET("training/FindTrariningPlans")
    Call<TrainingListResponse> getTrainingList(
            @Query("year") String Year,
            @Query("month") String Month,
            @Query("cityid") String CityId,
            @Query("planId") String PlanId);

    /*                                2-  API Training Details                                    */
    @GET("Training/GetTrainingDetails")
    Call<TrainingDetailsResponse> getTrainingDetails(
            @Query("planId") String PlanId);

    /*                                3- API Training Register                                    */
    @GET("Training/RequestTrainingRegistration")
    Call<ValidationModel.Success> getTrainingRegistration(
            @Query("planId") String PlanId,
            @Query("fullName") String FullName,
            @Query("email") String Email,
            @Query("countryId") String CountryId,
            @Query("phoneNum") String Phone,
            @Query("traineesCount") String Trainees,
            @Query("trainingManagerName") String MgrName,
            @Query("trainingManagerPhone") String MgrPhone);

    /*                                 4 API Training Inquires                                    */
    @GET("Training/RequestTrainingInquiry")
    Call<ValidationModel.Success> getTrainingInquiry(
            @Query("planId") String PlanId,
            @Query("fullName") String FullName,
            @Query("email") String Email,
            @Query("message") String Message,
            @Query("phoneNum") String Phone);

    /*                              # API for Login                                               */
    @GET("account/login")
    Call<ValidationModel> get_login(
            @Query("email") String Email,
            @Query("password") String Password);

    /*                              # API for Forgot Password                                     */
    @GET("account/forgetpassword")
    Call<ValidationModel> get_password(
            @Query("email") String email);

    /*                              # API for Register                                            */
    @POST("account/register")
    Call<ValidationModel> get_register(
            @Query("email") String Email,
            @Query("phone") String Phone,
            @Query("name") String UserName,
            @Query("country") String Country);

    /*                              # API for Countries Relate to Register                        */
    @GET("Customer/GetAllCountries")
    Call<List<CountriesRegisterModel>> getAllCountries();

    /*                              # API for Profile Data                                        */
    @GET("account/MyProfile")
    Call<ProfileModel> getProfileData(
            @Query("sessionId") String SessionId);

    /*                              # API for Update Profile                                      */
    @GET("account/UpdateProfileData")
    Call<ValidationModel> getUpdateProfile(
            @Query("sessionId") String SessionId,
            @Query("fullName") String FullName,
            @Query("Email") String Email,
            @Query("OldPassword") String OldPassword,
            @Query("NewPassword") String NewPassword);


    /*                              # API for Upload Profile Image                                */

    @GET("account/UploadImage")
    Call<ValidationModel.Success> UploadImage(
            @Query("sessionId") String SessionId,
            @Query("image") String Image,
            @Query("image_name") String ImageName);

    /*                              # Search APIs                                                 */

    /*                              1- API for Search                                             */
    @GET("product/Search")
    Call<ProductsResponse> Search(
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("sessionId") String sessionId,
            @Query("searchText") String searchText,
            @Query("searchTitle") String searchTitle,
            @Query("yearFrom") String yearFrom,
            @Query("yearTo") String yearTo,
            @Query("subject") String subject,
            @Query("producttype") String producttype,
            @Query("authors") String authors)
    ;

    /*                              2- API Getting  Authors                                       */
    @GET("product/GetAuthors")
    Call<SearchResponse> getAllAuthors();

    /*                              3- API Getting  Products                                      */
    @GET("product/GetProductTypes")
    Call<SearchResponse> getAllProducts();

    /*                              4- API Getting  Subjects                                      */
    @GET("customer/GetSubjectsList")
    Call<SearchResponse> getAllSubjects();

   /* *//*                                         # MyLibrary API                              *//*
    @GET("")
    Call<> getMyLibrary();

*/
     /*                                        # Articles APIs                                    */

    /*                              1- API for Get Articles List                                  */
    @GET("Article/GetArticlesList")
    Call<ArticlesResponse> getArticlesList(
            @Query("offset") String offset,
            @Query("limit") int limit,
            @Query("year") String Year,
            @Query("author") String Author);

    /*                              2- API to get Article Details                                 */
    @GET("Article/GetArticle")
    Call<ArticlesModel.ArticleDetails> getArticleDetails(
            @Query("article_Id") int article_id);


    /*                                       # Consulting APIs                                    */

    /*                                  1- API for Getting Consultants Details                    */
    @GET("Question/GetQuestionDetails")
    Call<QuestionDetailsResponse> getConsultants(
            @Query("question_Id") int Question_Id);

    /*                              2- API for Getting Consultants IDs                            */
    @GET("Question/GetQuestionsList")
    Call<QuestionsListResponse> getConsultantsIDs(
            @Query("offset") int offset);


    @GET("Question/GetQuestionsList")
    Call<QuestionsListResponse> getConsultant(
            @Query("offset") int offset,
            @Query("limit") int limit,
            @Query("GetMyConsultationOnly") boolean MyConsultant,
            @Query("searchtxt") String SearchText,
            @Query("sessionId") String SessionId);


    /*                              3- API ask 4 consulting                                       */
    @GET("Question/SaveConsultation")
    Call<QuestionDataResponse> setConsulting(
            @Query("sessionID") String SessionID,
            @Query("title") String title,
            @Query("text") String text,
            @Query("displayname") boolean diaplay_name);

    /*                              # API Payments                                                */
    /*                               1- Bank Transfer                                             */
    @GET("general/GetBankTransferPage")
    Call<BankTransferModel> getBankTransfer();

    /*                               2- Western Union                                             */
    @GET("general/GetWesternUnionPage")
    Call<BankTransferModel> getWesternUnion();


    /*                              # API About US                                                */
    @GET("general/GetAboutUsPage")
    Call<AboutUsResponse> getAboutUs();

    /*                              # API Privacy                                                 */
    @GET("general/GetPrivacyPolicyPage")
    Call<List<PrivacyModel>> getPrivacy(
            @Query("isArabic") boolean IsArabic);

    /*                              # API Tell A friend                                           */
    @GET("customer/TellAfriendAboutSite")
    Call<ValidationModel.Success> getTellAfriend(
            @Query("friendEmail") String FriendName,
            @Query("name") String FriendEmail,
            @Query("friendname") String YourName,
            @Query("messsage") String FriendMessage);

    /*                              # API Contact US                                              */
    @GET("general/ContactUsPageList")
    Call<ContactUs> getContactUS();

    @GET("Customer/ContactForm")
    Call<ValidationModel> setComplaints
            (@Query("name") String Name,
             @Query("email") String Email,
             @Query("text") String MessageBody,
             @Query("isComplain") boolean IsComplain,
             @Query("isSuggestion") boolean IsSuggestion,
             @Query("phone") String Phone);

    /*                              # API Daily Quotes                                            */
    @GET("product/GetRandomQuote")
    Call<DailyQuotesModel> getDailyQuotes(
            @Query("isArabic") boolean IsArabic);


    /*                             # API Notifications                                            */
    @GET("product/GetAllNotifications")
    Call<List<NotificationModel>> getNotifications();

    /*                             # API Complaint & Suggestions                                  */


    /*                              # API Getting Your Subscription                                */
    @GET("product/GetMySubscriptions")
    Call<MySubscriptionResponse> getMySubscription(
            @Query("sessionId") String SessionId);


    /*                             # API Subscriptions                                            */

    /*                             1-1 Moassy Subscription                                        */
    @GET("product/GetCorporateSubscription")
    Call<MoassyModel> getMoassySub();

    /*                             1-2 Request Moassy Order                                       */
    @GET("product/GetOfferPriceRequest")
    Call<ValidationModel.Success> getRequestMoassy(
            @Query("fullName") String FullName,
            @Query("email") String Email, @Query("company") String Company,
            @Query("country") String Country, @Query("notes") String Notes,
            @Query("phoneNum") String PhoneNumber);

    /*                               2-1 Managers Libraries                                        */
    @GET("customer/GetLibrariesList")
    Call<SearchResponse> getManagersLibraries();

    /*                               2-2 Manager Details Libraries                                */
   /* @GET("product/GetProductsByLibrary")
    Call<> getManagerDetails(@Query("libraryId") String LibraryId);
*/

    /*                                3- Main Subscription                                        */
    @GET("Subscription/GetSubscriptionTypes")
    Call<MainSubModel> getMainSubscription();


    /*                              # API  shopping Cart                                          */

    @GET("payment/GetShoppingCartData")
    Call<ShoppingCartResponseModel> GetShoppingCart(
            @Query("sessionID") String SessionId);

    /*                               1- Add Product to Invoice                                    */
    @GET("payment/AddProductToInvoice")
    Call<ValidationModel.Success> AddProductToInvoice(
            @Query("sessionID") String SessionId,
            @Query("productID") String ProductId);

    /*                               2- Add Library to Invoice                                    */
    @GET("payment/AddLibraryToInvoice")
    Call<ValidationModel.Success> AddLibraryToInvoice(
            @Query("sessionID") String SessionId,
            @Query("libraryId") String LibraryId);

    /*                               3- Add Subscription to Invoice                               */
    @GET("payment/AddSubscriptionToInvoice")
    Call<ValidationModel.Success> AddSubscriptionToInvoice(
            @Query("sessionID") String SessionId,
            @Query("subscriptiontype") String SubscriptionType);

    /*                                 4- Remove Product                                          */
    @GET("payment/DeleteInvoiceDetail")
    Call<ValidationModel.Success> DeleteInvoice(
            @Query("sessionID") String SessionId,
            @Query("invoiceDetailId") String InvoiceId);

    /*                                 5- Remove All Products                                     */
    @GET("payment/DeleteInvoice")
    Call<ValidationModel.Success> DeleteAllInvoices(
            @Query("sessionID") String SessionId);

    /*                               # APIs Settings                                              */

    /*                                1- Settings                                                 */
    @GET("Customer/SavePreferences")
    Call<ValidationModel> getSaveSettings(
            @Query("dailyQuoteTime") Time QuoteTime,
            @Query("isArabicLanguge") boolean isArabicLanguage,
            @Query("interestedSubjects") String Subjects,
            @Query("appearDailyQuote") boolean QuoteAppearnce,
            @Query("isArabicDailyQuoteLanguage") boolean isArabicQuotes,
            @Query("fontSize") int Font,
            @Query("sessionId") String SessionId,
            @Query("knowWhatIsNew") boolean isWhatNew,
            @Query("knowKhulasatNames") boolean isKhoulsatName);

    /*                                 2- Favourites                                              */
    @GET("customer/GetInterestedSubjectsByCustomerID")
    Call<SearchResponse> getFavourites(
            @Query("sessionId") String SessionId);



    /* -------------------------------------------------------------------------------------------*/

}



