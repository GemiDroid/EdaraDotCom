package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class DailyQuotesModel implements Serializable {

    /*
"id": 160,
"title": "لقد كان الإمداد هو الأساس الذي تقوم عليه إستراتيجيات الإسكندر وتكتيكاته",
"authorName": "دونالد انجلز",
"imageurl": "https://edara.com/images/logo.png"*/

    @SerializedName("id")
    @Expose
    private int Id;

    @SerializedName("title")
    @Expose
    private String Title;


    @SerializedName("authorName")
    @Expose
    private String Author;

    @SerializedName("imageurl")
    @Expose
    private String AuthorImage;


    public int getId() {
        return Id;
    }

    public String getTitle() {
        return Title;
    }

    public String getAuthor() {
        return Author;
    }

    public String getAuthorImage() {
        return AuthorImage;
    }
}
