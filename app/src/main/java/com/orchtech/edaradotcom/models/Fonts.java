package com.orchtech.edaradotcom.models;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by ahmed.yousef on 12/29/2016.
 */

public class Fonts {
    /*
    Here just put your font name only without any extension ..
     */
    public static Typeface get_fonts(Context context,String fileName){

        return Typeface.createFromAsset(context.getAssets(), "fonts/" + fileName + ".ttf");
    }
}
