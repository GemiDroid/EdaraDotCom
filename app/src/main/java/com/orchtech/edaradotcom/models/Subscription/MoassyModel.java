package com.orchtech.edaradotcom.models.Subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/14/2017.
 */

public class MoassyModel implements Serializable {

    @SerializedName("introduction")
    @Expose
    private String Intro;

    @SerializedName("title")
    @Expose
    private String Title;

    @SerializedName("bullets")
    @Expose
    private List<String> bulletsList;


    public String getIntro() {
        return Intro;
    }

    public String getTitle() {
        return Title;
    }

    public List<String> getBulletsList() {
        return bulletsList;
    }



    /*public class Bullets implements Serializable {

        private String Bullets;

        public String getBullets() {
            return Bullets;
        }
    }*/
}
