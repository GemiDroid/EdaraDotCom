package com.orchtech.edaradotcom.models.Subscription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class MainSubModel implements Serializable {


    @SerializedName("subscriptiontypeslist")
    @Expose
    private List<SubTypesList> subTypesListList;

    public List<SubTypesList> getSubTypesListList() {
        return subTypesListList;
    }

    public class SubTypesList implements Serializable {

        @SerializedName("basic_subscriptions")
        @Expose
        private List<BasicSub> basicSubList;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("price")
        @Expose
        private String price;


        @SerializedName("id")
        @Expose
        private String SubTypeId;

        public String getSubTypeId() {
            return SubTypeId;
        }

        public List<BasicSub> getBasicSubList() {
            return basicSubList;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getPrice() {
            return price;
        }

    }

    public class BasicSub implements Serializable {


        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("price")
        @Expose
        private String Price;

        @SerializedName("Is_Interest")
        @Expose
        private boolean IsInterest;

        public boolean getIsInterest() {
            return IsInterest;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getPrice() {
            return Price;
        }

    }
}
