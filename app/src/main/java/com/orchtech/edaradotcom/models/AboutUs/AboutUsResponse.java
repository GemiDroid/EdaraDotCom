package com.orchtech.edaradotcom.models.AboutUs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */
public class AboutUsResponse {


    @SerializedName("messageBody")
    @Expose
    private String MsgBody;

    @SerializedName("commitmentHeader")
    @Expose
    private String CommitmentHearder;

    @SerializedName("commitmentBody")
    @Expose
    private List<AboutUsBullet> getBulleted;

    public List<AboutUsBullet> getGetBulleted() {
        return getBulleted;
    }

    public String getMsgBody() {
        return MsgBody;
    }

    public String getCommitmentHearder() {
        return CommitmentHearder;
    }

    public class AboutUsBullet implements Serializable {

        @SerializedName("satictext")
        @Expose
        private String bullet_text;

        public String getBullet_text() {
            return bullet_text;
        }
    }
}
