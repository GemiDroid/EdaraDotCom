package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class NotificationModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String Id;

    @SerializedName("title_ar")
    @Expose
    private String Title_Ar;

    @SerializedName("date")
    @Expose
    private String Date;

    @SerializedName("description")
    @Expose
    private String Description;

    public String getId() {
        return Id;
    }

    public String getTitle_Ar() {
        return Title_Ar;
    }

    public String getDate() {
        return Date;
    }

    public String getDescription() {
        return Description;
    }
}
