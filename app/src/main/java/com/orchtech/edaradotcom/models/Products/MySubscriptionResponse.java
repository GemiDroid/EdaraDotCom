package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/11/2017.
 */

public class MySubscriptionResponse implements Serializable {

    @SerializedName("subscriptions")
    @Expose
    private List<MySubscriptionModel> mySubscriptionModelList;

    public List<MySubscriptionModel> getMySubscriptionModelList() {
        return mySubscriptionModelList;
    }

    public class MySubscriptionModel implements Serializable {


        @SerializedName("Id")
        @Expose
        private String ID;

        @SerializedName("dateFrom")
        @Expose
        private String DateFrom;

        @SerializedName("dateTo")
        @Expose
        private String DateTo;

        @SerializedName("name")
        @Expose
        private String Name;

        @SerializedName("is_electroniconly")
        @Expose
        private boolean IsElectronic;

        @SerializedName("no_of_copies")
        @Expose
        private String Copies;


        public String getID() {
            return ID;
        }

        public String getDateFrom() {
            return DateFrom;
        }

        public String getDateTo() {
            return DateTo;
        }

        public String getName() {
            return Name;
        }

        public boolean isElectronic() {
            return IsElectronic;
        }

        public String getCopies() {
            return Copies;
        }
    }
}
