package com.orchtech.edaradotcom.models.Profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class ProfileModel {


    @SerializedName("Id")
    @Expose
    private String ID;

    @SerializedName("firstName")
    @Expose
    private String FirstName;

    @SerializedName("middleName")
    @Expose
    private String MiddleName;

    @SerializedName("lastName")
    @Expose
    private String LastName;

    @SerializedName("password")
    @Expose
    private String Password;
    @SerializedName("image")
    @Expose
    private String image;

    public String getImage() {
        return image;
    }

    public String getID() {
        return ID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getPassword() {
        return Password;
    }
}
