package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 5/9/2017.
 */

public class KhoulastDialogResponse implements Serializable {


    @SerializedName("producttype")
    @Expose
    private KhoulasatManager khoulasatManager;

    public KhoulasatManager getKhoulasatManager() {
        return khoulasatManager;
    }

    public class KhoulasatManager implements Serializable {


        @SerializedName("id")
        @Expose
        private int Id;

        @SerializedName("name")
        @Expose
        private String Name;


        @SerializedName("about")
        @Expose
        private String About;

        @SerializedName("description")
        @Expose
        private String Description;

        // Getters ...............

        public int getId() {
            return Id;
        }

        public String getName() {
            return Name;
        }

        public String getAbout() {
            return About;
        }

        public String getDescription() {
            return Description;
        }
    }
}
