package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class PrivacyModel implements Serializable {

    @SerializedName("header")
    @Expose
    private String Header;

    @SerializedName("bullets")
    @Expose
    private List<String> Body;

    @SerializedName("recoveryPolicyBody")
    @Expose
    private String Instruction;


    public String getHeader() {
        return Header;
    }

    public List<String> getBody() {
        return Body;
    }

    public String getInstruction() {
        return Instruction;
    }
}
