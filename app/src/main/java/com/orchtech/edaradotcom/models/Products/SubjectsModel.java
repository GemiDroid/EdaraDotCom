package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.yousef on 4/12/2017.
 */

public class SubjectsModel {


    @SerializedName("id")
    @Expose
    private String ID;

    @SerializedName("text_ar")
    @Expose
    private String Text_Ar;

    @SerializedName("text_en")
    @Expose
    private String Text_En;

    @SerializedName("description")
    @Expose
    private String Description;

    @SerializedName("price")
    @Expose
    private String Price;

    @SerializedName("Is_Interest")
    @Expose
    private boolean Is_Interest;

    public String getID() {
        return ID;
    }

    public String getText_Ar() {
        return Text_Ar;
    }

    public String getText_En() {
        return Text_En;
    }

    public String getDescription() {
        return Description;
    }

    public String getPrice() {
        return Price;
    }

    public boolean isIs_Interest() {
        return Is_Interest;
    }

    public void setIs_Interest(boolean is_Interest) {
        Is_Interest = is_Interest;
    }
}
