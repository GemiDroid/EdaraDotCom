package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 3/30/2017.
 */

public class FiltersModel implements Serializable {


    @SerializedName("Data")
    @Expose
    private String Data;

    @SerializedName("Id")
    @Expose
    private int Id;

    public String getData() {
        return Data;
    }

    public int getId() {
        return Id;
    }
}
