package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class ProductsResponse implements Serializable {

    @SerializedName("items")
    @Expose
    List<ProductsModel> items;
    @SerializedName("item")
    @Expose
    ProductsModel itemsObject;

    public List<ProductsModel> getItems() {
        return items;
    }

    public ProductsModel getItemsObject() {
        return itemsObject;
    }
}

