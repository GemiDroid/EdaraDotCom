package com.orchtech.edaradotcom.models;

/**
 * Created by ahmed.yousef on 1/11/2017.
 */

public class ModelMenu {

    private String menu_txt;
    private int menu_img;


    public ModelMenu(String menu_txt, int menu_img) {
        this.menu_txt = menu_txt;
        this.menu_img = menu_img;
    }

    public ModelMenu(String menu_txt) {
        this.menu_txt = menu_txt;

    }

    public String getMenu_txt() {
        return menu_txt;
    }

    public void setMenu_txt(String menu_txt) {
        this.menu_txt = menu_txt;
    }

    public int getMenu_img() {
        return menu_img;
    }

    public void setMenu_img(int menu_img) {
        this.menu_img = menu_img;
    }
}
