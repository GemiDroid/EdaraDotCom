package com.orchtech.edaradotcom.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class ShoppingCartResponseModel implements Serializable {


    @SerializedName("shoppingcartdata")
    @Expose
    private List<ShoppingCartModel> shoppingCartModelList;

    public List<ShoppingCartModel> getShoppingCartModelList() {
        return shoppingCartModelList;
    }
}
