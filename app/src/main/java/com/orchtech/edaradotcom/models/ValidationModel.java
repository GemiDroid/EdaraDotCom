package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 2/8/2017.
 */

public class ValidationModel implements Serializable {


    @SerializedName("success")
    @Expose
    private Success success;


    @SerializedName("access")
    @Expose
    private Access access;

    public Success getSuccess() {
        return success;
    }

    public Access getAccess() {
        return access;
    }


    public class Success {
        @SerializedName("success")
        @Expose
        private boolean success;

        @SerializedName("message")
        @Expose
        private String message;

        public boolean isSuccess() {
            return success;
        }

        public String getMessage() {
            return message;
        }

    }


    public class Access {

        @SerializedName("session_id")
        @Expose
        private String session_id;

        @SerializedName("username")
        @Expose
        private String UserName;


        public String getUserName() {
            return UserName;
        }

        public String getSession_id() {
            return session_id;
        }

    }


}
