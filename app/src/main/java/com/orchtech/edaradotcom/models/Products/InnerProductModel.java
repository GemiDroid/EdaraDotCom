package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 5/3/2017.
 */

public class InnerProductModel implements Serializable {


    @SerializedName("id")
    @Expose
    private String Id;

    @SerializedName("text")
    @Expose
    private String Text;

    public String getId() {
        return Id;
    }

    public String getText() {
        return Text;
    }
}
