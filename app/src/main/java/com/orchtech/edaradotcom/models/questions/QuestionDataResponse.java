package com.orchtech.edaradotcom.models.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 3/16/2017.
 */

public class QuestionDataResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private Question QuestionResponse;

    public Question getResponse() {
        return QuestionResponse;
    }


    public class Question {

        @SerializedName("message")
        @Expose
        private String Message;
        @SerializedName("success")
        @Expose
        private boolean Sucess;

        public String getMessage() {
            return Message;
        }

        public boolean isSucess() {
            return Sucess;
        }

    }
}
