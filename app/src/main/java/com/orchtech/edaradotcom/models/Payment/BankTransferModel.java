package com.orchtech.edaradotcom.models.Payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class BankTransferModel implements Serializable {

    // Bank Transfer Fields ...
    @SerializedName("logoUrl")
    @Expose
    private String Logo;

    @SerializedName("header")
    @Expose
    private String Header;

    @SerializedName("firstBankName")
    @Expose
    private String FirstBankName;

    @SerializedName("firstBankImageUrl")
    @Expose
    private String FirstBankLogo;

    @SerializedName("firstIban")
    @Expose
    private String FirstIban;

    @SerializedName("secondBankName")
    @Expose
    private String SecondBankName;

    @SerializedName("secondBankImageUrl")
    @Expose
    private String SecondBankLogo;

    @SerializedName("secondIban")
    @Expose
    private String SecondBankIban;

    //   Western Union Fields ..
    @SerializedName("firstPoint")
    @Expose
    private String FirstPoint;

    @SerializedName("secondPoint")
    @Expose
    private String SecondPoint;

    @SerializedName("thirdPointHeader")
    @Expose
    private String ThirdPoint;

    @SerializedName("thirdPointBullets")
    @Expose
    private List<String> PointBullets;

    @SerializedName("fourthPoint")
    @Expose
    private String FourthPoint;

    @SerializedName("footer")
    @Expose
    private String Footer;


    public String getLogo() {
        return Logo;
    }

    public String getHeader() {
        return Header;
    }

    public String getFirstBankName() {
        return FirstBankName;
    }

    public String getFirstBankLogo() {
        return FirstBankLogo;
    }

    public String getFirstIban() {
        return FirstIban;
    }

    public String getSecondBankName() {
        return SecondBankName;
    }

    public String getSecondBankLogo() {
        return SecondBankLogo;
    }

    public String getSecondBankIban() {
        return SecondBankIban;
    }

    public String getFirstPoint() {
        return FirstPoint;
    }

    public String getSecondPoint() {
        return SecondPoint;
    }

    public String getThirdPoint() {
        return ThirdPoint;
    }

    public List<String> getPointBullets() {
        return PointBullets;
    }

    public String getFourthPoint() {
        return FourthPoint;
    }

    public String getFooter() {
        return Footer;
    }
}
