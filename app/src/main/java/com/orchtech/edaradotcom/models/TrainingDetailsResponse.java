package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class TrainingDetailsResponse implements Serializable {


    @SerializedName("trainingDetails")
    @Expose
    private TrainingDetailsModel trainingDetailsModel;


    public TrainingDetailsModel getTrainingDetailsModel() {
        return trainingDetailsModel;
    }


    public class TrainingDetailsModel {


        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("introduction")
        @Expose
        private String Intro;

        @SerializedName("goals")
        @Expose
        private String Goals;


        @SerializedName("programContent")
        @Expose
        private String Contents;

        @SerializedName("techniques")
        @Expose
        private String Techniques;


        // Getters ......


        public String getId() {
            return id;
        }

        public String getIntro() {
            return Intro;
        }

        public String getGoals() {
            return Goals;
        }

        public String getContents() {
            return Contents;
        }

        public String getTechniques() {
            return Techniques;
        }
    }
}
