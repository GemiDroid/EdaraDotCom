package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class SearchResponse implements Serializable {

    @SerializedName("authorsList")
    @Expose
    private List<AuthorModel> AuthorList;

    @SerializedName("subjectslist")
    @Expose
    private ArrayList<SubjectsModel> SubjectsList;

    @SerializedName("producttypeslist")
    @Expose
    private List<ProductTypessModel> ProductTypessList;


    public ArrayList<SubjectsModel> getSubjectsList() {
        return SubjectsList;
    }

    public List<ProductTypessModel> getProductTypessList() {
        return ProductTypessList;
    }

    public List<AuthorModel> getAuthorList() {
        return AuthorList;
    }
}
