package com.orchtech.edaradotcom.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class TrainingListResponse implements Serializable {


    @SerializedName("traininglist")
    @Expose
    private List<TrainingListModel> trainingListManager;

    public List<TrainingListModel> getTrainingListManager() {
        return trainingListManager;
    }


    public class TrainingListModel implements Serializable {

        @SerializedName("id")
        @Expose
        private int Id;


        @SerializedName("name")
        @Expose
        private String Name;

        @SerializedName("from")
        @Expose
        private String From;

        @SerializedName("to")
        @Expose
        private String To;

        @SerializedName("place")
        @Expose
        private String Place;

        // Getters ......


        public int getId() {
            return Id;
        }

        public String getName() {
            return Name;
        }

        public String getFrom() {
            return From;
        }

        public String getTo() {
            return To;
        }

        public String getPlace() {
            return Place;
        }
    }
}
