package com.orchtech.edaradotcom.models.Articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class ArticlesResponse {


    @SerializedName("articles")
    @Expose
    private List<ArticlesModel> articles;

    public List<ArticlesModel> getArticles() {
        return articles;
    }
}
