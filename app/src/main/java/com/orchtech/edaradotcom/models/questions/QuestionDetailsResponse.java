package com.orchtech.edaradotcom.models.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by ahmed.yousef on 3/13/2017.
 */
public class QuestionDetailsResponse implements Serializable {


    /*"question_id": 4162,
"answer": "السلام عليكم يجب أن نرى السيرة الذاتية أولا وقبل المقابلة ..أرجو إرسالها إلى:gulf@edara.com وjordan2@edara.com بالسرعة الممكنة ...بالتوفيقمع تحياتإدارة الاستشارات والتوجيهCoachingبقيادة : نسيم الصماديwww.edara.com",
"answered_by": "نسيم الصمادي",
"title": "باحث عن عمل",
"body": "هل يوجد لديكم وظائف ادارية ؟\r\n",
"date": "02/11/2016"*/
    /* --------------------------- Model for Question Details -------------------------------------*/

    @SerializedName("question")
    @Expose
    private QuestionDetails question_details;

    public QuestionDetails getQuestionDetails() {
        return question_details;
    }

    public class QuestionDetails implements Serializable {

        @SerializedName("question_id")
        @Expose
        private String question_id;

        @SerializedName("answer")
        @Expose
        private String answer;

        @SerializedName("answered_by")
        @Expose
        private String answered_by;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("body")
        @Expose
        private String body;

        @SerializedName("date")
        @Expose
        private String date;


        public String getQuestion_id() {
            return question_id;
        }

        public String getAnswer() {
            return answer;
        }

        public String getAnswered_by() {
            return answered_by;
        }

        public String getTitle() {
            return title;
        }

        public String getBody() {
            return body;
        }

        public String getDate() {
            return date;
        }

    }

    /* --------------------------- Model for Questions List -------------------------------------*/


}
