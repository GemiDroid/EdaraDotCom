package com.orchtech.edaradotcom.models.Articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 1/8/2017.
 */

public class ArticlesModel implements Serializable {

    @SerializedName("article_id")
    @Expose
    private int article_id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("author")
    @Expose
    private String author;

    @SerializedName("body")
    @Expose
    private String Body;

    @SerializedName("date")
    @Expose
    private String Date;


    public String getBody() {
        return Body;
    }

    public String getDate() {
        return Date;
    }

    public int getArticle_id() {
        return article_id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }


    public class ArticleDetails {


        @SerializedName("article")
        @Expose
        private ArticlesModel DetailsArticle;

        public ArticlesModel getDetailsArticle() {
            return DetailsArticle;
        }
    }



}
