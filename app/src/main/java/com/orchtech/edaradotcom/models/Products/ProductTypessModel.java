package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmed.yousef on 4/12/2017.
 */

public class ProductTypessModel {

    @SerializedName("id")
    @Expose
    private String ID;

    @SerializedName("name")
    @Expose
    private String Name;

    @SerializedName("language")
    @Expose
    private String Language;

    @SerializedName("about")
    @Expose
    private String About;

    @SerializedName("image_url")
    @Expose
    private String ImageUrl;

    @SerializedName("description")
    @Expose
    private String Description;


    public String getID() {
        return ID;
    }

    public String getName() {
        return Name;
    }

    public String getLanguage() {
        return Language;
    }

    public String getAbout() {
        return About;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getDescription() {
        return Description;
    }
}
