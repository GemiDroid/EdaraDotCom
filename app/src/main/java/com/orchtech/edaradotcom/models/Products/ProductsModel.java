package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 1/4/2017.
 */

public class ProductsModel implements Serializable {


    //------------------------------------ 14 Related Productes ----------------------------------//
    @SerializedName("relatedproducts")
    @Expose
    private List<ProductsModel> related_products;
    //------------------------------------ 0 Authors ---------------------------------------------//
    @SerializedName("authors")
    @Expose
    private List<AuthorModel> authors;
    //------------------------------------ 1 ID --------------------------------------------------//
    @SerializedName("id")
    @Expose
    private String id;
    //------------------------------------ 2 Title_Ar --------------------------------------------//
    @SerializedName("title_ar")
    @Expose
    private String title_ar;
    //------------------------------------ 3 Subtitle_Ar -----------------------------------------//
    @SerializedName("subtitle_ar")
    @Expose
    private String subtitle_ar;
    //------------------------------------ 4 Image_Url -------------------------------------------//
    @SerializedName("image_url")
    @Expose
    private String image_url;
    //------------------------------------ 5 Issued_Month ----------------------------------------//
    @SerializedName("issuedmonth")
    @Expose
    private String issuedmonth;
    //------------------------------------ 6 Issued_Year -----------------------------------------//
    @SerializedName("issuedyear")
    @Expose
    private String issuedyear;
    //------------------------------------ 7 ISBN ------------------------------------------------//
    @SerializedName("isbn")
    @Expose
    private String isbn;
    //------------------------------------ 8 Publish_Date ----------------------------------------//
    @SerializedName("publication_date")
    @Expose
    private String publication_date;
    //------------------------------------ 9 Download_Url ----------------------------------------//
    @SerializedName("download_url")
    @Expose
    private String download_url;
    //------------------------------------ 11 Rating_Bar -----------------------------------------//
    @SerializedName("rating")
    @Expose
    private String rate_ratio;
    //------------------------------------ 12 PDF Type -------------------------------------------//
    @SerializedName("isPDF")
    @Expose
    private boolean is_pdf;
    //------------------------------------ 13 Audio Type -----------------------------------------//
    @SerializedName("isAudio")
    @Expose
    private boolean is_audio;
    //------------------------------------ 15 Product Pages Count --------------------------------//
    @SerializedName("bookpagescount")
    @Expose
    private String bookPageCount;
    //------------------------------------ 16 Product PageNumber(For Search) ---------------------//
    @SerializedName("number")
    @Expose
    private String Number;
    //------------------------------------ 17 Product Access -------------------------------------//
    @SerializedName("access")
    @Expose
    private boolean isSubscribed;
    //------------------------------------- 18 Product Price -------------------------------------//
    @SerializedName("price")
    @Expose
    private String Price;
    //------------------------------------ 19 Product(Free/Paid)----------------------------------//
    @SerializedName("isPaid")
    @Expose
    private boolean isPaid;
    //------------------------------------- 20 Product Author ------------------------------------//
    @SerializedName("author")
    @Expose
    private String Author;
    //-------------------------------------- 21 Product Description ------------------------------//
    @SerializedName("description")
    @Expose
    private String Description;
    //--------------------------------------- 22 Product Inner Titles ----------------------------//
    @SerializedName("innertitles")
    @Expose
    private List<InnerProductModel> innerProductModels;


    //-------------------------------------- Getters  --------------------------------------------//


    public List<InnerProductModel> getInnerProductModels() {
        return innerProductModels;
    }

    public String getAuthor() {
        return Author;
    }

    public String getDescription() {
        return Description;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public String getPrice() {
        return Price;
    }

    public boolean isIs_pdf() {
        return is_pdf;
    }

    public boolean isIs_audio() {
        return is_audio;
    }

    public String getBookPageCount() {
        return bookPageCount;
    }

    public String getNumber() {
        return Number;
    }

    public List<ProductsModel> getRelated_products() {
        return related_products;
    }

    public boolean is_audio() {
        return is_audio;
    }

    public boolean is_pdf() {
        return is_pdf;
    }

    public List<AuthorModel> getAuthors() {
        return authors;
    }

    public String getId() {
        return id;
    }

    public String getTitle_ar() {
        return title_ar;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getSubtitle_ar() {
        return subtitle_ar;
    }

    public String getIssuedmonth() {
        return issuedmonth;
    }

    public String getIssuedyear() {
        return issuedyear;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPublication_date() {
        return publication_date;
    }

    public String getDownload_url() {
        return download_url;
    }

    public String getRate_ratio() {
        return rate_ratio;
    }


}



