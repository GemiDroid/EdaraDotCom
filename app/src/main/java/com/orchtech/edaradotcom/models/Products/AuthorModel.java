package com.orchtech.edaradotcom.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class AuthorModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name_ar")
    @Expose
    private String name_ar;

    @SerializedName("about_ar")
    @Expose
    private String about_ar;

    public String getId() {
        return id;
    }

    public String getName_ar() {
        return name_ar;
    }

    public String getAbout_ar() {
        return about_ar;
    }
}
