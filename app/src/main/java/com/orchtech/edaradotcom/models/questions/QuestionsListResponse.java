package com.orchtech.edaradotcom.models.questions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ahmed.yousef on 3/15/2017.
 */

public class QuestionsListResponse implements Serializable {

    @SerializedName("questions")
    @Expose
    private List<QuestionsList> question_list;

    public List<QuestionsList> getQuestionList() {
        return question_list;
    }

    public class QuestionsList implements Serializable {

        /* "question_id": 4181,
           "title": "اثر تنمية الموارد البشرية في تحسين بيئة العمل",
           "customer": "أبوعبيدة  جمال الدين",
           "date": "23/11/2016" */

        @SerializedName("question_id")
        @Expose
        private int question_id;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("customer")
        @Expose
        private String consumer;

        @SerializedName("date")
        @Expose
        private String date;


        public int getQuestion_id() {
            return question_id;
        }

        public String getTitle() {
            return title;
        }

        public String getConsumer() {
            return consumer;
        }

        public String getDate() {
            return date;
        }

    }

}
