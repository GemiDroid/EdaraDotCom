package com.orchtech.edaradotcom.models.ContactUs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */

public class ContactUs {


    @SerializedName("ContactUsPageConfiguration")
    @Expose
    private List<Contacts> Contacts;

    public List<Contacts> getContacts() {
        return Contacts;
    }


    public class Contacts {

        @SerializedName("ID")
        @Expose
        private int ID;

        @SerializedName("Country_Name")
        @Expose
        private String Country_Name;

        @SerializedName("Comany_Name")
        @Expose
        private String Company_Name;

        @SerializedName("Flag_Company")
        @Expose
        private String Flag_Company;

        @SerializedName("Address")
        @Expose
        private String Address;

        @SerializedName("Ph_Work")
        @Expose
        private String Ph_work;

        @SerializedName("Ph_Fax")
        @Expose
        private String Ph_fax;

        @SerializedName("Ph_Mobile")
        @Expose
        private String Ph_mobile;

        @SerializedName("Emails")
        @Expose
        private String Emails;

        @SerializedName("WhatsUp")
        @Expose
        private String WhatUp;


        public int getID() {
            return ID;
        }

        public String getCountry_Name() {
            return Country_Name;
        }

        public String getCompany_Name() {
            return Company_Name;
        }

        public String getFlag_Company() {
            return Flag_Company;
        }

        public String getAddress() {
            return Address;
        }

        public String getPh_work() {
            return Ph_work;
        }

        public String getPh_fax() {
            return Ph_fax;
        }

        public String getPh_mobile() {
            return Ph_mobile;
        }

        public String getEmails() {
            return Emails;
        }

        public String getWhatUp() {
            return WhatUp;
        }
    }

}
