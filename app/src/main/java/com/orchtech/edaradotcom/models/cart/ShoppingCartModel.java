package com.orchtech.edaradotcom.models.cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ahmed.yousef on 4/9/2017.
 */

public class ShoppingCartModel implements Serializable {


    @SerializedName("invoidedetailid")
    @Expose
    private String InvoiceDetailId;

    @SerializedName("itemid")
    @Expose
    private String ItemId;

    @SerializedName("numberofcopies")
    @Expose
    private String NoOfCopies;

    @SerializedName("itemname")
    @Expose
    private String ItemName;

    @SerializedName("price")
    @Expose
    private String Price;

    @SerializedName("finalprice")
    @Expose
    private String FinalPrice;


    public String getInvoiceDetailId() {
        return InvoiceDetailId;
    }

    public String getItemId() {
        return ItemId;
    }

    public String getNoOfCopies() {
        return NoOfCopies;
    }

    public String getItemName() {
        return ItemName;
    }

    public String getPrice() {
        return Price;
    }

    public String getFinalPrice() {
        return FinalPrice;
    }
}
