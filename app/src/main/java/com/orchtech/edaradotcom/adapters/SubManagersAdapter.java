package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.fragments.FragmentForgotPass;
import com.orchtech.edaradotcom.managers.ShoppingCart.AddLibraryManager;
import com.orchtech.edaradotcom.models.Products.SubjectsModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 4/16/2017.
 */

public class SubManagersAdapter extends RecyclerView.Adapter<SubManagersAdapter.SubManagerHolder> {

    static String SessionId;
    List<SubjectsModel> subjectsModelList;
    SubjectsModel subjectsModel;
    Context context;
    AddLibraryManager addLibraryManager;
    FragmentManager fragmentManager;

    public SubManagersAdapter(List<SubjectsModel> subjectsModels, Context context) {
        subjectsModelList = subjectsModels;
        this.context = context;
    }

    @Override
    public SubManagerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_sub_managers, parent, false);

        return new SubManagerHolder(v);
    }

    @Override
    public void onBindViewHolder(final SubManagerHolder holder, int position) {

        subjectsModel = subjectsModelList.get(position);

        holder.library_name.setText(subjectsModel.getText_Ar());
        holder.library_details.setText(subjectsModel.getDescription());

        holder.is_interest.setChecked(subjectsModel.isIs_Interest());
        if (!holder.is_interest.isChecked()) {
            holder.is_interest.setEnabled(false);
        } else {
        }

        final String LibId = subjectsModel.getID();

        holder.library_buy.setText(subjectsModel.getPrice() + "$ " + " شراء المكتبه ");


        holder.library_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                // Adding Library to Cart ......

                SessionId = Preferences.getFromPreference(context, "session_id");

                if (addLibraryManager == null) {
                    addLibraryManager = new AddLibraryManager();
                }

                addLibraryManager.add_library_to_cart(SessionId, LibId).enqueue(new Callback<ValidationModel.Success>() {
                    @Override
                    public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {
                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body() == null) {
                            } else {
                                if (response.body().isSuccess()) {
                                    FragmentForgotPass.showMsg("تم إضافة المكتبه بنجاح", "المكتبات", context).show();
                                } else {
                                    FragmentForgotPass.showMsg(response.body().getMessage(), "المكتبات", context).show();
                                }
                            }
                        } else {
                            context.startActivity(new Intent(context, BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidationModel.Success> call, Throwable t) {
                        Snackbar.make(v, context.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

                    }
                });

               /* fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                Fragment fragment_sub_managers = new FragmentManagers();
                Bundle bundle = new Bundle();
                bundle.putString("library_id", subjectsModel.getID());
                fragment_sub_managers.setArguments(bundle);

                Toast.makeText(context, subjectsModel.getID(), Toast.LENGTH_SHORT).show();
                // fragmentManager=(FragmentActivity)
                AllDialog.display_dialog(context, "تم إضافة المكتبة الى سلة مشترياتك").show();*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return subjectsModelList.size();
    }

    public interface GetId {

        public void getID();

    }

    public class SubManagerHolder extends RecyclerView.ViewHolder {


        TextView library_name, library_details;
        Button library_buy;
        Switch is_interest;
        View view_line;


        public SubManagerHolder(View itemView) {
            super(itemView);

            library_name = (TextView) itemView.findViewById(R.id.txt_title_sub_managers);
            library_details = (TextView) itemView.findViewById(R.id.txt_details_sub_managers);
            is_interest = (Switch) itemView.findViewById(R.id.switch_sub_managers);
            library_buy = (Button) itemView.findViewById(R.id.btn_buy_sub_managers);

            view_line = itemView.findViewById(R.id.view_line);
        }
    }
}
