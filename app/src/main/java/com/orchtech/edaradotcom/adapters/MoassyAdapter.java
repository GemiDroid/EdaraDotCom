package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

import java.util.List;

/**
 * Created by ahmed.yousef on 4/17/2017.
 */

public class MoassyAdapter extends RecyclerView.Adapter<MoassyAdapter.MoassyHolder> {


    List<String> bulletsList;
    String bullets;
    Context context;
    Boolean isArabic;

    public MoassyAdapter(List<String> bulletsList, Context context, Boolean isArabic) {
        this.bulletsList = bulletsList;
        this.context = context;
        this.isArabic = isArabic;
    }

    @Override
    public MoassyHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_moassy, parent, false);
        return new MoassyHolder(v);
    }

    @Override
    public void onBindViewHolder(MoassyHolder holder, int position) {


        bullets = bulletsList.get(position);

        holder.txt_moassy_bullets.setText(bullets);
    }

    @Override
    public int getItemCount() {
        return bulletsList.size();
    }


    public class MoassyHolder extends RecyclerView.ViewHolder {

        TextView txt_moassy_bullets;

        public MoassyHolder(View itemView) {
            super(itemView);

            LinearLayout lin_moassy = (LinearLayout) itemView.findViewById(R.id.lin_moassy);
            if (!isArabic)
                lin_moassy.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            else
                lin_moassy.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            txt_moassy_bullets = (TextView) itemView.findViewById(R.id.txt_moassy_bullets);

        }
    }
}
