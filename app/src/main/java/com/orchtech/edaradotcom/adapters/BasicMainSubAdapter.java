package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.Subscription.MainSubModel;

import java.util.List;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class BasicMainSubAdapter extends RecyclerView.Adapter<BasicMainSubAdapter.BasicSubHolder> {

    List<MainSubModel.BasicSub> basicSubList;
    MainSubModel.BasicSub basicSub;
    Context context;

    public BasicMainSubAdapter(List<MainSubModel.BasicSub> basicSubList, Context context) {
        this.basicSubList = basicSubList;
        this.context = context;
    }

    @Override
    public BasicSubHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_basic_main_sub, parent, false);
        return new BasicSubHolder(v);
    }

    @Override
    public void onBindViewHolder(BasicSubHolder holder, int position) {

        basicSub = basicSubList.get(position);

        holder.txt_basic_name.setText(basicSub.getTitle());
        holder.txt_basic_price.setText(basicSub.getPrice() + " $");
        holder.switch_basic_turn.setChecked(basicSub.getIsInterest());
        holder.switch_basic_turn.setEnabled(false);


    }

    @Override
    public int getItemCount() {
        return basicSubList.size();
    }


    public class BasicSubHolder extends RecyclerView.ViewHolder {

        TextView txt_basic_name, txt_basic_price;
        Switch switch_basic_turn;

        public BasicSubHolder(View itemView) {
            super(itemView);

            txt_basic_name = (TextView) itemView.findViewById(R.id.txt_basic_name);
            txt_basic_price = (TextView) itemView.findViewById(R.id.txt_basic_price);
            switch_basic_turn = (Switch) itemView.findViewById(R.id.stitch_basic_turn);


        }
    }
}
