package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentArticleDetails;
import com.orchtech.edaradotcom.models.Articles.ArticlesModel;

import java.util.List;

import static android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * Created by ahmed.yousef on 4/3/2017.
 */

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ArticlesHolder> {

    List<ArticlesModel> articlesModelList;
    ArticlesModel articlesModel;
    Context context;
    FragmentManager fragmentManager;

    public ArticlesAdapter(List<ArticlesModel> articlesModelList, Context context) {
        this.articlesModelList = articlesModelList;
        this.context = context;
    }

    @Override
    public ArticlesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.model_articles, parent, false);
        return new ArticlesHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticlesHolder holder, int position) {

        articlesModel = articlesModelList.get(position);

        holder.txt_article_title.setText(articlesModel.getTitle());
        holder.txt_article_pin.setText(articlesModel.getAuthor());
        holder.txt_article_date.setText(articlesModel.getDate());
        holder.txt_article_body.setText(articlesModel.getBody().substring(0, 200) + "..");
        final int id = articlesModel.getArticle_id();
        fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        holder.btn_articles_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment_article_details = new FragmentArticleDetails();
                Bundle bundle = new Bundle();
                bundle.putInt("article_id", id);
                fragment_article_details.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.frame_home, fragment_article_details)
                        .addToBackStack("article_details").addToBackStack("article_details").commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return articlesModelList.size();
    }


    public class ArticlesHolder extends ViewHolder {

        TextView txt_article_title, txt_article_pin, txt_article_date, txt_article_body;
        Button btn_articles_more;

        public ArticlesHolder(View itemView) {
            super(itemView);

            txt_article_title = (TextView) itemView.findViewById(R.id.txt_articles_title);
            txt_article_pin = (TextView) itemView.findViewById(R.id.txt_articles_pin);
            txt_article_date = (TextView) itemView.findViewById(R.id.txt_articles_date);
            txt_article_body = (TextView) itemView.findViewById(R.id.txt_articles_body);
            btn_articles_more = (Button) itemView.findViewById(R.id.btn_articles_more);

        }
    }
}
