package com.orchtech.edaradotcom.adapters;

/**
 * Created by ahmed.yousef on 4/30/2017.
 */

public interface FavoriteListener {
    void onFavoriteChecked(String model, boolean isChecked);
}
