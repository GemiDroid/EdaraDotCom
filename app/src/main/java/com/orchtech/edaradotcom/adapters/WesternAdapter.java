package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

import java.util.List;

/**
 * Created by ahmed.yousef on 4/17/2017.
 */

public class WesternAdapter extends RecyclerView.Adapter<WesternAdapter.WesternHolder> {


    List<String> bulletsList;
    String bullets;
    Context context;

    public WesternAdapter(List<String> bulletsList, Context context) {
        this.bulletsList = bulletsList;
        this.context = context;
    }

    @Override
    public WesternHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_western, parent, false);
        return new WesternHolder(v);
    }

    @Override
    public void onBindViewHolder(WesternHolder holder, int position) {


        bullets = bulletsList.get(position);

        holder.txt_western_bullets.setText(bullets);
    }

    @Override
    public int getItemCount() {
        return bulletsList.size();
    }


    public class WesternHolder extends RecyclerView.ViewHolder {

        TextView txt_western_bullets;

        public WesternHolder(View itemView) {
            super(itemView);

            txt_western_bullets = (TextView) itemView.findViewById(R.id.txt_western_bullets);
        }
    }
}
