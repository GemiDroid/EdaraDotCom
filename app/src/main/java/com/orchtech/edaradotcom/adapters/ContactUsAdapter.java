package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.ContactUs.ContactUs;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ahmed.yousef on 3/19/2017.
 */

public class ContactUsAdapter extends RecyclerView.Adapter<ContactUsAdapter.Holder> {

    List<ContactUs.Contacts> contactUsList;
    Context context;
    ContactUs.Contacts contacts;

    public ContactUsAdapter(List<ContactUs.Contacts> contactUsList, Context context) {
        this.contactUsList = contactUsList;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_contacts, parent, false);
        Holder holder = new Holder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, int position) {

        contacts = contactUsList.get(position);

        Picasso.with(context).load(contacts.getFlag_Company()).into(holder.co_flag);

        holder.country_name.setText(contacts.getCountry_Name());
        holder.co_name.setText(contacts.getCompany_Name());
        holder.address.setText(contacts.getAddress());
        if (contacts.getPh_work() == null) {
            holder.lin_phone.setVisibility(View.GONE);
        } else {
            holder.ph_work.setText(contacts.getPh_work());
        }
        if (contacts.getPh_fax() == null) {
            holder.lin_fax.setVisibility(View.GONE);
        } else {
            holder.ph_fax.setText(contacts.getPh_fax());
        }
        holder.ph_mobile.setText(contacts.getPh_mobile());
        if (contacts.getEmails() == null) {
            holder.lin_email.setVisibility(View.GONE);
        } else {
            holder.emails.setText(Html.fromHtml(contacts.getEmails()));
        }

        // holder.co_name.setText(contacts.getCompany_Name());
        if (contacts.getWhatUp() == null) {
            holder.lin_what_up.setVisibility(View.GONE);
        } else {
            holder.whatus.setText(contacts.getWhatUp());
        }

        holder.emails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.putExtra(Intent.EXTRA_EMAIL, contacts.getAddress());
                i.setType("message/rfc822");
                context.startActivity(Intent.createChooser(i, "ارسل باستخدام"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactUsList.size();
    }


    public class Holder extends RecyclerView.ViewHolder {

        TextView country_name, co_name, address, ph_work, ph_fax, ph_mobile, emails, whatus;
        ImageView co_flag;
        LinearLayout lin_address, lin_phone, lin_mobil, lin_email, lin_fax, lin_what_up;

        public Holder(View itemView) {
            super(itemView);
            country_name = (TextView) itemView.findViewById(R.id.txt_country_name);
            co_name = (TextView) itemView.findViewById(R.id.txt_co_name);
            address = (TextView) itemView.findViewById(R.id.txt_address);
            ph_work = (TextView) itemView.findViewById(R.id.txt_phone);
            ph_fax = (TextView) itemView.findViewById(R.id.txt_fax);
            ph_mobile = (TextView) itemView.findViewById(R.id.txt_mobile);
            emails = (TextView) itemView.findViewById(R.id.txt_email);
            whatus = (TextView) itemView.findViewById(R.id.txt_whats_up);
            co_flag = (ImageView) itemView.findViewById(R.id.img_flag);

            lin_address = (LinearLayout) itemView.findViewById(R.id.lin_address);
            lin_phone = (LinearLayout) itemView.findViewById(R.id.lin_phone);
            lin_mobil = (LinearLayout) itemView.findViewById(R.id.lin_mobile);
            lin_email = (LinearLayout) itemView.findViewById(R.id.lin_email);
            lin_fax = (LinearLayout) itemView.findViewById(R.id.lin_fax);
            lin_what_up = (LinearLayout) itemView.findViewById(R.id.lin_whats_up);
        }
    }


}
