package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.NotificationModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ahmed.yousef on 4/5/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    List<NotificationModel> notificationModelList;
    NotificationModel notificationModel;
    Context context;

    public NotificationAdapter(List<NotificationModel> notificationModelList, Context context) {
        this.notificationModelList = notificationModelList;
        this.context = context;
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_notification, parent, false);

        return new NotificationHolder(v);
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {

        notificationModel = notificationModelList.get(position);

        Locale locale = new Locale("ar", "EG");

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", locale);
        String date_format = notificationModel.getDate();

        try {
            Date date = format.parse(date_format);
            holder.txt_notification_date.setText(date + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.txt_notification_title.setText(notificationModel.getTitle_Ar());
        if (notificationModel.getTitle_Ar() == "عرض جديد") {
            holder.txt_notification_title.setTextColor(context.getResources().getColor(R.color.colorGreen));
        } else {
            holder.txt_notification_title.setTextColor(context.getResources().getColor(R.color.colorOrange));
        }

        holder.txt_notification_body.setText(notificationModel.getDescription());


    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }


    public class NotificationHolder extends RecyclerView.ViewHolder {

        TextView txt_notification_title, txt_notification_date, txt_notification_body;

        public NotificationHolder(View itemView) {
            super(itemView);

            txt_notification_title = (TextView) itemView.findViewById(R.id.notification_title);
            txt_notification_date = (TextView) itemView.findViewById(R.id.notification_date);
            txt_notification_body = (TextView) itemView.findViewById(R.id.notification_body);
        }
    }
}
