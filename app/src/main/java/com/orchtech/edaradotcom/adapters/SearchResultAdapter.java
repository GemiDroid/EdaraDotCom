package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentProductDetails;
import com.orchtech.edaradotcom.models.Products.ProductsModel;

import java.util.List;

import static android.view.View.GONE;

/**
 * Created by ahmed.yousef on 1/19/2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchHolder> {

    static String ID;
    Context context;
    List<ProductsModel> modelList;
    ProductsModel productsModel;
    FragmentManager fragmentManager;

    public SearchResultAdapter(Context context, List<ProductsModel> modelList) {
        this.context = context;
        this.modelList = modelList;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(context).inflate(R.layout.model_serach_result, parent, false);
        return new SearchHolder(v);
    }

    @Override
    public void onBindViewHolder(final SearchHolder holder, int position) {

        productsModel = modelList.get(position);

        if (productsModel.getTitle_ar().length() >= 20) {
            holder.product_title.setText(productsModel.getTitle_ar().substring(0, 20) + " ...");
        } else {
            holder.product_title.setText(productsModel.getTitle_ar());
        }
        holder.product_body.setText(productsModel.getSubtitle_ar());
        holder.product_page.setText("صفحة" + "(" + productsModel.getNumber() + ")");
        ID = productsModel.getId();
        // Check if client not subscribed ...
        if (!productsModel.isSubscribed()) {
            holder.product_price.setText(productsModel.getPrice() + " $");
        } else {
            holder.product_price.setVisibility(GONE);
        }
        holder.switch_isInterest.setChecked(productsModel.isSubscribed());
        Glide.with(context).load(productsModel.getImage_url()).into(holder.product_img);

        holder.product_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, ID, Toast.LENGTH_SHORT).show();
                Bundle i = new Bundle();
                fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                android.support.v4.app.Fragment fragment = new FragmentProductDetails();
                i.putString("product_id", ID);
                i.putBoolean("is_audio", productsModel.is_audio());
                i.putBoolean("is_paid", productsModel.isPaid());
                fragment.setArguments(i);
                fragmentManager.beginTransaction().replace(R.id.frame_home, fragment).
                        addToBackStack("product_details").commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }


    public class SearchHolder extends RecyclerView.ViewHolder {

        TextView product_title, product_body, product_page, product_price, product_more;
        ImageView product_img;
        Switch switch_isInterest;

        public SearchHolder(View itemView) {
            super(itemView);

            product_title = (TextView) itemView.findViewById(R.id.product_title);
            product_body = (TextView) itemView.findViewById(R.id.product_body);
            product_page = (TextView) itemView.findViewById(R.id.product_page);
            product_price = (TextView) itemView.findViewById(R.id.product_price);
            product_more = (TextView) itemView.findViewById(R.id.product_read_more);

            product_img = (ImageView) itemView.findViewById(R.id.product_img);
            switch_isInterest = (Switch) itemView.findViewById(R.id.switch_isInterest);
        }
    }
}
