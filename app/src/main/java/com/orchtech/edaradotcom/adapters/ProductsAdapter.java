package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.fragments.FragmentForgotPass;
import com.orchtech.edaradotcom.fragments.FragmentProductDetails;
import com.orchtech.edaradotcom.managers.ShoppingCart.AddProductManagaer;
import com.orchtech.edaradotcom.models.Products.ProductsModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 1/4/2017.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {


    static String SessionId;
    List<ProductsModel> productsModelList;
    ProductsModel productsModel;
    Context context;
    FragmentManager manager;
    Uri img_uri;
    AddProductManagaer addProductManagaer;

    public ProductsAdapter(List<ProductsModel> articlesModelList, Context context) {
        this.productsModelList = articlesModelList;
        this.context = context;
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_products, parent, false);
        ProductsViewHolder holder = new ProductsViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ProductsViewHolder holder, final int position) {

        productsModel = productsModelList.get(position);

        Picasso.with(context).load(productsModel.getImage_url()).placeholder(R.drawable.search_loading).into(holder.product_img);

        /*if (productsModel.getTitle_ar().length() <= 9) {
            holder.product_title.setText(productsModel.getTitle_ar().substring(0, 5) + "...");
        } else {*/
        holder.product_title.setText(productsModel.getTitle_ar());
        //}
        if (productsModel.getRate_ratio() == "") {

        } else {
            holder.product_rating.setRating(Float.valueOf(productsModel.getRate_ratio()));
        }

        final String prod_id = productsModel.getId();
        final boolean isAudio = productsModel.is_audio();
        final boolean isPaid = productsModel.isPaid();
        final boolean isSubscribed = productsModel.isSubscribed();
        final boolean isPdf = productsModel.is_pdf();
        final String img_url = productsModel.getImage_url();
        final String title_ar = productsModel.getTitle_ar();

        manager = ((FragmentActivity) context).getSupportFragmentManager();

        final Bundle bundle = new Bundle();
        bundle.putString("product_id", prod_id);
        bundle.putBoolean("is_audio", isAudio);
        bundle.putBoolean("is_paid", isPaid);


        holder.product_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                PopupMenu popupMenu = new PopupMenu(context, holder.itemView);

                if (isAudio && isSubscribed) {
                    popupMenu.getMenuInflater().inflate(R.menu.sub_listen_menu, popupMenu.getMenu());
                } else if (isPdf && isSubscribed) {
                    popupMenu.getMenuInflater().inflate(R.menu.sub_read_menu, popupMenu.getMenu());
                } else if (!isSubscribed) {
                    popupMenu.getMenuInflater().inflate(R.menu.un_sub_menu, popupMenu.getMenu());
                }
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menu_pro_details:

                                Fragment fragment = new FragmentProductDetails();
                                fragment.setArguments(bundle);
                                manager.beginTransaction().replace(R.id.frame_home, fragment).addToBackStack("product_details").commit();
                                break;

                            case R.id.menu_product_cart:

                                // Adding Product to Cart ......

                                SessionId = Preferences.getFromPreference(context, "session_id");

                                if (addProductManagaer == null) {
                                    addProductManagaer = new AddProductManagaer();
                                }

                                addProductManagaer.add_product_to_cart(SessionId, prod_id)
                                        .enqueue(new Callback<ValidationModel.Success>() {
                                            @Override
                                            public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                                                if (response.code() >= 200 && response.code() < 300) {
                                                    if (response.body() == null) {
                                                    } else {
                                                        if (response.body().isSuccess()) {

                                                            FragmentForgotPass.showMsg("تم اضافة المنتج بنجاح", "إضافة المنتجات", context).show();
                                                        } else {
                                                            FragmentForgotPass.showMsg(response.body().getMessage(), "إضافة المنتجات", context).show();
                                                        }
                                                    }
                                                } else {
                                                    context.startActivity(new Intent(context, BackEndFailureDialog.class));
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                                                Snackbar.make(view, context.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();

                                            }
                                        });


                                //  Toast.makeText(context, prod_id, Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.menu_product_read:
                                break;

                            case R.id.menu_pro_listen:
                                break;
                            case R.id.menu_pro_download:
                                break;

                        }
                        return false;

                    }
                });

                popupMenu.show();

            }
        });


        holder.product_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

/*
                String path = null;
                try {
                    path = MediaStore.Images.Media.insertImage(context.getContentResolver(), img_url, "", null);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*/
                //   Uri screenshotUri = context.get

              /*  Uri uri=Uri.parse(img_url);
                Intent i = new Intent();
                i.setType("image*//*");
                i.setAction(Intent.ACTION_SEND);
                  i.putExtra(Intent.EXTRA_STREAM, uri);
             //   i.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                //  i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                context.startActivity(Intent.createChooser(i, "شير عن طريق"));*/
           /*     Uri screenshotUri = Uri.parse(img_url);*/
               /* Bitmap bit= null;*/

                //     Bitmap bit = BitmapFactory.decodeStream(context.getContentResolver().openInputStream(Uri.parse(img_url)), null, null);

               /* } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, "Hey view/download this image");

                intent.putExtra(Intent.EXTRA_STREAM, bit);
                intent.setType("image*//*");
               context. startActivity(Intent.createChooser(intent, "Share image via..."));*/

                Intent intent = new Intent(Intent.ACTION_SEND);
                //   intent.putExtra(Intent.EXTRA_TEXT, "Hey view/download this image");
                   /* String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bit, "", null);
                    Uri screenshotUri = Uri.parse(path);*/
                //   Uri screenshotUri = Uri.parse(img_url);

                //   intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                //  intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_TEXT, title_ar);
                intent.setType("text/plain");

                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                context.startActivity(Intent.createChooser(intent, "Share image via..."));

            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manager = ((FragmentActivity) context).getSupportFragmentManager();
                Bundle bundle = new Bundle();
                bundle.putString("product_id", prod_id);
                bundle.putBoolean("is_audio", isAudio);
                bundle.putBoolean("is_paid", isPaid);
                Fragment fragment = new FragmentProductDetails();
                fragment.setArguments(bundle);
                manager.beginTransaction().replace(R.id.frame_home, fragment).addToBackStack("product_details").commit();
            }
        });

    }


    @Override
    public int getItemCount() {
        return productsModelList.size();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder {

        ImageView product_img, product_menu, product_share;
        TextView product_title;
        RatingBar product_rating;

        public ProductsViewHolder(View itemView) {
            super(itemView);

            product_img = (ImageView) itemView.findViewById(R.id.product_img);
            product_menu = (ImageView) itemView.findViewById(R.id.product_menu);
            product_title = (TextView) itemView.findViewById(R.id.product_title);
            product_rating = (RatingBar) itemView.findViewById(R.id.product_rating);
            product_share = (ImageView) itemView.findViewById(R.id.product_share);

        }
    }
}
