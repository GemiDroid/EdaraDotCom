package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.Products.MySubscriptionResponse;

import java.util.List;


/**
 * Created by ahmed.yousef on 4/11/2017.
 */

public class YourSubscriptionAdapter extends RecyclerView.Adapter<YourSubscriptionAdapter.SubHolder> {

    List<MySubscriptionResponse.MySubscriptionModel> subscriptionModelList;
    Context context;
    MySubscriptionResponse.MySubscriptionModel mySubscriptionModel;

    public YourSubscriptionAdapter(List<MySubscriptionResponse.MySubscriptionModel> subscriptionModelList, Context context) {
        this.subscriptionModelList = subscriptionModelList;
        this.context = context;
    }
    //

    @Override
    public SubHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_your_subscription, parent, false);
        return new SubHolder(v);
    }

    @Override
    public void onBindViewHolder(SubHolder holder, int position) {

        mySubscriptionModel = subscriptionModelList.get(position);

        holder.sub_type.setText(mySubscriptionModel.getName());
        holder.sub_copies.setText(mySubscriptionModel.getCopies());
        holder.sub_date_from.setText(mySubscriptionModel.getDateFrom().substring(0, 10));
        holder.sub_date_to.setText(mySubscriptionModel.getDateTo().substring(0, 10));
        if (mySubscriptionModel.isElectronic()) {
            holder.sub_electronic.setText("إلكتروني");
        } else {
            holder.sub_electronic.setText("ورقي");
        }


    }

    @Override
    public int getItemCount() {
        return subscriptionModelList.size();
    }


    public class SubHolder extends RecyclerView.ViewHolder {

        TextView sub_type, sub_copies, sub_date_from, sub_date_to, sub_electronic;

        public SubHolder(View itemView) {
            super(itemView);

            sub_type = (TextView) itemView.findViewById(R.id.sub_type);
            sub_copies = (TextView) itemView.findViewById(R.id.sub_copies);
            sub_date_from = (TextView) itemView.findViewById(R.id.sub_date_from);
            sub_date_to = (TextView) itemView.findViewById(R.id.sub_date_to);
            sub_electronic = (TextView) itemView.findViewById(R.id.sub_electronicOrNot);


        }
    }
}
