package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;

import java.util.List;

/**
 * Created by ahmed.yousef on 5/18/2017.
 */

public class SpinnerAdapter extends BaseAdapter {

    List<String> listAdapter;
    Context context;
    String item;

    public SpinnerAdapter(List<String> listAdapter, Context context) {
        this.listAdapter = listAdapter;
        this.context = context;
    }

    @Override
    public int getCount() {
        return listAdapter.size();
    }

    @Override
    public Object getItem(int position) {
        return listAdapter.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.model_spinner, parent, false);
        }

        item = listAdapter.get(position);

        TextView txt_spinner = (TextView) convertView.findViewById(R.id.txt_model_spinner);

        txt_spinner.setText(item);

        return convertView;
    }
}
