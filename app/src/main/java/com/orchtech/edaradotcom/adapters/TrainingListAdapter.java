package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentTrainingDetails;
import com.orchtech.edaradotcom.fragments.FragmentTrainingRegistration;
import com.orchtech.edaradotcom.models.TrainingListResponse;

import java.util.List;

/**
 * Created by ahmed.yousef on 5/10/2017.
 */

public class TrainingListAdapter extends RecyclerView.Adapter<TrainingListAdapter.TrainingHolder> {


    List<TrainingListResponse.TrainingListModel> trainingList;
    TrainingListResponse.TrainingListModel trainingModel;
    Context context;
    FragmentManager fragmentManager;

    public TrainingListAdapter(List<TrainingListResponse.TrainingListModel> trainingList, Context context) {
        this.trainingList = trainingList;
        this.context = context;
    }

    @Override
    public TrainingHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.model_trainng_list, parent, false);
        return new TrainingHolder(v);
    }

    @Override
    public void onBindViewHolder(TrainingHolder holder, int position) {

        trainingModel = trainingList.get(position);

        holder.txt_training_title.setText("- " + trainingModel.getName());
        holder.txt_training_from_to.setText(trainingModel.getFrom() + " - " + trainingModel.getTo());
        holder.txt_training_period.setText("الفترة " + (Integer.parseInt(trainingModel.getTo().substring(0, 2)) -
                Integer.parseInt(trainingModel.getFrom().substring(0, 2)) + 1 + " أيام "));

        holder.txt_training_language.setText("لغة البرنامج  العربية");

        holder.txt_training_place.setText(" المدينة " + trainingModel.getPlace());

        int id = trainingModel.getId();

        fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
        final Bundle b = new Bundle();
        b.putString("planId", String.valueOf(id));
        b.putString("training_from_to", holder.txt_training_from_to.getText().toString());
        b.putString("training_period", holder.txt_training_period.getText().toString());
        //  b.putString("training_language", holder.txt_training_language.getText().toString());
        b.putString("training_place", holder.txt_training_place.getText().toString());
        b.putString("training_title", holder.txt_training_title.getText().toString());

        holder.txt_training_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment frag = new FragmentTrainingRegistration();
                frag.setArguments(b);
                fragmentManager.beginTransaction().replace(R.id.frame_home, frag).addToBackStack("training_register").commit();


            }
        });
        holder.txt_training_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frag = new FragmentTrainingDetails();
                frag.setArguments(b);
                fragmentManager.beginTransaction().replace(R.id.frame_home, frag).addToBackStack("training_details").commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return trainingList.size();
    }


    public class TrainingHolder extends RecyclerView.ViewHolder {

        TextView txt_training_title, txt_training_from_to,
                txt_training_place,
                txt_training_language, txt_training_period, txt_training_details, txt_training_register;


        public TrainingHolder(View itemView) {
            super(itemView);

            txt_training_title = (TextView) itemView.findViewById(R.id.txt_training_title);
            txt_training_from_to = (TextView) itemView.findViewById(R.id.txt_training_from_to);
            txt_training_place = (TextView) itemView.findViewById(R.id.txt_training_place);
            txt_training_language = (TextView) itemView.findViewById(R.id.txt_training_language);
            txt_training_period = (TextView) itemView.findViewById(R.id.txt_training_period);
            txt_training_details = (TextView) itemView.findViewById(R.id.txt_training_details);
            txt_training_register = (TextView) itemView.findViewById(R.id.txt_training_register);


        }
    }
}
