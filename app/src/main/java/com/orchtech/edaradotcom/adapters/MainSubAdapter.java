package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.fragments.FragmentForgotPass;
import com.orchtech.edaradotcom.managers.ShoppingCart.AddSubscriptionManager;
import com.orchtech.edaradotcom.models.Subscription.MainSubModel;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 4/18/2017.
 */

public class MainSubAdapter extends RecyclerView.Adapter<MainSubAdapter.MainSubHolder> {


    static String SessionId;
    List<MainSubModel.SubTypesList> mainSubModelList;
    MainSubModel.SubTypesList mainSubModel;
    Context context;
    AddSubscriptionManager addSubscriptionManager;

    public MainSubAdapter(List<MainSubModel.SubTypesList> mainSubModelList, Context context) {
        this.mainSubModelList = mainSubModelList;
        this.context = context;
    }

    @Override
    public MainSubHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(context).inflate(R.layout.model_main_sub, parent, false);

        return new MainSubHolder(v);

    }

    @Override
    public void onBindViewHolder(MainSubHolder holder, int position) {

        mainSubModel = mainSubModelList.get(position);

        holder.txt_main_sub_title.setText(mainSubModel.getTitle());
        holder.txt_main_sub_description.setText(mainSubModel.getDescription());
        if (mainSubModel.getPrice() == null) {
            holder.btn_buy_main_sub.setVisibility(View.GONE);
        } else {
            holder.btn_buy_main_sub.setText(mainSubModel.getPrice() + "$ " + "اشترك الآن");
        }
        if (mainSubModel.getBasicSubList().isEmpty()) {
            holder.rec_basic_main_sub.setVisibility(View.GONE);
        } else {
            BasicMainSubAdapter
                    basicMainSubAdapter = new BasicMainSubAdapter(mainSubModel.getBasicSubList(), context);
            holder.rec_basic_main_sub.setAdapter(basicMainSubAdapter);
        }

        int[] main_img = {R.drawable.ic_sub_listen, R.drawable.ic_sub_shamil, R.drawable.ic_sub_year};

        Glide.with(context).load(main_img[position]).into(holder.img_main_sub);

        final String SubTypeId = mainSubModel.getSubTypeId();
        holder.btn_buy_main_sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                SessionId = Preferences.getFromPreference(context, "session_id");

                if (addSubscriptionManager == null) {
                    addSubscriptionManager = new AddSubscriptionManager();
                }
                addSubscriptionManager.add_subscription_to_cart(SessionId, SubTypeId)
                        .enqueue(new Callback<ValidationModel.Success>() {
                            @Override
                            public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                                if (response.code() >= 200 && response.code() < 300) {
                                    if (response.body() == null) {
                                    } else {
                                        if (response.body().isSuccess()) {
                                            FragmentForgotPass.showMsg("تم إضافة الاشتراك بنجاح", "الإشتراكات", context).show();
                                        } else {
                                            FragmentForgotPass.showMsg(response.body().getMessage(), "الإشتراكات", context).show();
                                        }
                                    }
                                } else {
                                    context.startActivity(new Intent(context, BackEndFailureDialog.class));
                                }
                            }

                            @Override
                            public void onFailure(Call<ValidationModel.Success> call, Throwable t) {
                                Snackbar.make(v, context.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                            }
                        });

                //FragmentForgotPass.showMsg("تم اضافة الاشتراك", "الإشتراك الرئيسي", context).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainSubModelList.size();
    }


    public class MainSubHolder extends RecyclerView.ViewHolder {


        RecyclerView rec_basic_main_sub;
        TextView txt_main_sub_title, txt_main_sub_description;
        Button btn_buy_main_sub;
        ImageView img_main_sub;

        public MainSubHolder(View itemView) {
            super(itemView);

            rec_basic_main_sub = (RecyclerView) itemView.findViewById(R.id.rec_main_sub);
            rec_basic_main_sub.setItemAnimator(new DefaultItemAnimator());
            rec_basic_main_sub.setLayoutManager(new LinearLayoutManager(context));

            txt_main_sub_title = (TextView) itemView.findViewById(R.id.txt_title_main_sub);
            txt_main_sub_description = (TextView) itemView.findViewById(R.id.txt_desc_main_sub);

            btn_buy_main_sub = (Button) itemView.findViewById(R.id.btn_buy_main_sub);
            img_main_sub = (ImageView) itemView.findViewById(R.id.img_main_sub);


        }
    }
}
