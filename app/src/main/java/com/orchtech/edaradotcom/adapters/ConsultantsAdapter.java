package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.fragments.FragmentConsultantDetails;
import com.orchtech.edaradotcom.models.questions.QuestionsListResponse;

import java.util.List;

/**
 * Created by ahmed.yousef on 5/8/2017.
 */

public class ConsultantsAdapter extends RecyclerView.Adapter<ConsultantsAdapter.ConsultantsHolder> {

    android.support.v4.app.FragmentManager fragmentManager;
    Context context;
    List<QuestionsListResponse.QuestionsList> questionsLists;
    QuestionsListResponse.QuestionsList questions;


    public ConsultantsAdapter(Context context, List<QuestionsListResponse.QuestionsList> questionsLists) {
        this.context = context;
        this.questionsLists = questionsLists;
    }

    @Override
    public ConsultantsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.model_consultants, parent, false);

        return new ConsultantsHolder(v);
    }

    @Override
    public void onBindViewHolder(ConsultantsHolder holder, int position) {


        questions = questionsLists.get(position);


        final int id = questions.getQuestion_id();

        holder.txt_question_id.setText(id + " #");
        holder.txt_question_writer.setText(questions.getConsumer());
        holder.txt_question_date.setText(questions.getDate());
        holder.txt_question_body.setText(questions.getTitle());

        fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

        holder.btn_question_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new FragmentConsultantDetails();
                Bundle i = new Bundle();
                i.putInt("consultant_id", id);
                fragment.setArguments(i);
                fragmentManager.beginTransaction().
                        replace(R.id.frame_home, fragment).
                        addToBackStack("consultant_details").commit();

            }
        });


    }

    @Override
    public int getItemCount() {
        return questionsLists.size();
    }

    public class ConsultantsHolder extends RecyclerView.ViewHolder {

        TextView txt_question_id, txt_question_writer, txt_question_date, txt_question_body, btn_question_read_more;

        public ConsultantsHolder(View itemView) {
            super(itemView);


            txt_question_writer = (TextView) itemView.findViewById(R.id.consultant_writer);
            txt_question_id = (TextView) itemView.findViewById(R.id.consultant_id);
            txt_question_date = (TextView) itemView.findViewById(R.id.consultant_date);
            txt_question_body = (TextView) itemView.findViewById(R.id.consultant_title);
            btn_question_read_more = (TextView) itemView.findViewById(R.id.consultant_read);
        }
    }
}
