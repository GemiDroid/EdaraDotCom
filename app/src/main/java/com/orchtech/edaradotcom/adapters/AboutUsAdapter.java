package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.AboutUs.AboutUsResponse;

import java.util.List;

/**
 * Created by ahmed.yousef on 1/19/2017.
 */

public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsAdapter.AboutUsHolder> {

    Context context;
    List<AboutUsResponse.AboutUsBullet> aboutUsBulletsList;
    AboutUsResponse.AboutUsBullet aboutUsBullet;

    public AboutUsAdapter(Context context, List<AboutUsResponse.AboutUsBullet> aboutUsBulletsList) {
        this.context = context;
        this.aboutUsBulletsList = aboutUsBulletsList;
    }

    @Override
    public AboutUsAdapter.AboutUsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.model_about_us, parent, false);
        return new AboutUsHolder(view);

    }

    @Override
    public void onBindViewHolder(AboutUsAdapter.AboutUsHolder holder, int position) {

        aboutUsBullet = aboutUsBulletsList.get(position);

        holder.txt_bullet.setText(aboutUsBullet.getBullet_text());


    }

    @Override
    public int getItemCount() {
        return aboutUsBulletsList.size();
    }

    public class AboutUsHolder extends RecyclerView.ViewHolder {

        TextView txt_bullet;

        public AboutUsHolder(View itemView) {
            super(itemView);

            txt_bullet = (TextView) itemView.findViewById(R.id.txt_about);
        }
    }
}
