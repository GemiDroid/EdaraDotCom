package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.ModelMenu;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ahmed.yousef on 12/29/2016.
 */


public class MenuAdapter extends BaseExpandableListAdapter {


    ModelMenu modelMenu;
    ModelMenu modelMenu2;
    private Context _context;
    private List<ModelMenu> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<ModelMenu, List<ModelMenu>> _listDataChild;

    public MenuAdapter(Context _context, List<ModelMenu> _listDataHeader, HashMap<ModelMenu, List<ModelMenu>> _listDataChild) {
        this._context = _context;
        this._listDataHeader = _listDataHeader;
        this._listDataChild = _listDataChild;
    }

//============================================ Child =========================================//

    @Override
    public int getChildrenCount(int child) {
        try {
            return this._listDataChild.get(this._listDataHeader.get(child)).size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Object getChild(int group, int child) {
        return this._listDataChild.get(this._listDataHeader.get(group)).get(child);
    }

    @Override
    public long getChildId(int group, int child) {
        return child;
    }

    @Override
    public View getChildView(int group, int child, boolean b, View convertView, ViewGroup viewGroup) {


        /*modelMenu = this._listDataChild.get(this._listDataHeader.get(group)).get(child);*/

        modelMenu = (ModelMenu) getChild(group, child);

        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.model_menu_child, null);
        }

        TextView txt_menu_child = (TextView) convertView.findViewById(R.id.txt_menu_child);
        TextView txt_color_separator = (TextView) convertView.findViewById(R.id.txt_color_separator);
        //   ImageView img_menu_child = (ImageView) convertView.findViewById(R.id.img_menu_child);


        if (modelMenu != null) {
            txt_menu_child.setText(modelMenu.getMenu_txt());
            //   img_menu_child.setImageResource(modelMenu.getMenu_img());

            //   Picasso.with(_context).load(modelMenu.getMenu_img()).into(img_menu_child);
        }


        /*if((getChild(group,child).equals(getChild(group,_listDataChild.size()-1)))){
            txt_color_separator.setVisibility(View.GONE);
        }
        else {
            txt_color_separator.setVisibility(View.VISIBLE);
        }*/

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int group, int child) {
        return true;
    }

    //============================================ Parent ========================================//

    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public Object getGroup(int group) {
        return this._listDataHeader.get(group);
    }

    @Override
    public long getGroupId(int group) {
        return group;
    }

    @Override
    public View getGroupView(int group, boolean isExpanded, View convertView, ViewGroup viewGroup) {

      /*  modelMenu = this._listDataHeader.get(group);*/

        modelMenu = (ModelMenu) getGroup(group);
        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.model_menu_parent, null);
        }
        TextView txt_menu_parent = (TextView) convertView.findViewById(R.id.txt_menu_parent);
        ImageView img_menu_parent = (ImageView) convertView.findViewById(R.id.img_menu_parent);
        ImageView img_selector = (ImageView) convertView.findViewById(R.id.img_selector_menu_parent);
        txt_menu_parent.setText(modelMenu.getMenu_txt());
        img_menu_parent.setImageResource(modelMenu.getMenu_img());


        String[] parent_expanded = {"الإصدارات", "الإشتراكات", "تواصل معنا", "عن الشركه"};
        switch (group) {

            case 0:
                img_selector.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    img_selector.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    img_selector.setImageResource(R.drawable.ic_arrow_down);
                }
            case 1:
                img_selector.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    img_selector.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    img_selector.setImageResource(R.drawable.ic_arrow_down);
                }
            case 8:
                img_selector.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    img_selector.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    img_selector.setImageResource(R.drawable.ic_arrow_down);
                }
            case 9:
                img_selector.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    img_selector.setImageResource(R.drawable.ic_arrow_up);
                } else {
                    img_selector.setImageResource(R.drawable.ic_arrow_down);
                }
                break;
            default:
                img_selector.setVisibility(View.GONE);
        }




       /*
        }



        for (int i = 0; i < parent_expanded.length; i++) {
          if(modelMenu.getMenu_txt().equals(parent_expanded[i])) {
              img_selector.setVisibility(View.VISIBLE);
              if (isExpanded) {
                  img_selector.setImageResource(R.drawable.up_arrow);
              } else {
                  img_selector.setImageResource(R.drawable.down_arrow);
              }
          }
        }
        img_selector.setVisibility(View.VISIBLE);*/


        /*if (modelMenu.getMenu_txt().equals("تواصل معنا")) {
            if (isExpanded) {
                img_selector.setImageResource(R.drawable.up_arrow);
            } else {
                img_selector.setImageResource(R.drawable.down_arrow);
            }
        }
        if (modelMenu.getMenu_txt().equals("عن الشركه")) {
            if (isExpanded) {
                img_selector.setImageResource(R.drawable.up_arrow);
            } else {
                img_selector.setImageResource(R.drawable.down_arrow);
            }
        }
        if (modelMenu.getMenu_txt().equals("")) {
            if (isExpanded) {
                img_selector.setImageResource(R.drawable.up_arrow);
            } else {
                img_selector.setImageResource(R.drawable.down_arrow);
            }
        }

        *//*} else if () {
            if (isExpanded) {
                img_selector.setImageResource(R.drawable.up_arrow);
            } else {
                img_selector.setImageResource(R.drawable.down_arrow);
            }*//*
        else {
            img_selector.setVisibility(View.GONE);
            img_selector.setImageResource(R.drawable.down_arrow);
        }*/


        return convertView;
    }

    //============================================================================================//

    @Override
    public boolean hasStableIds() {
        return false;
    }

}
