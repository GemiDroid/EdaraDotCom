package com.orchtech.edaradotcom.adapters;

/**
 * Created by ahmed.yousef on 4/19/2017.
 */

/*public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.FavouritesHolder> {


    List<String> ListIDs;

    List<SubjectsModel> subjectsModelList;
    SubjectsModel subjectsModel;
    Context context;

    public FavouritesAdapter(List<SubjectsModel> subjectsModelList, Context context) {
        this.subjectsModelList = subjectsModelList;
        this.context = context;
        ListIDs = new ArrayList<>();
    }

    public List<String> getCheckedList() {
        return ListIDs;
    }

    @Override
    public FavouritesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.model_favourites, parent, false);

        return new FavouritesHolder(view);
    }

    @Override
    public void onBindViewHolder(final FavouritesHolder holder, final int position) {


        subjectsModel = subjectsModelList.get(position);
        holder.txt_fav_name.setText(subjectsModel.getText_Ar());


        //in some cases, it will prevent unwanted situations
        holder.switch_fav.setOnCheckedChangeListener(null);


        holder.switch_fav.setChecked(subjectsModel.isIs_Interest());






        holder.switch_fav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                // .get(holder.getAdapterPosition()).setSelected(isChecked);


                if (isChecked) {
                    if (!ListIDs.contains(subjectsModel.getID()))
                        ListIDs.add(subjectsModel.getID());
                    buttonView.setChecked(true);
                    //   subjectsModelList.get(holder.getAdapterPosition()).setIs_Interest(isChecked);

                    Toast.makeText(context, ListIDs.size() + "", Toast.LENGTH_SHORT).show();

                } else {
                    if (ListIDs.contains(subjectsModel.getID()))
                        ListIDs.remove(subjectsModel.getID());
                    buttonView.setChecked(false);
                    Toast.makeText(context, ListIDs.size() + "", Toast.LENGTH_SHORT).show();
                }
                holder.switch_fav.setChecked(ListIDs.contains(subjectsModel.getID()));


            }


        });

    }

    @Override
    public int getItemCount() {
        return subjectsModelList.size();
    }

    public class FavouritesHolder extends RecyclerView.ViewHolder {

        TextView txt_fav_name;
        Switch switch_fav;

        public FavouritesHolder(View itemView) {
            super(itemView);

            txt_fav_name = (TextView) itemView.findViewById(R.id.txt_favourites);
            switch_fav = (Switch) itemView.findViewById(R.id.switch_favourites);
        }
    }
}*/

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.models.Products.SubjectsModel;

import java.util.ArrayList;

/**
 * Created by ahmed.yousef
 * on 4/19/2017.
 */

public class FavouritesAdapter extends RecyclerView.Adapter<FavouritesAdapter.FavouritesHolder> {

    private ArrayList<SubjectsModel> items;
    private Context context;
    private FavoriteListener listener;

    public FavouritesAdapter(ArrayList<SubjectsModel> items, Context context, FavoriteListener listener) {
        this.items = items;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public FavouritesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.model_favourites, parent, false);

        return new FavouritesHolder(view);
    }

    @Override
    public void onBindViewHolder(final FavouritesHolder holder, final int position) {


        SubjectsModel model = items.get(position);

        holder.txt_fav_name.setText(model.getText_Ar());


        //in some cases, it will prevent unwanted situations
        holder.switch_fav.setOnCheckedChangeListener(null);

        if (model.isIs_Interest()) {
            holder.switch_fav.setChecked(true);
        } else {
            holder.switch_fav.setChecked(false);
        }

        holder.switch_fav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listener.onFavoriteChecked(items.get(position).getID(), true);
                } else {
                    listener.onFavoriteChecked(items.get(position).getID(), false);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class FavouritesHolder extends RecyclerView.ViewHolder {

        TextView txt_fav_name;
        Switch switch_fav;

        public FavouritesHolder(View itemView) {
            super(itemView);
            txt_fav_name = (TextView) itemView.findViewById(R.id.txt_favourites);
            switch_fav = (Switch) itemView.findViewById(R.id.switch_favourites);
        }
    }
}


