package com.orchtech.edaradotcom.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.orchtech.edaradotcom.R;
import com.orchtech.edaradotcom.activities.BackEndFailureDialog;
import com.orchtech.edaradotcom.fragments.FragmentCart;
import com.orchtech.edaradotcom.fragments.FragmentForgotPass;
import com.orchtech.edaradotcom.managers.ShoppingCart.DeleteInvoiceManager;
import com.orchtech.edaradotcom.models.ValidationModel;
import com.orchtech.edaradotcom.models.cart.ShoppingCartModel;
import com.orchtech.edaradotcom.settings.Preferences;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmed.yousef on 5/2/2017.
 */

public class CardDataAdapter extends RecyclerView.Adapter<CardDataAdapter.CardDataHolder> {

    static String SessionId;
    List<ShoppingCartModel> cartModelList;
    ShoppingCartModel shoppingCartModel;
    Context con;
    DeleteInvoiceManager deleteInvoiceManager;
    FragmentActivity fragmentActivity;

    public CardDataAdapter(List<ShoppingCartModel> cartModelList, Context con) {
        this.cartModelList = cartModelList;
        this.con = con;
    }

    @Override
    public CardDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(con).inflate(R.layout.model_cart_data, parent, false);
        return new CardDataHolder(v);


    }

    @Override
    public void onBindViewHolder(CardDataHolder holder, final int position) {

        shoppingCartModel = cartModelList.get(position);

        holder.txt_cart_serial.setText((position + 1) + "");
        holder.txt_cart_quantity.setText(shoppingCartModel.getNoOfCopies());
        holder.txt_cart_product.setText(shoppingCartModel.getItemName());
        holder.txt_cart_price.setText(shoppingCartModel.getPrice() + " $");
        final String InvoiceId = shoppingCartModel.getInvoiceDetailId();


        holder.txt_cart_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                SessionId = Preferences.getFromPreference(con, "session_id");
                // Toast.makeText(con, InvoiceId, Toast.LENGTH_SHORT).show();
                if (deleteInvoiceManager == null) {
                    deleteInvoiceManager = new DeleteInvoiceManager();
                }
                deleteInvoiceManager.DeleteInvoice(SessionId, InvoiceId).enqueue(new Callback<ValidationModel.Success>() {
                    @Override
                    public void onResponse(Call<ValidationModel.Success> call, Response<ValidationModel.Success> response) {

                        if (response.code() >= 200 && response.code() < 300) {
                            if (response.body() == null) {
                            } else {
                                if (response.body().isSuccess()) {
                                    FragmentForgotPass.showMsg("تم حذف المنتج بنجاح", "سلة المشتريات", con).show();
                                    FragmentCart.getShoppingCart(v, fragmentActivity);

                                    // cartModelList.remove(shoppingCartModel);
                                } else {
                                    FragmentForgotPass.showMsg(response.body().getMessage(), "سلة المشتريات", con).show();
                                }
                            }
                        } else {
                            con.startActivity(new Intent(con, BackEndFailureDialog.class));
                        }
                    }

                    @Override
                    public void onFailure(Call<ValidationModel.Success> call, Throwable t) {

                        Snackbar.make(v, con.getResources().getString(R.string.internet_failure), Snackbar.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartModelList.size();
    }


    public class CardDataHolder extends RecyclerView.ViewHolder {

        TextView txt_cart_serial, txt_cart_quantity, txt_cart_product, txt_cart_price, txt_cart_delete;

        public CardDataHolder(View itemView) {
            super(itemView);
            txt_cart_serial = (TextView) itemView.findViewById(R.id.txt_cart_serial);
            txt_cart_quantity = (TextView) itemView.findViewById(R.id.txt_cart_quantity);
            txt_cart_price = (TextView) itemView.findViewById(R.id.txt_cart_price);
            txt_cart_product = (TextView) itemView.findViewById(R.id.txt_cart_product);
            txt_cart_delete = (TextView) itemView.findViewById(R.id.txt_cart_delete);

        }
    }
}
